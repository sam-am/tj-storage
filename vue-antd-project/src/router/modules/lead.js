let leadRoute = {
  path: 'lead',
  name: 'lead',
  meta: {
    name: [{name:'引导'}],
    token: true
  },
  component: () => import('@/views/Lead/index.vue')
}

export default leadRoute