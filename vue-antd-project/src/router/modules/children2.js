let children2Route = {
  path: 'children2',
  name: 'children2',
  meta: {
    name: [{name:'嵌套路由'},{name:'children2'}],
    token: true
  },
  component: () => import('@/views/Children2/index.vue')
}

export default children2Route