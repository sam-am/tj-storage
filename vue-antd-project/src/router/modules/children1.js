let children1Route = {
  path: 'children1',
  name: 'children1',
  meta: {
    name: [{name:'嵌套路由'},{name:'children1'}],
    token: true
  },
  component: () => import('@/views/Children1/index.vue')
}

export default children1Route