import router from './index'  //导入路由实例

// 从node_modules中引入 nprogress js对象和css样式
import NProgress from 'nprogress'  //会找到js文件
import 'nprogress/nprogress.css'

/*
  1）问题：怎么知道我们去的是内部页面还是外部页面？
    - 在路由元信息中添加区分字段，布尔值区分是内部页面还是外部页面
*/
// 路由守卫是对页面权限的认证，已经存在token了，对内部页面和外部页面进行区分
// 登录页验证是否存在token
router.beforeEach((to,from,next)=>{
  NProgress.start();
  if(to.meta.token) {  // 内部页面
    if(sessionStorage.getItem('token')) {
      next()
    } else {
      next('/login')
    }
  } else {
    // 外部页面
    next()
  }
})

router.afterEach((to,from)=>{
  NProgress.done();
})

// 与main.js产生关联