// 公共的请求配置

// 1. 下载axios后导入axios实例
import axios from 'axios'
// 2. 创建axios实例
let service = axios.create({
  baseURL: 'https://www.fastmock.site/mock/38a8cb64cbc0fc25f31cc5152fb662b6/admin',
  timeout: 6000
})

// 3. 创建拦截器

// 3.1 请求拦截 request.use(参数一，参数二) => 参数一：成功请求拦截的处理方法，参数二：失败请求拦截处理方法
service.interceptors.request.use(
  config => {
    // 添加请求拦截要做的事
    config.headers['token'] = sessionStorage.getItem('token') || ''  //发起请求的时候在请求头中添加token
    return config
  },
  error => {
    return console.log('请求失败', error);
  }
)

// 3.2 响应拦截
service.interceptors.response.use(
  con => {
    return con
  },
  err => {
    return console.log('请求失败', err);
  }
)

// 导出对象
export default service