import { ref } from 'vue'
import { getPie } from "@/http/api/index.js";

export const pieModules = () => {
  let selectlist = ["全部渠道", "线上", "门店"];
  let activeIdex = ref(0);
  let pieData = ref([])
  let pieObjAll = ref({})

  const change = (index) => {
    activeIdex.value = index;
    // 改变传入的值
    if (index == 0) {
      pieData.value = pieObjAll.value.channel;
    } else if (index == 1) {
      pieData.value = pieObjAll.value.online;
    } else {
      pieData.value = pieObjAll.value.store;
    }
  };

  const getPieD = () => {
    getPie("/getPie").then((res) => {
      // console.log(res);
      if(res.data.code == 200) {
        pieData.value = res.data.pieData.channel
        pieObjAll.value = res.data.pieData;
      }
    });
  };

  return {selectlist, activeIdex, pieData, pieObjAll, change, getPieD}
}