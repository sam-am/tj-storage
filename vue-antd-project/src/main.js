// 入口文件 - 项目第一次运行时执行，这里面的代码都会被执行

import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

// 引入全局样式表
import './style/common.css'

// 引入阿里矢量图表库样式
import './style/Icon.css'

// 导入路由守卫
import './router/beforEach.js'

// 注入模拟的数据
import './mock/mock.js'

// createApp(App).use(store).use(router).mount('#app')
let app = createApp(App)
app.use(store)
app.use(router)
app.mount('#app')
