# vue3-admin-project

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### 目录结构说明
```
node_modules - 项目依赖包
public - 静态文件
src - 项目核心代码
assets - 公共方法
components - 组件
router - 路由文件
store - 仓库文件
views - 页面级组件
App.vue - 根组件
man.js - 入口文件 - 项目第一次运行的时候执行，这里面的代码都会执行
vue.config.js - webpack配置文件
```

### 项目配置
1. 处理公共样式
1）在src文件夹下创建common.css文件 => 在main.js中进行引入
  - 清除浏览器默认间距
  - 给每个div添加怪异盒模型
2）根据组件需要继承高度

2. 封装请求实现页面和请求一一对应
1）为什么在项目中需要请求封装 axios
  - 因为我们在工作中有3服务器 线上服务器 开发服务器 测试服务器 => 修改服务器根地址
  - 快速维护
  - 添加公共属性，比如服务器根地址，请求超时，设置拦截器
2）为什么使用axios?
 - 就是axiso的特点：官网 https://www.axios-http.cn/docs/intro
3）在src文件夹下创建http文件夹 => index.js 请求拦截
 - 引入axios: npm install axios
 - fastmock官网
 - api文件夹下创建和views文件夹页面实现对应

 3. 封装登录请求模块, 实现页面和api文件夹对应

 4. 按需引入element ui 组件库
  - 安装插件 npm install element-plus --save
  - 安装按需引入的插件 npm install -D unplugin-vue-components unplugin-auto-import
  - 在vue.config.js配置插件，注意要添加一个 configureWebpack字段允许插件
  - 可以在组件内直接使用饿了么组件了

  ### 业务
  1. 完成基本样式
  2. 基本功能的实现

  ### 登录组件密码框的封装

  ### 点击登录，提交表单数据，请求后端返回数据

  1. 封装的密码框中用emit传递数据给父组件 - 登录页
  2. 登录页接收到子组件给的数据
  3. 点击登录时，拿到表单数据，发起登录请求后端接口数据，拿到res中的token存储在浏览器中并跳转到布局页

  ### 布局页面搭建
  1. 路由表添加路由信息
  2. 创建组件
  3. 引入侧边栏组件代码
  4. 引入icon - 安装 - 注册所有 或者 安装插件按需引入


  ## 侧边栏实现
    - 侧边栏导航的两种情况：有嵌套 没有嵌套
    - 侧边栏的渲染：根据不同的用户 => 动态生成 侧边栏导航目录 （不同权限用户进入应该看到不同权限下的目录结构）
      1、分析数据结构 => [
        {path:'/index',name:'首页',icon:''},
        {path:'/document',name:'文档',icon:''},
        {path:'',name:'嵌套路由',icon:'',childlren:[
          {path:'children1',name:'children1',icon:''},
        ]},
        ]
  ### 二次封装element ui 组件
    1）认识组件
    2）根据我们的页面进行处理 => 侧边栏导航根据用户动态生成
    实现： 在LeftC文件夹下创建一个文件 NavItem.vue => 关注侧边栏每一项的内容是嵌套的还是不嵌套

  ### 侧边栏通过element menu组件的router属性启用路由模式
    - 通过 default-active 设置加载时的激活项，它的值作为path跳转

  ### 动态处理icon
    - 创建一个公共方法，自动查找icon

  ## 右边的布局
  ### 头部布局

  ### 收缩组件
    1. 完成基本样式
    2. 点击样式改变

  ### 封装搜索组件
    
  1）基本样式布局

  ## 封装全屏组件

    1）搭建文件结构
    2）实现功能
  
  ## 封装切换字体大小组件

  ## 侧边栏优化

    1) 开启路由模式
    2）配置路由表，添加对应视图组件

    3) 浏览器刷新 高亮和浏览器地址一一对应
      - 通过watch实时监听路由信息，将路由信息中的地址赋值给默认高亮，就可以实现高亮是动态的

    4）如果高亮在嵌套路由里面，刷新时嵌套路由会收起来，--待解决

  ## 封装面包屑组件

    1）文件结构搭建
    2）使用element ui面包屑组件
    3) 二次处理element ui 面包屑组件
    3）样式处理

  ## 权限处理
    1. 基础版本
      - 适用情况：官网，后台管理系统，没有权限，几个权限
        1）前端处理内部页面和外部页面 => 路由守卫
        2）后端 => 内部接口和外部接口 => 在请求的时候添加token => 请求拦截器
      - 实现
        1）登录页点击登录 => 成功 => 后端返回token => 保存在本地
        2）适用路由守卫 => 进行判断用户是否登录
        3）将路由守卫抽离出去
        4）添加路由进度条 --第三方插件 nprogress
          - 安装
          - 从node_modules中引入 nprogress js对象和css样式

  ## 封装退出登录组件

  ## 解决刷新左侧字体大小数据丢失问题

  ## 权限处理 --进阶版本
  - 根据用户登录的时候动态生成侧边栏导航（权限问题，公共10，A10，B10）
  - 路由固定的
  - 30个以下 --权限不能太多

  ## 封装路由标签组件
    注意：数据结构：看到ui图就转换成数据结构 特点
    1）基本样式布局
    2）定义数据结构 => 数组
    3）需要有默认值 => 用户首次进入后台管理显示首页
    4）切换左侧导航栏 => 路由改变 => 监听路由信息 => 拿到当前路由的name，添加到初始数组完成数据视图交互
    5）数组追加数据去重
    6）当前url一一对应的高亮 => 动态class属性 => 三元表达式 两个class样式选择
    7）删除：高亮的；不高亮的
    8）添加删除点击事件 --阻止事件冒泡
    9）添加路由跳转事件

  ## ECharts初体验
  一、echarts初体验
    1、创建一个视图组件挂载到index视图组件中
    2、安装echarts
    3、在生命周期中将echarts实例对象挂载到容器节点中
    4、认识echarts中的属性
    5、抽离成一个echarts组件，通过父组件传递数据给子组件，使用动态的数据生成我们的echarts图表
    如果处理的数据是一个数组，要写成一个工厂函数

  ## mockjs 造假数据的流程

    1、安装 npm install mockjs
    2、创建mock文件夹目录 --用于放mock假数据的文件
    3、在目录下创建一个.js文件 --名字自己命名 贴近语义化，比如courseData.js => 课程数据
    4、在目录下再创建一个mock.js作为假数据的入口文件，以后所有的数据文件都导入到这个入口文件内
    5、将入口文件注入到main.js中
    6、在需要请求数据的组件内请求我们mock的假数据即可

  ## 动态路由

  - 用户点击登录 获取到用户的权限 => 侧边栏导航 => 路由信息
  - 知识点：vuex 路由 promise


  ## 在请求拦截中的请求头header的添加token

  ## 将路由配置表的每一个路由配置项抽离出去
