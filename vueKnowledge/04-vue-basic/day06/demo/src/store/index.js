import { createStore } from '../../node_modules/vuex/dist/vuex.cjs'

import user from './user'

const store = createStore({
  state() {
    return {
      count: 88,
      list: [1,2,3,4,5,6,7,8,9]
    }
  },
  getters: {
    getevenlist(aa) {
      // console.log(aa.count);
      // return aa.count
      return aa.list.filter((item)=>{
        return item % 2 ==0
      })
    }
  },
  mutations: {
    changeList(bb,cc) {
      // console.log(bb);
      // bb.list = cc
      bb.count = cc
    }
  },
  actions: {
    asyncChangeCount(dd) {
      setTimeout(()=>{
        dd.commit('changeList',300)
      },2000)
    }
  },
  modules: {
    user: user
  }
})

export default store

/*
  当大仓库和小仓库有同名方法的时候，在组件中触发这个方法就不知道是触发的大仓库的方法还是小仓库的方法，
  开启命名空间的作用就是：在用commit触发方法的时候要加上小仓库的名字作为路径来区分开两个方法

*/