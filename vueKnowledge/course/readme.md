# SZ-2208

这里是我们的代码，我们的课件



## 仓库地址

```
git clone https://gitee.com/young1024plus/sz2208.git
```

这个仓库最好只看，不要去修改。

如果不小心改了，就重新克隆一遍



或者用远程仓库覆盖本地的方式

```
git fetch --all
git reset --hard origin/master
git pull
```



每节课都会更新代码，本地的仓库也要更新

```
git pull
```



## 随堂视频

阿里云： https://www.aliyundrive.com/s/hvEfrRAAFFB

百度云： 链接: https://pan.baidu.com/s/1XuZPBl7a4SSxUjDZs9OQAQ 提取码: j541





## 案例

博客： http://www.young1024.com:3008/

maizuo : http://www.young1024.com:1234/vue/#/movie/now

点餐后台管理系统： http://www.young1024.com/dian/#/dashboard



