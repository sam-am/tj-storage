后台地址： 

​	mp.weixin.qq.com



登录小程序的管理后台

   版本管理（开发人员管理），数据查看， 基本信息查看或者修改。广告管理





## 版本管理

开发版本- 从开发工具提交的代码



审核版本 - 从开发版本中提交审核的

​	提交了审核 -   微信那边是人工审核，回复时间是不确定的

​	如果通过了，就可以发布到线上版本了

​	通过了也可以发布到体验版本，只有体验成员和开发人员才能访问到



线上版本，审核通过了，就可以发布到线上版本



## 成员管理

开发人员，体验成员， 运营成员







## 开发管理

### 开发设置

AppID  小程序id , 是必须要知道的，要用这个id进行开发

AppSecret ： 需要后端开发就需要这个秘钥了，出现这个秘钥需要保存好，如果丢了找不回来了，只能重置



### 服务器域名

如果在小程序的页面中需要调接口，那么接口域名就必须要配置这里，如果不配置就不给访问





## 广告

流量主：可以接别人的广告到你的小程序中展示； （前提：用户量要大于1k）

广告主： 可以花钱推广你的小程序；



## 设置

小程序的信息设置，名称，简称，头像，介绍

物料下载（小程序码）

也可以装让，注销

注意： 虽然可以修改，但是修改有词数限制







# 开发文档

https://developers.weixin.qq.com/miniprogram/dev/framework



### 文档必读

**指南 - 起步 - 目录结构 - 配置小程序 - 小程序框架  ！！！**

框架

组件 - 视图容器 - 基础内容- 表单组件 - 导航  - 媒体组件

API - 基础 - 路由 - 界面 - 网络 - 数据缓存

​		开放接口





## 创建项目



### 目录结构

cloudfunctions  云函数的文件夹

miniprogram  小程序的文件夹

​	app.js   小程序的入口文件

​	app.json  小程序的全局配置文件

​	app.wxss  全局的样式文件 （就是以前的css）

​	style  样式文件夹

​	pages 页面文件夹

​	images 图片

​	components 组件



### app.json

小程序的全局配置

pages  配置页面的路径，所有的页面路径都需要配置在这里，如果没有就显示不了，并且最开头的那个就是小程序打开时的页面

window  小程序**框架**的全局 样式设置， 背景色，顶部导航条..

tabbar 配置底部导航菜单



### 创建页面

没一个页面都需要放在一个文件夹中，这样好管理;

1. 通过开发工具，右键 ->  新建page  创建出一个页面，并且自行把创建出来页面路径添加到pages字段中
2. 直接在pages 字段中写需要新建页面的路径，也会自动帮我们创建出页面



一个页面 包含4个文件

.js  逻辑代码   `<script></script>`

.json 当前页面的配置

.wxml 页面结构文件   `html`

.wxss  当前页面的样式文件 `css`

​		以前怎么写css 的在wxss中也怎么，但是有些高级选择器不支持，支持列表中列出的



#### 组件(wxml)

在小程序的页面中所有的页面结构都需要写在 wxml中就是以前html文件，但是基本不能使用原生标签，都需要用到小程序它自己的组件

view  一个块级容器 像以前的div

text  用来显示文本，可以用来解析特殊字符，

​	space 可以配置显示空格

​	decode 配置是否解析 特殊字符  ， `&nbsp;`  `&lt;` ...



navigator  导航标签  相当于a标签

​	url  需要跳转到页面路径



image  显示图片的组件

​	默认尺寸是320*240  如果不指定图片模式，图片就会被拉升到这个尺寸  

​	mode 设置图片模式

​	aspectFit  保持比例

​	top 裁剪模式  图片会被裁剪，从顶部开始裁剪



input 输入框组件

​	type  设置组件的类型

​	password  是否是密码框

​	绑定事件  bindinput  输入后触发，然后绑定的函数会得到事件对象: e.currentTarget  e.target 都是元素对象， e.detail.value 是获取到输入框的值

##### 语法

**数据绑定 {{}}**

需要取到 data中的数据使用{{}} ,在{{}}中可以写js的表达式，但是也不能写js的与语句，  在属性中也可以使用 {{}} 取值



**列表渲染 wx:for**

可以遍历data中的数组，wx:for="{{数组}}"   默认就会 item和index 属性

如果要修改变量名和下标，就可以通过 wx:for-item  wx:for-index 修改

```
 <view wx:key="index" wx:for="{{arr}}">
    {{item.name}}-
    {{item.age}}
    :{{index}}
  </view>
```

```
<view wx:for-item="obj" wx:for-index="idx" wx:key="index" wx:for="{{arr}}">
    {{obj.name}}-
    {{obj.age}}
    :{{idx}}
 </view>
```



**条件渲染**

wx:if     wx:elif     wx:else

```
<view wx:if="{{score>=90}}">优秀</view>
<view wx:elif="{{score>=80}}">中等</view>
<view wx:elif="{{score>=60}}">一般</view>
<view wx:else>不及格</view>
```



### 样式文件 wxss

和以前的css 语法一样 ，页面中的.wxss是局部样式，只有在当前页面生效，如果最外层的app.wxss就是全局的样式

选择器，不支持高级选择器，只支持列表中的

![image-20211127094008751](assets/image-20211127094008751.png)



#### 单位

正常的单位都是可以用。px , pt ...

推荐使用是自有的单位，rpx  ,   使用效果和之前的rem差不多。会根据屏幕的大小改变尺寸。

如果你的设计稿是750px宽度的，就直接使用rpx单位就好了，会自行去适配

![image-20211127094211243](assets/image-20211127094211243.png)



### 事件系统

在小程序里面是没有点击事件的，都是触摸事件，绑定事件都是通过bind绑定的：bindtap   bindtouch...

绑定的函数 直接page的对象中

调用的函数不用加()

需要传值就需要使用 data- 绑定属性，然后通过e.currentTarget.dataset获取，或者e.target.dataset

```
<view class="helf" data-num="20" bindtap="helfclick"></view>
```

```
Page({
  helfclick:function(e){
    // 需要获取 这个传过来的值
    console.log(e.currentTarget.dataset.num)
    console.log(e.target.dataset.num)
    console.log('aaaaaaaaaaa')
  }
})
```



### 简易数据双向绑定

在小程序中的数据不是双向绑定的，是单向的，但是对于输入框来说有双向绑定的工能

```
<input class="inp" model:value="{{msg}}"></input>
```



### setData

用来修改data， data中值发生变化，视图也会更新

```
this.setData({
   msg:123
})
```



### .js文件

页面的逻辑代码，数据 、生命周期， 函数...



onLoad    页面一打开的时候就会执行 ， 有个 options的参数，可以获取到页面的传值

```
<navigator url="/pages/demo/demo?name=young&age=18">跳转到demo页面</navigator>
```

在demo.js中onload中的options就可以获取到传递的值

```
onLoad: function (options) {
    console.log(options)
},
```



onReady    已经把页面渲染好了

onShareAppMessage  分享页面的操作，如果把这个函数去掉了，那么这个页面不给分享了



## tabbar

```
"tabBar":{
    "color":"#F62132",
    "selectedColor":"#1266C3",
    "list":[
      {
        "pagePath":"pages/index/index",
        "text":"首页",
        "iconPath":"image/shouye1.png",
        "selectedIconPath":"image/shouye2.png"
      },
      {
        "pagePath":"pages/test/test",
        "text":"测试",
        "iconPath":"image/shoucang1.png",
        "selectedIconPath":"image/shoucang2.png"
      }
    ]
  },
```





## API

小程序提供的一些方法，可以帮助我们做一些特定的功能

### 路由

编程式导航

wx.switchTab   跳转到tabBar的页面

```
wx.switchTab({
	url: '/pages/index/index',
})
```



wx.redirectTo   替换当前页面（不会留下记录）

```
wx.redirectTo({
      url: '/pages/demo/demo',
    })
```



wx.navigateTo  跳转页面  （有记录）

```
wx.navigateTo({
      url: '/pages/demo/demo',
    })
```

**注意：redirectTo 和 navigateTo  都不能跳转到tabBar的页面**



### 界面

小程序提供的 ui交互界面  ，如：Toast ， loading ，弹出框.... 



showToast

如果有图标，title的最多7个，   没有图标 icon:none ，title就没有字数限制

```
wx.showToast({
      title: '这是提示1这是提示2这是提示3',
      icon: 'error'
    })
```



showModal   弹出一个对话框

```
 wx.showModal({
      title:"确定吗?",
      content:"真的不要吗?",
      cancelColor: 'cancelColor',
      success:function(res){
        console.log(res)
        if(res.cancel){
          console.log('点了取消')
        }else if(res.confirm){
          console.log('点了确定')
        }
      },
      fail:function(){
        console.log('错误')
      }
    })
```



### 网络

小程序提供的 请求方法

wx.request   发起一个请求

```
wx.request({
      url: 'https://www.young1024.com:3002/news',
      method:'POST',
      data:{
        type:'NBA'
      },
      success:function(res){
        console.log(res)
      },
      fail:function(){
        console.log('请求失败')      
      }
    })
```



wx.uploadFile  上传文件的方法





### 数据缓存

就是操作localstorage的方法



wx.setStorage()   异步操作, 操作结果在success 回调函数中

```
wx.setStorage({
      key:'name',
      data:'young',
      success:function(){
        console.log('存成功了')
      }
    })
```

wx.getStorage()   从本地存储取值

```
wx.getStorage({
      key:'name',
      success:function(res){
        console.log(res)
      }
    })
```



wx.removeStorage  删除本地存储的值

```
wx.removeStorage({
      key: 'name',
    })
```

