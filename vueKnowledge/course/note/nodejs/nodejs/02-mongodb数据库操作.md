1. 先登录数据库

   ```
   mongo
   ```

   

## 操作

查看所有数据库

```
show dbs
```



创建数据库

```
// use <数据库名称> 
use dl2104   //使用use命令创建的数据库不会出现在列表中，需要插入一条数据才会被创建

// 插入一条数据
db.dl2104.insert({name:'aa'})
```



删除数据库

```
use <数据库>
db.dropDatabase()
```



> 一个数据，可以用个表格文件文件来表示
>
> 一个集合表示一个字表
>
> 每一行的数据，代表是文档





查看数据里面的集合

```
use <数据库>
show collections
```



创建集合

```
db.createCollection('stus')
```



删除集合

```
db.<集合名称>.drop()
```





插入数据

```
db.<集合名称>.insert({<数据>})
db.stus.insert({name:'yy1'})
db.stus.insert({name:'yy2'})

// 插入数据，mongodb会自动插入 _id字段，是唯一的一个id
```



查找

```
db.<集合名称>.find()
db.stus.find()
```

-- 加条件查找

```
db.<集合名称>.find({<查找条件>})

db.stus.find({name:'yy2'})
```



修改数据

```
db.<集合名称>.updata({<查找条件>},{修改的数据}) 
//找到了多个，第一个会被修改，然后数据会被替换

db.stus.update({name:'yy3'},{age:20})

db.<集合名称>.update({<查找条件>},{$set:{<修改的数据>}})  //不会被覆盖替换
db.stus.update({name:'yy3'},{$set:{age:20}})

```



删除数据

``` 
db.<集合名称>.remove({<查找条件>})

db.stus.remove({age:20})  //会把age=20的数据都删掉
```



查找限制条数

```
db.<集合名称>.find().limit(<限制的条数>)

db.stus.find().limit(5)
```



跳过条数

```
db.<集合名称>.find().skip(n)  // 跳过n条后的数据都找出来

db.stus.find().skip(5)
```



分页

limit()和skip()结合使用就可以做出分页的效果

```
// 一页的条数
// 需要第几页的数据

//  一页的数据为3条(n)，需要第二页的数据（p）
// 	db.stus.find().limit(n).skip((p-1)*n)

db.stus.find().limit(3).skip((2-1)*3)
```



排序

```
db.<集合名称>.find().sort({<排序的字段>:1})  1-升序   -1就是降序

db.stus.find().sort({age:1})  // 按照年龄从小到大
db.stus.find().sort({age:-1})  // 按照年龄从大到小
```



查找的结果限制显示的字段

```
db.<集合名称>.find({},{<需要显示的字段:1>}) 
//  {}空对象代表查找所有，
// 	第二个参数是需要限定需要显示的字段  1 显示   0不显示
```





高级查询

https://www.yiibai.com/mongodb/mongodb_query_document.html

```
db.<集合名称>.find({<需要查找的字段>:{高级查询的条件}})

db.stus.find({age:{$gt:25}})  //大于25

db.stus.find({age:{$lte:20}})  //小于等于20的


$lt 小于
$lte 小于等于

$gt 大于
$gte 大于等

$ne 不等于
```



查找出来的格式化显示

```
db.stus.find().pretty()
```

在命令行中如果如果 大于20条，也只显示20条 ，需要敲 it  ，再显示更多







## 练习

创建学生数据库，有个学生集合

创建不小于30条的学生信息

学生信息的字段包括：
姓名  学号  性别 爱好 年龄  地区  身高 体重  学科  艺名  手机号





操作：

查询升高高于160

查询升高高于160女生

查询的来大连的学生

查询有没有’张三‘

查询体重小于 50的

查询体重小于50的女生

查询体重大于60的男生

查询H5学科的学生





## 预习

http://www.mongoosejs.net/