Node.js

运行在服务端的JavaScript

v8引擎

特点： 事件驱动、非租塞I/O



## 1.安装

下载地址： http://nodejs.cn/download/

版本要求：

​	win7 :  v14 +

​	win10： v16+ 

（不是要必要的，只要保持v14以上的版本就可以了--因为后面学习的脚手架工具对node就版本要求）  



检查版本：

```
node -v
// v16.4.2
```



## 2.npm

包管理工具，安装了node之后自带有npm



NPM(node package manager)是随同NodeJS一起安装的包管理工具，能解决NodeJS代码部署上的很多问题.

[Node](https://www.npmjs.com/)[模块（包）平台：](https://www.npmjs.com/)https://www.npmjs.com/

Npm作用和使用场景：

- 允许用户从NPM服务器下载别人编写的第三方包到本地使用。

- 允许用户从NPM服务器下载并安装别人编写的命令行程序到本地使用。

- 允许用户将自己编写的包或命令行程序上传到NPM服务器供别人使用。



检查npm的版本

```
npm -v
// 7.22.0
```

npm的版本就不要随意更新了，npm是对node的版本是有要求的。（一般默认版本就可以了）

```
npm i -g npm  // 更新npm命令
```



### 常用命令

### **初始化项目**

```
npm init
```

**对话**

package name: (demo)     // 当前创建的包名叫什么 , 如果和括号里面的名称相同，敲回车就可以，如果不同，就输入你的包名

version: (1.0.0)   // 版本号

description:   // 项目的描述

entry point: (index.js)   // 项目的入口文件

test command:   // 测试命令

git repository:    // git 仓库

keywords:   // 当前项目的关键词

author:    // 作者

license: (ISC)   // 协议



以上的对话完成之后，在当前的项目目录中就会生成一个package.json

package.json  是项目的描述文件（上面的对话，都在这里面）



如果觉得以上的对话太繁琐了，而且你所有的对话都可以是默认选项，则可以跳过这个对话

```
npm init -y
```



**package.json**

```
Name	 - 包名。
Version	 - 包的版本号。
Description  - 包的描述。
Author	 - 包的作者姓名。
contributors - 包的其他贡献者姓名。
Dependencies	 -项目应用运行依赖模块。如果依赖包没有安装，npm 会自动将依赖包安装在 node_module 目录下。
devDependencies	 -项目应用开发环境依赖
Repository	 - 包代码存放的地方的类型，可以是 git 或 svn，git 可在 Github 上。
main 	- main 字段是一个模块ID，它是一个指向你程序的主要项目。就是说，如果你包的名字叫 express，然后用户安装它，然后require("express")。
keywords 	- 关键字
licenses	 -授权方式
directories	 -目录
```



### 安装包（下载库文件）

**本地安装**

通常都是用来项目的依赖

```
npm install <库文件名称>

简写： npm i <库文件名称>
```

```
npm i jquery
```

```
npm i jquery@2.2
// 指定下载库文件的版本
```

下载好了之后，就会在你的目录中，多了一个文件夹 node_modules,  里面就是放着你下载好的库文件



**全局安装**

通过是用来安装的工具的，vue-cli 、cnpm、 pm2、 create-react-app

```
npm install @vue/cli -g
npm install -g @vue/cli
```

ps：苹果电脑需要注意权限问题：

要切换到管理员权限再进行全局安装

```
sudo -i
```





### 淘宝镜像

因为npm 的服务器是在国外，由于网络原因，我们在国内就可能会下载速度比较慢，也可能下载不下来。

就需要切换到淘宝镜像

淘宝服务器每10分钟就去npm的服务器上同步一次（把npm上的库文件都复制到淘宝服务器上）

我们切换了淘宝镜像后，就从国内的淘宝服务器下载了，这样速度就会比较快了

```
设置：npm config set registry https://registry.npm.taobao.org
查看: npm config get registry
还原：npm config set registry https://registry.npmjs.org/
```



淘宝镜像工具

```
npm i -g cnpm
```

npm能做的事情，cnpm基本都能做，而且直接走的淘宝镜像路径

（个人不建议使用cnpm，因为下载来的库文件路径稍微有点奇怪）



### 查看当前已安装

```
npm ls  // 本地的已安装
npm ls -g  // 全局的已安装
```



### 安装库的时候写入到package.json

```
npm i jquery --save
// npm i jquery -S

npm i swiper --save-dev
// npm i swiper -D
```

--save是把库写dependencies 字段中  (新版本的npm自动就会加上这个参数)

--save-dev 是把库写到 devDependencies



把依赖写入到package.json中，如果要把项目给别人，别人看到package.json就知道你的项目使用哪些库了，既然库文件都是从npm下载的，，拷贝给别人，也不需要拷贝node_modules中的文件

### 项目复活

npm i 会读取package.json中dependencies和devDependencies的字段，把相关依赖下载下来

```
npm i
```



### 版本号

> 只需要了解，不需要操作
>

使用NPM下载和发布代码时都会接触到版本号。NPM使用语义版本号来管理代码

Semantic-Versioning 语义化版本控制分为X.Y.Z三位，分别代表主版本号、次版本号和补丁版本号。当代码变更时，版本号按以下原则更新。

如果只是修复bug，需要更新Z位。

如果是新增了功能，但是向下兼容，需要更新Y位。

如果有大变动，向下不兼容，需要更新X位。

版本号有了这个保证后，在申明第三方包依赖时，除了可依赖于一个固定版本号外，还可依赖于某个范围的版本号。例如"argv": "0.0.x"表示依赖于0.0.x系列的最新版argv。

一个标准的版本号必须是X.Y.Z的形式，X是主版本，Y是副版本，Z是补丁版本。.

X: 代表发生了不兼容的API改变

Y: 代表向后兼容的功能性变化

Z: 代表向后兼容bug fixes

语义化版本号规则

X.Y.Z - A.B.C 连字符范围

1.2.3 - 2.3.4 等价于 >=1.2.3 <=2.3.4 1.2.3 - 2 等价于 >=1.2.3 <3.0.0

~1.2.3 波浪线范围

~1.2.3 等价于 >=1.2.3 <1.(2+1).0 等价于="">=1.2.3 <1.3.0
 ~1.2 等价于 >=1.2.0 <1.(2+1).0 等价于="">=1.2.0 <1.3.0 (Same as 1.2.x)
 ~1 等价于 >=1.0.0 <(1+1).0.0 等价于 >=1.0.0 <2.0.0 (Same as 1.x)

npm社区：https://www.npmjs.com/



### index.js

创建的项目不会帮我们创建这个文件，需要我们手工创建

index.js

```
console.log('我是index.js里面的内容！！！')
var a = 1; 
var b = 2;
console.log(a+b)
```



**执行index.js**

在项目目录的命令行窗口中执行： 

```
node index.js
// node index  // 默认就找.js文件， 可以把后缀省略
```





## 全局变量

官方文档： http://nodejs.cn/api/

node提供的内置模块都在这个文档中可以找到



**__dirname**

```
// 获取当前执行文件的所在目录
console.log(__dirname) 
/Users/zlyoung/Desktop/dl2104/code/node/day01/demo
```

**__filename**

```
// 当前文件的完整路径
console.log(__filename)
/Users/zlyoung/Desktop/dl2104/code/node/day01/demo/index.js
```



**require()**

引入一个模块

```
var jq = require('jquery');
var xx = require('./xx.js')
```



**console**

打印到控制台

```
console.log()
console.dir()
```



**module**

导出模块

```
module.exports = {} 
```

 **exports**

也是导出模块，但是只是module.exports的一个引用

```
exports.add = function(){}
exports.num = 10;
```

**定时器**

```
setInterval()
setTimeout()
```



## path

path.dirname('path')

```
const path = require('path')
```

```
// 返回__dirname的父级文件夹
console.log(path.dirname(__dirname))
```



path.basename('path')

```
// 当前所在的文件
console.log(path.basename(__filename))
```



**path.join()**

​	// 路径拼接 需要拼接的路径传在 join()方法参数中就可以了
​			// 在不同的系统下，路径可能不一样的； 有的是 /代表下一级，而有的是 \代表下一级
​			//   E:\VSC\Vue\node\day1\a\b
​			//   /Users/zlyoung/Desktop/dl2104/code/node/day01/demo/a/b

```
console.log(path.join(__dirname,'a','b'))
```



## fs 文件系统

和文件读取信息，写入... 都要用到这个库文件

```
const fs = require('fs')
```

### fs.stat()

读取文件的属性信息

参数1. 路径

参数2.回调函数  

​		形参1：错误信息

​		形参2： 读取到的文件信息

```
fs.stat('./test.txt',function(err,info){
	console.log(err)
	// 判断是否是一个文件
	console.log(info.isFile())
})
```

```
fs.stat('./node_modules',function(err,info){
	console.log('--------')
	console.log(info.isFile())

	// 判断是否是一个文件夹
	console.log(info.isDirectory())
})
```

isFIle() // 判断是否是一个文件

isDirectory()	// 判断是否是一个文件夹



### readFile()

读取文件内容的方法

```
//参数1： 需要读取的文件路径
//参数2： 解析编码
//参数3： 回调函数
fs.readFile('./test.txt','utf8',function(err,info){
	console.log(err)
	console.log(info)
})
```

```
// 如果不传解析编码，得到的是一个Buffer文件
// 可以通过toString()方法转换
fs.readFile('./test.txt',function(err,info){
	console.log(err)
	console.log(info.toString())
})
```



### writeFile()

写入文件

```
// 会覆盖
fs.writeFile('./test.txt','abc123',function(){
	console.log('ok')
})
```

- r+ 打开文件用于读写。

- w+ 打开文件用于读写，将流定位到文件的开头。如果文件不存在则创建文件。

- a 打开文件用于写入，将流定位到文件的末尾。如果文件不存在则创建文件。

- a+ 打开文件用于读写，将流定位到文件的末尾。如果文件不存在则创建文件。

```
fs.writeFile('./test.txt','我是写在最后的内容',{flag:'a+'},function(){
 	console.log('ok')
})
```



添加到最后

```
fs.appendFile('./test.txt','2',function(){
	console.log('aa')
})
```



### 删除文件

```
// 删除文件
fs.unlink('./a.txt',function(){
	console.log('del')
})
```



### 大文件复制

```
var fs = require('fs');

// 复制小文件
// fs.readFile('./test.txt',function(err,info){
// 	fs.writeFile('./a.txt',info,function(){
// 		console.log('a')
// 	})
// })

// 创建读的流（它只管读）
var reader = fs.createReadStream('./test.txt');

// 创建写的流（它只管写）
var writer = fs.createWriteStream('./a.txt');

// reader读到的流，通过pipe 写出
reader.pipe(writer);
```





## crypto

加密

### md5

MD5特点

•不可逆；

•不管加密的内容多长，最后输出的结果长度都是相等的；

•内容不同输出的结果完全不同，内容相同输出的结果完全相同。



加密步骤

```
// 1
var crypto = require('crypto');

// 2.创建md5加密对象
var md5 = crypto.createHash('md5')

// 3.使用md5对象进行加密 
var md5sum = md5.update('hello 2103aaaaaaaaaaaaaaaaaa');
// md5sum 是一个Hash对象，我们是看不懂的


// 4.通过digest()返回16进制的 加密后的字符串
// hex 是固定写法，
var result = md5sum.digest('hex');
console.log(result)
```

 

```
var result2 = crypto.createHash('md5').update('2104 aaa').digest('hex')
console.log(result2)
```

**注意**   digest()一个加密对象只能用一次



### sha256

sha256的加密方式
		需要有一个你的密码，然后再用你的密码去加密你要加密的字符串

```
// 1
var crypto = require('crypto');


// sha256的加密方式
// 需要有一个你的密码，然后再用你的密码去加密你要加密的字符串

// 2. 创建sha256的加密对象 , 需要加入你的密码
var sha = crypto.createHmac('sha256','yy321');


// 3.加密  返回一个Hmac对象，我们也是看不懂的
var shaSum = sha.update('2104--');

// 4.通过digest()返回16进制的 加密后的字符串
var result = shaSum.digest('hex')
```

```
var r2 = crypto.createHmac('sha256','yy321').update('hello').digest('hex')
console.log(r2)
```



## express

创建服务器的第三方框架

### 安装

```
npm i express --save
```

### 创建服务

```
var express = require('express');

// 是一个对象也是一个函数
// 函数：会创建出一个可以管理服务器的对象，比如：监听端口
// 对象：可以提供一些中间件（中间件后面讲）
var app = express();

app.get('/',function(req,res){
	res.send('这是首页内容')
})

// 监听服务器端口
app.listen(2104,function(){
	console.log('服务器启动了...')
})
// 本机地址
// localhost
// 127.0.0.1
```



创建get、post路由

```
var express = require('express');
var app = express();

// 所有通过浏览器地址可以访问到的，都是get请求

// get() 设置一个get路由
// 参数1 ：路由的路径
// 参数2 ： 是一个回调函数：
//	req 所有请求相关的都在这个对象里面，比如参数，路径...
//  res 所有需要响应的都通过这个对象响应
app.get('/',function(req,res){
	res.send('这是首页内容')  //返回一个字符串给客户端
})

// post() 设置一个post()路由
app.post('/test',function(req,res){
	res.send('post返回-回来的数据')
})

// 监听服务器端口
app.listen(2104,function(){
	console.log('服务器启动了...')
})

```



### Request

所有请求的数据都可以通过这个对象获取到

req.params

```
// 获取路由参数
app.get('/test1/:uid',function(req,res){
	// 所有请求数据都在这个req对象里
	console.log(req.params)
	res.send('ok')
})
```



req.query

```
// 获取get表单类型的数据  ？后面的数据
// /test2?name=young&age=18
app.get('/test2',function(req,res){
	console.log(req.query)
	res.send('test2')
})
```



req.body

获取post 方式提交过来的数据 ，前提要使用一个中间件 urlencoded({extended:trueå})

```
// 如果要接受到post提交的数据就必须要配置下面的这个中间件
app.use(express.urlencoded({extended:false}))
```

```
app.post('/test',function(req,res){
	// req.body不能直接获取到post提交过来的数据的
	// 需要配置一个中间件，才可以获取到post方式提交过来的数据
	console.log(req.body);
	res.send('post-test')
})
```



### 设置静态目录

```
// 设置目录
// 在静态目录中的文件，都可以通过地址栏访问到
app.use(express.static(path.join(__dirname,'public')))
```



### Response

所有要从服务响应回去都要通过这个对象

#### res.send() 

返回字符串，

```
app.get('/test',function(req,res){
	// 返回字符串
	// 如果返回的标签，标签会被解析
	res.send('<h1>index</h1>')
})
```



#### res.json

返回json格式的数据，（通常是用来写接口的，因为接口返回的数据都是json格式的）

```
app.get('/test1',function(req,res){
	// 返回json格式的数据，也就是一个对象
	res.json({
		code: 200,
		msg: '请求成功',
		data: [1,3,4,2,5,7,8,3]
	})
})
```



#### res.redirect

重定向

```
// 重定向，访问/test2 跳转到 /
app.get('/test2',function(req,res){
	res.redirect('/')
})
```



#### res.sendFile

返回一个文件内容

```
// 返回一个文件内容
app.get('/test3',function(req,res){
	// 需要传入一个文件的路径
	res.sendFile(path.join(__dirname,'demo.txt'))
})
```



### res.download

下载文件

```
// 下载文件
app.get('/test4',function(req,res){
	// 需要传入一个文件路径，访问这个路由这个文件将会被下载
	res.download(path.join(__dirname,'demo.txt'))
})
```



### res. render

返回模板  (前提是设置了模板引擎和模板目录)

参数1: 模板的名称

参数2：返回到模板的数据

```
app.get('/test5',function(req,res){
	res.render('test',{
		name:'young',
		age: 18,
		desc:'<h2>这是标签的内容</h2>',
		arr: [
			{name:'y1',age:19},
			{name:'y2',age:20},
			{name:'y3',age:16},
			{name:'y5',age:19},
			{name:'y6',age:22}
		]
	})
})
```





## ejs

是一种模板引擎

1.安装

```
npm i ejs --save
```



2.设置

```
// 设置模板引擎
app.set('view engine','ejs');

// 设置模板目录
app.set('views',path.join(__dirname,'views'))
```



ejs的模板标签

<%- %> 解析输出； 会把html 解析后输出

<%= %>原样输出；会把html当做字符串输出

<%  %> 脚本标签，不输出，用于流程控制

```
<div>
	- 解析输出，会把html标签解析后再输出
</div>
<%- desc %>

<div>
	= 是原样输出，会把html当做字符串输出
</div>
<%= desc %>
```

遍历数组

```
<div>
		<div>遍历一个数组 需要通过脚本标签</div>
		<ul>
			<% for(var i=0; i<arr.length; i++){ %>
				<li>
					<%- arr[i].name %>
					<%- arr[i].age %>
				</li>
			<% } %>
		</ul>
</div>
```





**包含**

可以把页面中公共的部分(头部，脚部...)抽离出来，通过include包含进来

```
<%- include('./header.ejs') %>
```





# mongodb

非关系型的数据库（也是最像关系型数据的）

noSQL , 不想需要专门的语法去操作数据库

​	mysql oracle ,sqlserver  这些都是关系型的数据，都需要用sql语句去操作数据库

而mongodb不需要,  用的都是js语法，只要记住对应的操作方法就可以了





## 1.下载安装

https://www.mongodb.com/try/download/enterprise



安装的过程中，最好不要切换路径，如果切换了一定先记住这个路径在哪里

还有就是compass不要勾选安装

![image-20211122093551187](assets/image-20211122093551187.png)







