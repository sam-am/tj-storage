# session

会话

多个路由之间可以共享数据，和之前的cookie基本是一样的，cookie是存在客户端（浏览器），而session是存在服务端的（服务器）。



## express-session

### 1.安装

```
npm i express-session --save
```

## 2.导入

```
var session = require('express-session')
```

session是一个方法，可以生成一个中间件，方法的参数可以配置我们的session 参数

### 3.使用session中间件

```
app.use(session({
	secret:'young',
	cookie:{
		maxAge: 1000*60*60  //设置过期时间 单位：毫秒
	}
}))
```

配置了中间之后，就可以通过 req.session.xx = xx 去设置session

### 4.登录用户保存在session

```
1.验证允许登录的用户,登录成功的用户信息保存在session

2.req.session.user = users[0]
```

### 5. 保存在session的用户信息，在模板取到

可能有多个模板都要需要用户信息的，如果通过render返回的话，重复代码就比较多，就不适合开发操作， 可以通过 中间件 + res.locals 去解决

```
app.use(function(req,res,next){
	res.locals.user = req.session.user
	next()
})

// 在每个模板里面都有 从session.user 设置的值
```

