# pm2

```
npm i -g pm2
```



启动一个服务

```
pm2 start index.js
```

启动了服务后会在后台运行，不会关闭



启动一个服务，并且监听文件改动自动重启

(只要在这个项目里面有文件改动，都会重启，包括上传文件)

```
pm2 start index.js --watch
```







查看已经启动的项目

```
pm2 list
```



重启项目

```
pm2 restart index.js

pm2 restart id
pm2 restart name
```



查看控制台

```
pm2 log
```



查看资源状态

```
pm2 monit
```

