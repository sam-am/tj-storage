mongoose

操作mongodb数据库的一个驱动



两个概念：

Schema  //只是和数据库做一一对应的模型

Model   // 可以操作数据的一个实例

## 使用方法

### 安装

```
npm i mongoose --save
```

### 连接

```
var mongoose = require('mongoose');  

// 连接数据库
mongoose.connect('mongodb://localhost:27017/students')  

// 监听连接事件
mongoose.connection.on('disconnected',function(){
	console.log('数据库断开')
})

mongoose.connection.on('connected',function(){
	console.log('已经连接成功')
})

mongoose.connection.on('connecting',function(){
	console.log('正在连接')
})

mongoose.connection.on('disconnecting',function(){
	console.log('正在断开')
})
```

### 创建schema

需要保存什么字段，就需要在schama里面配置什么字段

schema参数1：对应的字段

参数2：指定集合

```
var stuSchema = mongoose.Schema({   //创建了一个数据库模型
	name:String,
	age:Number,
	height:Number,
	sex:String
})
```

### 创建Model

model的第一参数是对应数据库的集合（复数的形式）, 

```
var stuModel = mongoose.model('stu',stuSchema); //创建了stu Model 这个对象可以用来操作数据库了
```

### 保存操作

```
var stu = new stuModel({  //创建model实例
	name:'yy2',
	age:19,
	height:170,
	sex:'men'
})

stu.save(function(err,info){  // 保存操作
	console.log(err)
	console.log(info)
})
```

### 查询

#### find()

参数1：查询条件

参数2：只显示你需要显示的字段， 字段名称之间用空格隔开

参数3：回调函数  

​		参数1：err 查找失败的错误信息

​		参数2：stus是查找到信息

```
stuModel.find({},'name age',function(err,stus){
	console.log(stus)
})
```

#### findById()

根据ID查找

参数1就是id

```
stuModel.findById('6138731b273054b67b7a8e66',function(err,stu){
	console.log(stu)
})
```

### 删除

#### remove()

参数1：查找条件

```
stuModel.remove({name:'yy1'},function(err,info){
	console.log(err)
	console.log(info)
})
```

#### findByIdAndRemove()

根据Id删除

```
stuModel.findByIdAndRemove('6138734d50847548a8b7d400',function(err){
	console.log(err)
})
```

### 修改

#### update

参数1：查找条件

参数2： 修改的值

参数3：回调函数

```
stuModel.update({name:'yy3'},{age:20},function(err,info){
	console.log(err)
	console.log(info)
})

```

#### findByIdAndUpdate

根据ID修改

```
stuModel.findByIdAndUpdate('613877f1c47e87fd34f9d70c',{age:22},function(err){
	console.log(err)
})
```

