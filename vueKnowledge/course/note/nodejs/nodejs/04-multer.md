# Multer

用来上传文件的中间件

注意：表单一定得是 enctype="multipart/form-data"  类型的





## 1.安装

```
npm i multer --save
```

## 2.导入

```
var multer = require('multer')
```



## 3.配置

1.上传文件的保存路径

2.上传后的文件名

```
// m-2 
var storage = multer.diskStorage({
	destination:function(req,file,cb){ // 设置保存路径
		cb(null,path.join(__dirname,'public'))
	},
	filename:function(req,file,cb){ //设置保存的文件名
		console.log(file)
		/*
			{
			  fieldname: 'avatar',  //表单的name属性
			  originalname: 'FgB4yALyW_NCyo-aXF66BXFatl23.jpeg', //原始文件名称
			  encoding: '7bit',  // 编码
			  mimetype: 'image/jpeg'  // 文件类型
			}
		*/ 
		// Date.now()
		//03.aa.ccc.aa.bb.66.jpeg
		//[03,aa,ccc,aa,bb,66,jpeg]

		// 时间戳+后缀 组成的文件名
		var hz = file.originalname.split('.');
		cb(null, Date.now()+'.'+hz[hz.length-1])
	}
})
// 创建中间件
var upload = multer({storage:storage})
```

## 4.在路由中使用

当前我们就是上传一个文件，所以就用singel方法就可以了

```
// single 只上传一个文件
// singel() 里面的参数要和表单的name属性一致
app.post('/save',upload.single('avatar'),function(req,res){
	console.log(req.file)
	/*
		{
		  fieldname: 'avatar',  //表单name属性
		  originalname: '1629769905309.png',  //原始文件名
		  encoding: '7bit', //编码
		  mimetype: 'image/png', //文件类型
		  destination: '/Users/zlyoung/Desktop/xa2107/node/day05/peject/public',
		  // 上传后的路径
		  filename: '1631867774924.png',// 上传后的文件名
		  path: '/Users/zlyoung/Desktop/xa2107/node/day05/peject/public/1631867774924.png',
		  // 上传后的完整路径
		  size: 14472  // 文件大小
		}
	*/
	res.send('ok')
})
```

表单

```
<form action="/save" method="post" enctype="multipart/form-data">
		<input type="file" name="avatar">
		<input type="submit">
</form>
```

