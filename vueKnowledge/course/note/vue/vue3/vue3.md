# 介绍

官网： https://v3.cn.vuejs.org/

是vue2升级版，大多数内容都是可以和vue2一样的，应为他是做想下的兼容，然后他有新增的api, 也有弃用了的。

Vue3新特性：

数据双向绑定的方式改变了，改成了Proxy

增加了setup函数

增加了hooks

支持ts

支持jsx

对性能提高 1.2 -2  倍

对vue2做了兼容（不能在使用放弃了api）



# 开始vue3

使用脚手架工具创建项目的时候选择vue3

```
vue create demo
```



![image-20220602093840072](assets/image-20220602093840072.png)



> 项目创建好之后，使用的路由插件是4.0版本，  和之前3.x的版本大部分是一样的，不一样的地方可以先看一遍文档，再使用。    或者遇到某一个路由问题再去详细的看文档。
>
> https://router.vuejs.org/zh/index.html



1.创建vue实例的时候，不是通过new了，而是createApp这个方法来创建

2.挂载点也不是通过el属性了，而是通过mount()方法

3.插件的使用方式不是通过属性来加载，而是通过use()方法来使用



## 初体验

HomeView.vue

setup函数

```
<template>
  <div class="home">
    <img alt="Vue logo" src="../assets/logo.png">
    <div>
      {{ obj.count }}
      <div>
        <button v-on:click="add">++</button>
      </div>
      <hr/>
      <div>
        {{ name }}
        <button @click="change">change</button>
      </div>
    </div>
  </div>
</template>

<script>
import { reactive } from 'vue'   // 导入创建响应式对象的方法
export default {
  name: 'HomeView',
  setup(props){ // setup函数
    const obj = reactive({count:0}) // 创建一个响应式对象
    return {  // 在setup函数里面返回的东西, 就可以在外面使用了
      name:'young',
      obj,
      add(){  // 修改响应式对象 (会更新到视图)
        // console.log('aaaaaaaaaaaaaaa')
        obj.count++
        console.log(obj.count)
      },
      change(){  // 修改非响应式对象 (不会更新到视图)
        // console.log('aaaaaaaaaaaaaa')
        // console.log(this.name)
        this.name = 'yyds'
        console.log(this.name)
      }
    }
  }
}
</script>
```



## vscode插件

vue3的插件名称： volar

安装完成之后要重启vscode

还要禁用vue2的插件 vetur



## setup函数

setup 函数是vue3里面新加的钩子

接受两个对象，props ,  context

可以返回一个对象，返回的这个对象就可以在外面调用了，比如： 数据，方法



是在 实例的创建挂载前执行的



## reactive/shallowReactive

```
<template>
  <div class="home">
    <div>
        {{ state.user.name }}
        <div> {{ state.user.hobby.h1}} </div>
        <div> {{ state.user.hobby.h2}} </div>

        <div>
          <button @click="change">change</button>
        </div>

        <hr/>

        <div>
          {{user.user.name}}
          <div>{{user.user.hobby.h1}}</div>
          <div>{{user.user.hobby.h2}}</div>
          <div>{{user.age}}</div>
          <div>
            <button @click="change2">change2</button>
          </div>
        </div>


    </div>
  </div>
</template>

<script>
// reactive 创建深层次的响应式对象
// shallowReactive  创建非深层次的响应式对象
import { reactive, shallowReactive } from 'vue'   // 导入创建响应式对象的方法
export default {
  name: 'HomeView',
  setup(props){ // setup函数
    const state = reactive({
      user: {
        name: 'young',
        hobby:{
          h1:'code',
          h2:'music'
        }
      }
    })

    const user = shallowReactive({  //非深层次的响应式对象
      age:18,  // 只有第一层数据修改的时候才会触发视图的更新,
      user: {  // 第二层往后的数据修改了,就不会触发视图的更新
        name:'yy',
        hobby: {
          h1: 'jump',
          h2: 'rap'
        }
      }
    })
    
    return {
      state,
      user,
      change(){
        state.user.name = 'yyds';
        state.user.hobby.h1 = 'run';
        state.user.hobby.h2 = 'study';
      },
      change2(){

        user.age++  ;
        // user.user.name = 'yyyyyyyyyyyyyyyyy'
        // 如果上面两句一起执行, 因为age是第一层数据,所以修改就会触发视图的更新
        // 第二句是修改第二层的数据,本来是不会触发视图的更新的,
        // 但是age更新,他也会跟着一起更新
        // console.log(user.user.name)
      }
    }
  }
}
</script>

```



## ref

 用来创建基本数据类型的响应式对象
		要在js中获取值,要通过.value来获取
		修改值也是要通过.value

```
<template>
  <div class="home">
    <div>
      <h2>{{num}}</h2>
      <div>
        <button @click="add">num++</button>
      </div>
      <hr/>
      <div>
        {{user.name}}:::{{user.age}}

        <div>
          <button @click="change">change</button>
        </div>
      </div>

    </div>
  </div>
</template>

<script>

import { ref } from 'vue'
// 用来创建基本数据类型的响应式对象
// 要在js中获取值,要通过.value来获取
// 修改值也是要通过.value
export default {
  name: 'HomeView',
  setup(props){ // setup函数
    let num = ref(0)
    let user = ref({
      name:'young',
      age:18
    })

    return {
      num,
      user,
      add(){
        num.value++
        console.log(num.value)
      },
      change(){
        user.value.name = 'yyds',
        user.value.age++
      }
    }
  }
}
</script>

```



## isRef/isReactive

用来判断是哪种响应式数据创建

```
<template>
  <div class="home">
    <div>{{state.name}}</div>
    <div>{{num}}</div>
    <div>
      <button @click="test">判断</button>
    </div>
  </div>
</template>

<script>

import { ref, reactive, isRef, isReactive } from 'vue'
// isRef 是用来判断是否是 ref 创建的
// isReactive 用来判断是否是 reactice 创建的
export default {
  name: 'HomeView',
  setup(props){ // setup函数
  
    const state = reactive({
      name:'young'
    })
    const num = ref(0)

    return {
      state,
      num,
      test(){
        console.log(isRef(num))
        console.log(isRef(state))
        console.log('---------------------------')

        console.log(isReactive(num))
        console.log(isReactive(state))
      }
    }

  }
}
</script>
```



## toRefs

reactive创建的对象，如果使用扩展运算符...再返回，就会失去响应式的效果

如果通过toRefs转换后, 再展开返回，修改state的时候，页面中也还有响应式的效果

```
<template>
  <div class="home">
    <!-- <div>{{name}}</div>
    <div>{{age}}</div>
    <div>
      <button @click="change">++</button>
    </div> -->

    <!-- <hr/>
    <div>{{user.uname}}</div>
    <div>{{user.uage}}</div> -->

    <hr/>
    <div>{{name}}:::{{age}}</div>
    <div>
      <button @click="test">++</button>
    </div>

  </div>
</template>

<script>

import { ref, reactive, toRefs } from 'vue'

export default {
  name: 'HomeView',
  setup(props){ // setup函数

    const state = reactive({
      name:'young',
      age: 18
    })

    const user = ref({
      uname:'yy',
      uage: 28
    })

    let obj = toRefs(state)  // 把state把他转换成ref创建的
  
    return {
      // ...state,  // 通过...展开的reactive对象, 就会失去响应式效果,
      // user,
      ...obj,
      // change(){
      //   state.age++
      //   console.log(state)
      // }
      test(){   // 此时对state修改也会响应到页面中,因为使用toRef进行了转换
        state.age++
      }
    }

  }
}
</script>

```



## readonly

创建只读对象（不能被修改）

```
<template>
  <div class="home">
    <div>{{state.name}}</div>
    <div>{{state.age}}</div>
    <div>
      <button @click="change">++</button>
    </div>
    
    <hr/>

    <div>{{aa.name}}</div>
    <div>{{aa.age}}</div>

     <div>
      <button @click="change2">test  ++</button>
    </div>

    <hr/>
    <div>{{count.count}}</div>
    <div>
      <button @click="change3">change3</button>
    </div>
  </div>
</template>

<script>

import { reactive, readonly } from 'vue'

export default {
  name: 'HomeView',
  setup(props){ 

    const state = reactive({
      name:'young',
      age: 18
    })

    let aa = readonly(state) // 使用readonly处理的对象是不能修改的的,修改就会报警告
    let count = readonly({count:0})  // 普通对象

    return {
      state,
      aa,
      count,
      change(){
        state.age++
      },
      change2(){
        aa.age++
      },
      change3(){
        console.log(count.count++)
      }
    }

  }
}
</script>

```



## shallowReadonly

第一层数据就不能修改,第二层往后的数据就可以修改

```
<template>
  <div class="home">
    <div>{{state.name}}</div>
    <div>{{state.age}}</div>
    <div>
      <button @click="change">++</button>
    </div>

    <hr/>

    <div>{{aa.name}}</div>
    <div>{{aa.age}}</div>
    <div>{{aa.hobby.h1}}</div>
    <div>{{aa.hobby.h2}}</div>

    <div>
      <button @click="change2">test  ++</button>

      <button @click="change3">修改第二层的数据</button>
    </div>

  </div>
</template>

<script>

import { reactive, shallowReadonly } from 'vue'

export default {
  name: 'HomeView',
  setup(props){ 

    const state = reactive({
      name:'young',
      age: 18,
      hobby:{
        h1:'code',
        h2:'music'
      }
    })

    let aa = shallowReadonly(state)  // 第一层数据就不能修改,第二层往后的数据就可以修改
  
    return {
      state,
      aa,
      change(){
        state.age++
      },
      change2(){
        aa.age++
      },
      change3(){
        aa.hobby.h1 = 'rap'
        aa.hobby.h2 = 'jump'
      }
     
    }

  }
}
</script>
```



## setup中使用生命周期

在setup中的生命周期钩子，都是要先导入， onXXX

| `beforeCreate`    | Not needed*         |
| ----------------- | ------------------- |
| `created`         | Not needed*         |
| `beforeMount`     | `onBeforeMount`     |
| `mounted`         | `onMounted`         |
| `beforeUpdate`    | `onBeforeUpdate`    |
| `updated`         | `onUpdated`         |
| `beforeUnmount`   | `onBeforeUnmount`   |
| `unmounted`       | `onUnmounted`       |
| `errorCaptured`   | `onErrorCaptured`   |
| `renderTracked`   | `onRenderTracked`   |
| `renderTriggered` | `onRenderTriggered` |
| `activated`       | `onActivated`       |
| `deactivated`     | `onDeactivated`     |

使用方法

```
<template>
  <div class="home" id="box">
    <div>{{state.name}}</div>
    <div>{{state.age}}</div>
  </div>
</template>

<script>

import { reactive, onBeforeMount, onMounted } from 'vue'
// 在setup里面使用生命周期,都是函数

export default {
  name: 'HomeView',
  setup(props){ 
    const state = reactive({
      name:'young',
      age: 18
    })

    // console.log(document.getElementById('box'))
    onBeforeMount(()=>{  // 挂载前,在箭头函数里面就是要挂载前要做的操作
      console.log(this)
      // console.log(document.getElementById('box').innerHTML)
    })

    onMounted(()=>{  // 挂载后
      // console.log(this)
      console.log(document.getElementById('box').innerHTML)
    })

    return {
      state
    }
  }
}
</script>

```



## provide、inject

在setup里面去做跨代传值

HomeView.vue

```
<template>
  <div class="home" id="box">
    <h2>parent</h2>
    <Son></Son>
  </div>
</template>

<script>
import Son from './Son.vue'
import { provide } from 'vue'
export default {
  name: 'HomeView',
  setup(props){ 
    // 需要向下传的值,就通过provide方法传
    // 参数1: 是名称
    // 参数2: 需要传的值
    provide('flag','yy')  
   
    provide('money',100)
  },
  components:{
    Son
  }
}
</script>
```

Son.vue

```
<template>
  <h2>son</h2>
  <div>这是son组件</div>
  <div>
    <div>flag:{{flag}}</div>
    <div>money:{{money}}</div>
  </div>
</template>

<!-- vue3中的单文件组件, template里面没有只有一个根节点的要求 -->
<script>
  // 在后代组件中接收provide传下来的值
  import { inject } from 'vue'
  export default {
    name: 'HomeView',
    setup(props){ 
      // 需要接受祖先组件传下来的值, 
      // 就要用inject去接收 
      // 参数就是 祖先组件定义的名称
      // 返回值就是 传下来的值
      const flag = inject('flag')  
      console.log(flag)
      const money = inject('money');
      console.log(money)

      return {
        flag,
        money
      }
    }
  }
</script>
```



## script   setup

在script标签加上setup属性
  		然后在script标签里面写的代码,就相当于是setup()里面写的代码

```
<template>
  <div class="home" id="box">
      <!-- 调用响应式对象的值 -->
      <h2>{{state.count}}</h2>

      <div>
        <!-- 触发一个方法 -->
        <button @click="add">++</button>
      </div>

      <!-- <div>{{user}}</div> -->
  </div>
</template>

<script setup>
  // setup的语法
  // 在script标签加上setup属性
  // 然后在script标签里面写的代码,就相当于是setup()里面写的代码
  import { reactive } from 'vue';  // 导入reactive的方法
  
  const state = reactive({  // 创建响应式对象
    count:0
  })

  {  // 里面的这个user就不属于里面的顶层了,所以在外面就不能使用
    const user = {
      name:'yy'
    }
  }

  function add(){  // 创建一个方法,修改响应式对象的值
    state.count++
  }
</script>

```


## computed 计算属性
```
<template>
  <div class="home">
    {{ state.count }}
    <div>
      <button @click="add">++</button>
    </div>

    <hr/>

    <!--  调用计算属性,  调用了多次,计算属性的方法只会执行一次 -->
    {{countPlus}}
    {{countPlus}}
    {{countPlus}}


    <hr/>

    {{person}}
    {{person}}
    {{person}}

    <div>
      <button @click="changeAge">change-age</button>
    </div>

  </div>
</template>

<script>

// 1.导入计算属性的方法
import { reactive, computed } from 'vue'
export default {
  name: 'HomeView',
  setup(props){
    const state = reactive({
      count: 0
    })
    // 2.1  回调函数的形式
    // computed()的返回值才是计算属性
    const countPlus = computed(()=>{
      console.log('aaaaaaaaaaa')
      return state.count   // 返回回去的属性,就是当前计算属性的依赖
    })


    const user = reactive({
      age:18
    })

    // 2.2对象的形式  (就是ref创建的对象)
    // 对象里面两个属性
    // get() 取计算属性的时候会被触发
    // set() 计算属性重新赋值的会被触发
    const person = computed({
      get:()=>{  // 调用person的时候就会触发这个方法
        return user.age+2
      },
      set: ()=>{ // person.value被赋值的时候,就会调用这个set方法
        console.log('AAAAAAAAAAAAAAA')
        return 88
      }
    })
    
    return {
      state,
      add(){
        state.count++
      },
      countPlus,  // 3.把计算属性返回
      person,
      changeAge(){
        // user.age++
        // person = 66
        // console.log(person)
        person.value = 100
      }
    }

  }
}
</script>
```

## watch 侦听器
```
<template>
  <div class="home">
    {{ state.count }}
    <div>
      <button @click="add">++</button>
    </div>

    <hr/>
    <div>
      num: {{num}}    
      age: {{age}}    
    </div>
    <div>
      <button @click="change">change</button>
    </div>
  
  </div>
</template>

<script>

// 1.导入侦听器的方法
import { reactive, ref, watch } from 'vue'
export default {
  name: 'HomeView',
  setup(props){
    const state = reactive({
      count: 0
    })
    // 
    // 2. 使用侦听器方法
    // 参数1 : 是一个函数,返回值就是要被侦听的值,  ()=>{return state.count}
    // 参数2: 也是一个函数, 形参1:修改后的新值, 参数2: 修改前的值
    
    // 只要参数1的值有变化,就会触发参数2的这个函数 

    // 侦听一个值的情况
    watch(()=>state.count,(newCount,oldCount)=>{
      console.log(newCount)
      console.log(oldCount)
    })


    const num = ref(0);
    const age = ref(18)

    // 侦听多个值的情况
    watch([num,age],(newvalue,oldvalue)=>{
      console.log(newvalue)  //修改后的数组  (数组元素的顺序是和参数1的数组顺序一样)
      console.log(oldvalue)  //修改前的数组
    })
    
    // 参数2:解构出来
    watch([num,age],([newNum, newAge],[oldNum, oldAge])=>{
      console.log('------------------------')
      console.log(newNum)
      console.log(newAge)
      console.log('========================')
      console.log(oldNum)
      console.log(oldAge)
    })
    return {
      state,
      add(){
        state.count++
      },
      num,
      age,
      change(){
        num.value++,
        age.value++
      }
    }

  }
}
</script>
```





# vite

下一代前端开发与构建工具

使用了之后就一个感受： 快

https://vitejs.cn/



## 创建项目

```
npm init vite@latest vueDemo   // 最后一个是项目的名称
```



- 选择框架

![image-20220606141136107](assets/image-20220606141136107.png)

- 选择开发语言

![image-20220606141235932](assets/image-20220606141235932.png)



进入项目目录然后安装依赖

```
cd vueDemo
npm i
```



- 启动项目

```
npm run dev
```



- 打包

```
npm run build
```



- 预览

```
npm run preview
```



## 项目目录

大部分都是之前的目录结构一样的

index.html  不在public中，而是在根目录中了

vite.config.js   和vite相关的配置

```
import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

// https://vitejs.dev/config/
export default defineConfig({
  base:'./',  // 配置资源的路径
  server: {  // 开发环境服务器的配置
    port: 2022,
    open: true
  },
  plugins: [vue()]
})

```

https://vitejs.cn/config/

