# webpack

打包工具 

官网：

https://www.webpackjs.com/

https://webpack.js.org/



视频：

http://it.qfedu.com/oa.php/Public/share?c2lkPTE4MDM=



# 使用

1.需要安装两个依赖

```
npm init -y
```

webpack       webpack-cli

```
npm i webpack webpack-cli -D

npm i webpack webpack-cli --save-dev
```



2.创建配置文件

build/webpack.common.js



## entry

项目的入口

## output

项目的出口

```

const path = require('path')
// path.resolve 返回的是绝对路径

// webpack的配置
module.exports = {
  entry: path.resolve(__dirname,'../src/main.js'),  // 入口文件
  output: {  // 出口
    path: path.resolve(__dirname,'../dist'), // 打包后的文件夹路径
    filename: 'bundle.js'  // 打包后的文件
  }
}
```



2.在package.json 添加运行命令

```
 "scripts": {
    "build": "webpack --config build/webpack.common.js",
    "test": "echo \"Error: no test specified\" && exit 1"
  },
```



3.执行打包

```
npm run build
```

打包完成后，会在目录中添加dist/bundle.js文件



## 插件

在配置对象中添加 plugins字段来添加插件

```

const path = require('path')
// path.resolve 返回的是绝对路径

// html-webpack-plugin  插件
const htmlwebpackplugin = require('html-webpack-plugin');


// webpack的配置
module.exports = {
  entry: path.resolve(__dirname,'../src/main.js'),  // 入口文件
  output: {  // 出口
    path: path.resolve(__dirname,'../dist'), // 打包后的文件夹路径
    filename: 'bundle.js'  // 打包后的文件
  },
  plugins:[
    // 使用插件
    new htmlwebpackplugin({
      template: path.resolve(__dirname,'../public/index.html'),
      filename: 'index.html'
    })
  ]
}
```





## loader

作用： 遇到不同的文件后，用怎么样的方式去处理

比如： 遇到css 文件后改怎么处理

style-loader    css-loader   

```
module:{ // 使用loader
    rules:[  // 使用loader的规则
      {
        test: /\.css$/, // 匹配文件的规则  (正则)
        use:['style-loader','css-loader'] // 配置到了css文件后就用这两个loader去处理
      }
    ]
  }
```

