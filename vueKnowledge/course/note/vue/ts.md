# typescript

简称ts

官网：https://www.typescriptlang.org/

中文手册：https://typescript.bootcss.com/

视频： http://it.qfedu.com/oa.php/Public/share?c2lkPTE3Mzk=



是js的一个超集，可以把js编程强类型的语言





# 工具

typescript   :  是把ts编译成js

ts-node  ： 可以直接在node环境中执行ts文件的工具

```
npm i -g typescript
npm i -g ts-node

//检查
tsc
ts-node
```





# 代码

## js中问题

```
// 把字符串分成数组的方法
function sstr(str){
  return str.split('')
}

var arr = sstr('aaaaaaaaaa')

console.log(arr)

// 传的是一个数字,数子对象是没有split方法的,所以这个就报错了
// 这个错误是在运行时,才发现的错误
var nns = sstr(123)
console.log(nns)

```



## 开始ts

需要先初始化项目

```
npm init -y
tsc --init 
// 初始化ts项目
// tsconfig.json  ts的配置文件
```



编译ts文件

```
tsc ts文件名
tsc index.ts

// 会生成同样名字的js文件
```



监听当前文件夹内的ts 文件，实时编译

```
tsc -w
tsc --watch
```



执行ts文件

```
ts-node ts文件名
ts-node index.ts
```



## 基本的数据类型

```
let num:number = 10;  // 数字类型
// num = 'young'

let str:string = 'young'; // 字符串类型

let flag:boolean = true;  // 布尔类型

let test:null = null ; // null类型

let unde:undefined = undefined; // undefined

let age = 20;  // 自行检测数据类型
// age = 'yy'
```



## 任意类型、未知类型

```
let aa:any = 20;  // 任意类型 (基本是不推荐用的)
aa = 'yy'

let uu:unknown // 未知类型
uu = 20;
uu = 'yyy'
```



## 定义数组

```
// let arr = [1,'y',true]  // 原来js中的数组里面是可以随意放元素的

// let nums:Array<number> = [1,2,3,false]  // 数字类型的数组

// 方式1
let aas:Array<string> = ['yy1','yy2'] // 字符串的数组
// 方式2
let nums:number[] = [1,2,3]

```



## 元组

```
// 就是数组的严格版, 顺序,类型,长度都要一致,否则就会报错

let yuan:[string,number];
// yuan = ['yy1',18]

// yuan = [18,'yy']  // x
// yuan = ['yy',18, 20]  // x

```



## 接口

```

// 接口----------------------------
// 就是规定对象的key有哪些
// 或者说是对象的模型

interface Person{
  name:string,
  age: number,
  sex: string,
  height?: number   // ? 表示可选
}

let obj:Person = {
  name: 'young',
  age: 18,
  sex: '0'
}

let ps:Person[] = [
  {
    name: 'young',
    age: 18,
    sex: '0'
  },
  {
    name: 'young',
    age: 18,
    sex: '0',
    height: 180
  }
]

// 接口继承 ------------
interface Stu extends Person{
  stuid:string,
  code: boolean
}

let xiaoming:Stu = {
  stuid:'123',
  code: true,
  name:'xiaoming',
  age: 16,
  sex:'1',
  height: 190,
}

// 定义数组里面 存放数字的数组
// [[1,2,3],[2,3,4]]
interface NumberArr{
  [index:number]:Array<number>
}
let nns:NumberArr = [
  [1,2],[3,4]
]
```



## 枚举

```

// 枚举 ----------------------
// 一组常量
enum season {
  spring = 5,//会有默认值(数字),是从0开始, 如果指定了默认值,就从指定的位置开始
  summer,
  autumn = 20,
  winter
}

// console.log(season)

enum light {
  red='红', // 指定的默认值是字符串,所有的key都需要指定
  green='绿',
  yellow='黄'
}
console.log(light.red)

```





## 函数

```
// 函数-------------------------------
// 参数,和返回值都需要定义
function demo(){
  console.log('aaaaaaaaaaa')
}
demo()


// add  (参数的类型):返回值的类型
function add(a:number,b:number):number{
  // return a+b
  return a+b
}

// 指定是没有返回值的函数
function func(str:string):void{
  console.log(str)
}

// add(1,'a')  // x
// add(22,11)  
// func('啊啊啊啊啊啊啊啊啊啊啊')
// func(123)    // x 
```





## 泛型

```

// 泛型---------------------------
// 广泛的意思,在定义的时候还不确定是什么类型
// 在调用的时候才能确定下来

// T就表示的是一种类型
function demo2<T>(a:T){
  return a
}

console.log(demo2(2))
console.log(demo2('young'))
console.log(demo2({name:'yy1',age:18}))

// 可以定义多个
function demo3<T,U>(a:T,b:U){
  console.log(a,b)
}
demo3(1,'aaaaa')
```

