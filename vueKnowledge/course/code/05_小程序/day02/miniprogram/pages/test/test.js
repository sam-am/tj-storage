// pages/test/test.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    msg: 'hello mini',
    myclass: 'box',
    list: [
      {name: 'YY1',age: 18},
      {name: 'YY2',age: 20},
      {name: 'YY3',age: 22},
      {name: 'YY4',age: 12},
    ],
    flag: true,
    score: 51
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})