// pages/test3/test3.js
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   * 只有onLoad这个生命周期有默认的参数,这个参数用来页面之间传参,
   * 跳转到当前页面 ?后面的参数
   */
  onLoad(options) {
    console.log(options)
    console.log(1)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
    console.log(2)
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    console.log(3)
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
    console.log(4)
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   * 如果这个被删除,就说明当前不能被分享
   */
  onShareAppMessage() {
    console.log(5)
  }
})