const myreq = require('../../utils/request')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    table: {}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    // console.log(options)
    var that = this;
    myreq({
      url:'/vv1/table',
      data: {
        tid: options.tid
      },
      success(res){
        // console.log(res)
        that.setData({
          table: res.data
        })

        wx.setStorageSync('table', res.data)
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  },
  getuser(res){
    // console.log(res)
    const user = wx.getStorageSync('user')
    myreq({
      url: '/vv1/update',
      data: {
        openid: user.openid,
        userinfo: res.detail.rawData
      },
      success(data){
        // console.log(data)
        wx.redirectTo({
          url: '/pages/menus/menus',
        })
      }
    })
  }
})