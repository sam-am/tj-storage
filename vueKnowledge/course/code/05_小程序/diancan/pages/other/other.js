const myreq = require('../../utils/request')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list: [],
    total: 0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    const yidian = wx.getStorageSync('yidian');
    this.setData({
      list: yidian
    })


    let sum = 0; 
    for(let i=0; i<yidian.length; i++){

      sum += yidian[i].price*yidian[i].num
    }
    
    this.setData({
      total: sum
    })

  },
  xiadan(){

    var that = this;
    const user = wx.getStorageSync('user')

    const table = wx.getStorageSync('table')

    myreq({
      url: '/vv1/order',
      data: {
        openid: user.openid,
        tid: table._id,
        list: JSON.stringify(that.data.list),
        total: that.data.total
      },
      success(res){
        // console.log(res)

        wx.removeStorageSync('yidian');

        wx.redirectTo({
          url: '/pages/result/result?oid='+res.data._id,
        })
      }
    })
  }
})