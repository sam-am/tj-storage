const myreq = require('../../utils/request')
Page({

  /**
   * 页面的初始数据
   */
  data: {
      list:[],
      active: 0,   // 保存选择的第几个菜品的分类
      dlist: [],
      total: 0  // 计算已点的总数量
  },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

    var that = this;
    myreq({
      url: '/vv1/serie',
      data: {
        uid: '6229947e9eea28ca6c8cddfe'
      },
      success(res){
        // console.log(res)
        that.setData({
          list: res.data
        })


        myreq({
          url: '/vv1/dish',
          data:{
            serie: that.data.list[that.data.active]._id
          },
          success(dishes){
            // console.log(dishes)

            const yidian = wx.getStorageSync('yidian')
            if(yidian){
              for(let i=0; i<yidian.length; i++){
                for(let j=0; j<dishes.data.length; j++){
                  if(yidian[i]._id == dishes.data[j]._id){  
                    dishes.data[j].num = yidian[i].num
                  }
                }
              }

              var sum = 0; 
              for(let i=0; i<yidian.length; i++){
                sum += yidian[i].num
              }
              that.setData({
                total: sum
              })


            }

            that.setData({
              dlist: dishes.data
            })
          }
        })

      }
    })
  },

  changetype(e){
    var that = this;
    this.setData({
      active: e.target.dataset.idx
    })

    myreq({
      url: '/vv1/dish',
      data: {
        serie: that.data.list[e.target.dataset.idx]._id
      },
      success(res){
        // console.log(res)

        // 拿到菜的列表后,应该和yidian对比一下
        // 如果有yidian就要更新num字段
        const yidian = wx.getStorageSync('yidian')
        if(yidian){
          for(let i=0; i<yidian.length; i++){
            for(let j=0; j<res.data.length; j++){
              if(yidian[i]._id == res.data[j]._id){  
                res.data[j].num = yidian[i].num
              }
            }
          }
        }
    
        that.setData({
          dlist: res.data
        })
      }
    })

  },
  add(e){
    // console.log(1)
    // console.log(e.target.dataset.idx)
    // console.log(this.data.dlist[e.target.dataset.idx])

    const idx = e.target.dataset.idx; // 点击的那一个菜的下标

    // 判断这个菜有没有点过
    if(this.data.dlist[idx].num){
      this.data.dlist[idx].num++   // 有菜就在加1
    }else{
      this.data.dlist[idx].num = 1  // 没点过就设置为1
    }

    // 更新数据到页面
    this.setData({
      dlist: this.data.dlist
    })

    // 把已点的菜保存起来 (storage)
    
    let yidian = wx.getStorageSync('yidian');
    
    if(yidian){
      // 有点过情况
      // 也要判断在已点中有没有点过, 
      // 有点过就更新
      // 没点过就追加

      var flag = false // 是否有点过这个菜
      var index = 0;   // 是第几个菜
      // console.log(yidian)
      for(let i=0; i<yidian.length; i++){
        // 判断当前点的这个菜在已点中有没有
        if(yidian[i]._id == this.data.dlist[idx]._id){ 
          flag = true;
          index = i; 
        }
      }

      if(flag){  
        // 有这个菜,就更新
        yidian[index] = this.data.dlist[idx]
      }else{
        // 没有这个菜就追加
        yidian.push(this.data.dlist[idx])
      }

    }else{ // 从来没有点过菜,就直接把这个菜加到yidian中
      yidian = []
      yidian.push(this.data.dlist[idx])
    }

    var sum = 0; 
    for(let i=0; i<yidian.length; i++){
      sum += yidian[i].num
    }
    this.setData({
      total: sum
    })

    // console.log(yidian)
    wx.setStorageSync('yidian', yidian)


  },
  sub(e){
    const idx = e.target.dataset.idx; 

    let yidian = wx.getStorageSync('yidian');

    // 判断这个菜有没有点过
    if(this.data.dlist[idx].num>0){
      this.data.dlist[idx].num--;
    }else{
      
    }

    
    // 更新已点的数据
    for(let i=0; i<yidian.length; i++){
      if(this.data.dlist[idx]._id == yidian[i]._id){
        yidian[i] = this.data.dlist[idx]
      }
    }

    // num为0清除
    for(let i=0; i<yidian.length; i++){
      if(yidian[i].num<=0){
        yidian.splice(i,1)
      }
    }

    // console.log(yidian)

    this.setData({
      dlist: this.data.dlist
    })

    var sum = 0; 
    for(let i=0; i<yidian.length; i++){
      sum += yidian[i].num
    }
    this.setData({
      total: sum
    })

    wx.setStorageSync('yidian', yidian)
  }
})