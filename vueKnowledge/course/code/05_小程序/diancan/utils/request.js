module.exports = function(obj){
  
  wx.showLoading({
    title: 'loading...',
  })

  wx.request({
    url: 'https://www.young1024.com:177'+obj.url,
    method: 'post',
    data: {
      // uid: '6229947e9eea28ca6c8cddfe',
      uid: '62261818ab79a395356e956d',
      ...obj.data
    },
    success(res){
      obj.success(res.data)
    },
    fail(err){
      if(obj.fail){
        obj.fail(err)
      }
    },
    complete(){
      wx.hideLoading({
        success: (res) => {},
      })
    }
  })
}