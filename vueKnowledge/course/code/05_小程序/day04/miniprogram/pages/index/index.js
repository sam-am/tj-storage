
Page({
  saveuser(){
    // console.log('aaaaaaaa')

    // 同步的存
    wx.setStorageSync('user',{name:'young',age: 19})

    // 回调函数的存
    wx.setStorage({
      key: 'hobby',
      data: ['code','run']
    })


    // 同步的取
    // 没有取成功的情况就为空
    console.log(wx.getStorageSync('user1'))


    wx.getStorage({
      key: 'user2',
      success: function(res){
        console.log(res.data)
      },
      fail(err){  // 没有取成功的情况
        console.log(err)
      }
    })
  },

  removeuser(){
    wx.removeStorage({
      key: 'user',
      success(){
        console.log('移除成功')
      },
      fail(){
        console.log('移除失败')
      }
    })
    wx.removeStorageSync('hobby')
  },
  removeall(){
    // wx.clearStorage({
    //   success: (res) => {},
    // })

    wx.clearStorageSync()
  },
  getmsg(){
    // wx.getStorageInfo({
    //   success: (option) => {
    //     console.log(option)
    //   },
    // })

    const user = wx.getStorageSync('user')

    console.log(user)

    wx.getStorage({
      key: 'hobby',
      success(res){
        console.log(res.data)
      }
    })

  }
})