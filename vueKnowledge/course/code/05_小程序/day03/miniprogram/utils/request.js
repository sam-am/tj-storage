

/*
  obj = {
    url: '/city',
    data: {
      page: 1,
      size: 10
    },
    success:function(res){

    }
  }
*/
module.exports = function(obj){
  wx.showLoading({
    title: 'loading...',
  })
  wx.request({
    url: 'http://www.young1024.com:1234/'+obj.url,
    data: obj.data,
    success(res){
      obj.success(res.data)
    },
    fail(err){
      // console.log(err)
      // obj.fail(err)
      if(obj.fail){
        obj.fail(err)
      }
    },
    complete(){
      wx.hideLoading({
        success: (res) => {},
      })
    }
  })
}