// pages/index/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    time: 1665480534000,
    t2: 1665394134000
  },
  toYMD(str){
    const day = new Date(str);
    const strtime = day.getFullYear()+'年'+(day.getMonth()+1)+'月'+day.getDate()+'日';
    return strtime
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    wx.hideTabBarRedDot({
      index: 0,
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  },
  gomy(){
    wx.switchTab({
      url: '/pages/my/my',
    })
  },
  gotest(){
    wx.navigateTo({
      url: '/pages/test/test',
    })
  },
  gotest2(){
    wx.redirectTo({
      url: '/pages/test/test',
    })
  },
  showtoast(){
    wx.showToast({
      title: '这是一个错误的提示这是一个错误的提示这是一个错误的提示!',
      icon: 'none',
      duration: 3000
    })
  },
  showmodel(){
    wx.showModal({
      title: '这是一个提示框的标题',
      content: '这是提示的内容文本这是提示的内容文本这是提示的内容文本这是提示的内容文本这是提示的内容文本这是提示的内容文本!',
      cancelText: '拒绝',
      confirmText: '接受',
      success: function(res){
        console.log(res)
      }
    })
  },
  showloading(){
    wx.showLoading({
      title: '拼命加载中',
    })

    setTimeout(()=>{
      wx.hideLoading()
    },3000)
  },
  msg(){
    wx.setTabBarBadge({
      index: 1,
      text: '9'
    })
  }
})