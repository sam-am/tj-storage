import { createStore } from 'vuex'
const store = createStore({
  state: {
    count: 0
  },
  mutations:{
    changeCount(state){
      state.count++
    },
    setCount(state,num){
      state.count = num
    }
  },
  plugins: [(store)=>{   
    /*
      1.先判断再storage中没有存的值
        有 - 赋值给state.count
        没有  -  把默认值存在storage中

      2. 修改的时候同步更新到storage
    */
    // vuex 初始化好了之后就会触发这个方法
    // console.log(store.state.count)
    const local = localStorage.getItem('vuex')
    // console.log(local)
    if(local){
      const s  = JSON.parse(local)
      // console.log(s)
      // store.state = s
      store.commit('setCount',s.count)
    }else{
      localStorage.setItem('vuex',JSON.stringify(store.state))  
    }

    store.subscribe((mutation, state)=>{
      // 只要有修改了就会触发这个方法
      // console.log(state)
      localStorage.setItem('vuex',JSON.stringify(state)) 
    })
    
  }]
})

export default store
