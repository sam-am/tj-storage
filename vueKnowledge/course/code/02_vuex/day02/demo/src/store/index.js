import { createStore } from 'vuex'

// 1. 导入vuex-persist的插件
import VuexPersistence from 'vuex-persist'
// 2. 创建vuex-persist插件的实例
const vuexLocal = new VuexPersistence({
  storage: window.localStorage   // 指定用那种储存方式
})

const store = createStore({
  state: {
    count: 0,
    name: 'young'
  },
  mutations:{
    changeCount(state){
      state.count++
    },
    setCount(state,num){
      state.count = num
    }
  },
  // 3. 使用插件
  plugins: [vuexLocal.plugin]
})

export default store
