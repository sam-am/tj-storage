import { createStore } from 'vuex'

import user from './user'

const store = createStore({
  state: {   //  存放数据
    count: 0,
    list: [1,25,66,22,24,88]
  },
  getters: { // 统一取数据的方法  // 全局的计算属性
    oulist(state){
      return state.list.filter(item=>{
        return item%2 == 0
      })
    }
  },
  mutations: {  // 修改数据的方法(同步)   // 纯函数 
    // changeCount(state){   // X!  
    //   setTimeout(()=>{
    //     state.count++
    //   },1000)
    // },
    changeCount(state){
      state.count++
    },
    setCount(state,num){
      state.count = num
    }
  },
  actions: {   //异步修改数据, 只是可以写异步; 修改数据也是通过mutations.
    asyncChangeCount(context){   // context == $store
      setTimeout(() => {
        context.commit('changeCount')
      }, 1000);
    }
  },
  modules: {    // 仓库模块化
    // 在这个modules中加载一个小仓库
    user:user
  },
  
})

export default store
