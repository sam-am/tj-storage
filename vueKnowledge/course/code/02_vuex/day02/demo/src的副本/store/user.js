const user = {   // 用来保存用户的一个小仓库
  namespaced: true,    // 命名空间, 调用里面的方法就要通过仓库名称
  state(){
    return {
      name:'young',
      age: 18
    }
  },
  mutations: {
    changName(state){
      state.name = 'yyds'
    },
    changeAge(state){
      state.age++
    }
  }
}

export default user