// 1
import axios from 'axios'
import qs from 'qs'

// 2
const myaxios = axios.create({
  baseURL: 'http://www.young1024.com:1234/',
  timeout: 6000
})

// 2.1
myaxios.interceptors.request.use((config)=>{
  config.data = qs.stringify(config.data)
  return config
})

// 2.2
myaxios.interceptors.response.use((res)=>{
  return res.data
})

//3
export default myaxios