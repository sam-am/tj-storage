
import myaxios from '@/utils/axios'

// 1.从vuex导入了一个创建仓库的方法
import { createStore } from 'vuex'

// 2.然后使用了这个方法创建了一个仓库对象,然后导出
const store = createStore({
  state: {  // 用来存放共享的数据的,类似全局的data
    count: 18,
    list: [2,3,3,4,5,88,22,33,11],
    age: 20,
    name: 'young',
    mylist: []   // 定义数组保存数据
  },
  getters: {  // 提供统一取数据的方法  // 如果取都有相同的操作就可以定义在这里(就像是以全局的computed)
    // 把list过滤一遍里面呢只有偶数
    getevenList(state){
      const oulist = state.list.filter(item=>{
        return item%3 == 0
      })
      return oulist
    }
  },
  mutations: { // 修改数据的方法  (同步)
    changeCount(state){   // 提供了一个修改state.count的方法
      state.count++
    },
    changeAge(state){
      state.age++
    },
    setList(state,list){  
      // 定义一个mutations 来修改数组, 转进来什么就修改什么
      state.mylist = list
    }
  },
  actions: {   // 异步修改数据的方法
    // 虽然是异步,但是也要通过mutations去修改
    asyncChangeAge(context){    // context就是相当于仓库对象 , $store   
      console.log(context)
      // 3秒后再修改age
      setTimeout(()=>{
        context.commit('changeAge')
      },3000)
    },
    asyncSetList(context){
      // 需求： 要请求接口拿到数据后，再给state, 这个请求是异步， 所以就要写在actions中
      myaxios.get('city').then(res=>{
        // console.log(res)
        if(res.status==0){
          context.commit('setList', res.data.cities)
        }
      })
    }
  },
  modules: {   // 仓库模块(把一个大仓库分成一个一个小仓库)
  }
})

export default store
