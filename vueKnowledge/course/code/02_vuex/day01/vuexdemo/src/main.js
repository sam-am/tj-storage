import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'   //  ./store/index.js


const app = createApp(App)
app.use(store)  // 3.在vue实例中挂载了这个仓库.   
app.use(router)
app.mount('#app')
