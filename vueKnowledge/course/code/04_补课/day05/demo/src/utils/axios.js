import axios from "axios"
import qs from 'qs';


const myaxios =  axios.create({
  baseURL: 'https://www.young1024.com:3002/'
})

myaxios.interceptors.request.use(function(config){

  config.data = qs.stringify(config.data)
  
  return config
})


myaxios.interceptors.response.use(function(res){
  return res.data
})

export default myaxios;



