/* eslint-disable */   
// 上面这个注释放在文件的开头, 意思就是当前不用eslint去检查代码格式了

// 配置一个路由对象
// 1.引入相关的库
import Vue from "vue";
import VueRouter from "vue-router";

import HomeView from '../views/HomeView.vue'


// 2.vue使用了VueRouter这个插件
Vue.use(VueRouter)


// 3.1 把路由配置文件抽离出来

// 路由对象的配置中两个属性是必须的  

// path 匹配的路径
// component 匹配到这个路径要显示的视图
// redirect 路由重定向
// 配置子路由 children
    // 1. children属性中配置子路由 (通常path不写/的)
    // 2. 父路由的视图中使用<router-view />来显示子路由的视图

// name 属性  跳转的时候可以根据这个name来跳转.

// 动态路由- :来表示路由的参数, 如果这个参数有?表示这个参数可选.


const routes = [  // 路由表的配置
  {  // 配置路由  , 一个对象就是一个路由的配置
    path: '/',
    name: 'HomeView',
    component: HomeView
  },
  {
    path: '/about',
    name: 'about',
    component: ()=>import('../views/AboutView.vue')
  },
  {
    path: '/my',
    name: 'my',
    component: ()=>import('../views/MyView.vue')
  },
  {
    path: '/test/:id',
    name: 'test',
    component: ()=>import('../views/TestView.vue'),
    meta: {
      title: '这是test的标题',
      age: 18,
      name: 'young'
    }
  },
  {
    path:'/a',
    redirect: '/my'
  },
  {
    path: '/parent',
    name: 'parent',
    component: ()=>import('../views/ParentView.vue'),
    // 配置子路由
    children: [
      {
        path: 'son1',
        name: 'son1',
        component: ()=>import('../views/Son1View.vue')
      },
      {
        path: 'son2',  
        name: 'son2',
        component: ()=>import('../views/Son2View.vue')
      },
      {
        // 一般在子路由中是不加/的, 如果加 直接访问 /son3就可以访问到这个子路由了
        // 如果不加,就是要通过父路由的路径,在加上子路由的路径才可以访问到
        path: '/son3',   
        name: 'son3',
        component: ()=>import('../views/Son3View.vue')
      }
    ]
  },
  {
    // 动态路由  :表示访问这个路由的参数 , 如果这个参数有?代表这个参数可选
    // 动态路由要和子路由要区分开
    path: '/active/:uid?',
    component: ()=>import('../views/ActiveView.vue'),
    children: [
      {
        path: 'son1',
        component: ()=>import('../views/Son1View.vue')
      }
    ]
  }
]

// 在当页面中,再跳转到当前页,就会报一个错误,但是不影响使用
// 下面的这段代码就是可以把错误屏蔽掉

const originalPush = VueRouter.prototype.push; 
VueRouter.prototype.push = function push(location) {
  return originalPush.call(this, location).catch((err) => err ); 
};

// 3.创建VueRouter实例
const router = new VueRouter({
  mode: 'hash',
  // mode: 'history',
  routes
})

export default router;