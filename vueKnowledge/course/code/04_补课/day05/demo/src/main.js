import Vue from 'vue'
import App from './App.vue'
// 5.引入router的配置实例
import router from './routes'
Vue.config.productionTip = false


import myaxios from './utils/axios'

Vue.prototype.$axios = myaxios;
// Vue.prototype.$a = 'a';
// Vue.prototype.$aaa = 'aaa';
// Vue.prototype.$AAAA = 'AAAA';

new Vue({
  router,   // router: router   // 6. router实例挂载到vue实例中
  render: h => h(App),
}).$mount('#app')
