import Vue from 'vue'

import 'normalize.css/normalize.css' // css初始化文件(重置标签的默认样式)

import ElementUI from 'element-ui'  // 导入了elementui组件
import 'element-ui/lib/theme-chalk/index.css'  // 导入了elementui组件的样式
import locale from 'element-ui/lib/locale/lang/en' // 导入语言包

import '@/styles/index.scss' // 全局的css

import App from './App'
import store from './store'
import router from './router'

import '@/icons' // 全局注册了icon组件 (作用: 只要把svg图标文件,放在icons/svg文件夹内容,就可以直接使用这个图标 )

import '@/permission' // 路由的守卫的文件 (全局前置守卫/全局后置守卫)


// 判断是否是生产环境
if (process.env.NODE_ENV === 'production') {
  const { mockXHR } = require('../mock')
  mockXHR()
}

// Vue使用element这个插件
Vue.use(ElementUI, { locale })
// 如果想要中文版 element-ui，按如下方式声明
// Vue.use(ElementUI)

Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
