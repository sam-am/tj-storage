import router from './router'
import store from './store'
import { Message } from 'element-ui'  // 导入elementUI一个提示框
import NProgress from 'nprogress' // 进度条工具
import 'nprogress/nprogress.css' // 进度条工具提示

import { getToken } from '@/utils/auth' // 取cookie的方法
import getPageTitle from '@/utils/get-page-title'  // 获取一个title的方法


NProgress.configure({ showSpinner: false }) // 进度条工具的配置

const whiteList = ['/login'] // 路由的白名单(不需要登录也可以访问页面,路径就写在这里)


// ! 全局的前置守卫
router.beforeEach(async(to, from, next) => {
  // 开启进度条
  NProgress.start()

  // 设置页面的title
  document.title = getPageTitle(to.meta.title)

  // 从cookie中拿到token
  const hasToken = getToken()

  // 判断是否有token
  if (hasToken) {  // 有登录的情况
    if (to.path === '/login') {  // 先判断是否是登录页面
      next({ path: '/' })  // 有登录还访问登录页,就跳转到 /
      NProgress.done()  // 关闭进度条
    } else {  // 不是登录页的情况
      const hasGetUserInfo = store.getters.name  // 从仓库中有没有用户名
      if (hasGetUserInfo) {
        next()  //有用户名,该跳转到那里就跳到那里
      } else {   
        // 没有用户名的情况,
        try {
          // 调用仓库中的actions获取用户信息的方法
          // 在这里没有出错, 就说明获取用户信息成功了,并且把name,avatar保存到了仓库中
          await store.dispatch('user/getInfo')

          next()  // 该跳转到那里就跳到那里
        } catch (error) {
          // 获取用户信息失败的情况
          await store.dispatch('user/resetToken')  //清空仓库中的token
          Message.error(error || 'Has Error')  // 弹出一个提示框
          next(`/login?redirect=${to.path}`)  // 返回到登录页
          NProgress.done()  //// 关闭进度条
        }
      }
    }
  } else {
    // 没有登录的情况
    // 判断访问的这个页面是否在白名单内
    if (whiteList.indexOf(to.path) !== -1) {
      next()  // 直接走
    } else {
      // 不在白名单内,就跳转到登录页
      next(`/login?redirect=${to.path}`)
      NProgress.done()
    }
  }
})

router.afterEach(() => {
  // finish progress bar
  NProgress.done()
})
