import { login, logout, getInfo } from '@/api/user'
import { getToken, setToken, removeToken } from '@/utils/auth'
import { resetRouter } from '@/router'

const getDefaultState = () => {
  return {
    token: getToken(),
    name: '',
    avatar: ''
  }
}

const state = getDefaultState()

const mutations = {
  RESET_STATE: (state) => {
    Object.assign(state, getDefaultState())
  },
  SET_TOKEN: (state, token) => {
    state.token = token
  },
  SET_NAME: (state, name) => {
    state.name = name
  },
  SET_AVATAR: (state, avatar) => {
    state.avatar = avatar
  }
}

const actions = {
  // 登录的操作
  // 第一个参数结构commit
  // 第二个参数是传过来的对象(username,password)
  login({ commit }, userInfo) { 
    const { username, password } = userInfo
    return new Promise((resolve, reject) => {

      // 调用了登录的接口
      login({ username: username.trim(), password: password }).then(response => {
        // 登录成功
        const { data } = response
        commit('SET_TOKEN', data.token)  // 把登录请求成功返回回来的数据中的token, 保存在仓库中
        setToken(data.token)  // 把token也保存在cookie中
        resolve()  // 返回登录成功的状态
      }).catch(error => {
        reject(error)   // 登录失败的状态
      })
    })
  },

  // get user info
  getInfo({ commit, state }) {
    return new Promise((resolve, reject) => {
      // 调用获取用户信息的接口方法
      getInfo(state.token).then(response => {
        const { data } = response

        if (!data) {
          return reject('Verification failed, please Login again.')
        }

        const { name, avatar } = data

        commit('SET_NAME', name)   // 把用户名存到仓库中
        commit('SET_AVATAR', avatar)  // 把有头像存到仓库中
        resolve(data)   //返回成功的状态
      }).catch(error => {
        reject(error)  //  返回: 获取用户信息失败的情况
      })
    })
  },

  // user logout
  logout({ commit, state }) {
    return new Promise((resolve, reject) => {
      logout(state.token).then(() => {
        removeToken() // must remove  token  first
        resetRouter()
        commit('RESET_STATE')
        resolve()
      }).catch(error => {
        reject(error)
      })
    })
  },

  // remove token
  resetToken({ commit }) {
    return new Promise(resolve => {
      removeToken() // must remove  token  first
      commit('RESET_STATE')
      resolve()
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}

