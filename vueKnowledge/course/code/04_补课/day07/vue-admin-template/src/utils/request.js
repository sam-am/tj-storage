import axios from 'axios'
import { MessageBox, Message } from 'element-ui'
import store from '@/store'
import { getToken } from '@/utils/auth'

// 创建axios实例
const service = axios.create({
  baseURL: process.env.VUE_APP_BASE_API, // 读取.env文件来获取到baseURL
  // withCredentials: true, // send cookies when cross-domain requests
  timeout: 5000 // 请求超时的时间
})

// 请求拦截
// 就是做了一件事情: 给请求头加token
service.interceptors.request.use(
  config => {
    // 先判断仓库中有没有存token
    if (store.getters.token) {
      // 有token,就给请求头加个字段 X-token,  它的值就是存的token
      config.headers['X-Token'] = getToken()
    }
    return config
  },
  error => {
    // 请求出错的情况
    console.log(error) // for debug
    return Promise.reject(error)  // 返回一个错误状态的promise
  }
)

// 响应拦截
service.interceptors.response.use(
  response => {
    const res = response.data
    // 判断状态是否等于20000   , 等于20000表示成功
    if (res.code !== 20000) {
      Message({  // 弹出一个错误的提示框
        message: res.message || 'Error',
        type: 'error',
        duration: 5 * 1000
      })

      // 不等于20000, 在判断是否以下的状态  50008, 50012 , 500014
      // 这些状态可能是登录过期
      if (res.code === 50008 || res.code === 50012 || res.code === 50014) {
        // to re-login
        MessageBox.confirm('You have been logged out, you can cancel to stay on this page, or log in again', 'Confirm logout', {
          confirmButtonText: 'Re-Login',
          cancelButtonText: 'Cancel',
          type: 'warning'
        }).then(() => {

          // 登录过期,先谈个提示框,然后调用仓库中重置token的方法
          store.dispatch('user/resetToken').then(() => {
            location.reload()  // 刷行页面
          })
        })
      }
      return Promise.reject(new Error(res.message || 'Error'))  // 返回一个错误的状态
    } else {
      // 等于20000, 表示成功,返回请求到的数据
      return res
    }
  },
  error => {
    console.log('err' + error) // for debug
    Message({
      message: error.message,
      type: 'error',
      duration: 5 * 1000
    })
    return Promise.reject(error)
  }
)

export default service
