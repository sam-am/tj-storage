Vue-element-admin

这个项目有两个版本

admin : 是完整的版本

template : 简单版本



# 项目开始

1.克隆项目

```
git clone https://gitee.com/panjiachen/vue-admin-template.git
```

2.安装依赖

```
npm i 
```

3.启动项目

```
npm run dev
```

就可以访问： localhost:9528



# 目录介绍

build   // 打包相关的配置

mock // 本地mock的数据

test // 测试相关的配置的文件



.env   // 项目运行的配置文件 （会根据不同的环境来读不同的文件）

​	.development  // 开发环境的配置

​	.production // 生产环境的配置

​	.staging  // 测试环境的地址



.eslintignore  // eslint的忽略文件（不想用代码格式简单的文件路径，就写在这个文件内）	 

.eslinttrc.js  // eslint的规则配置文件



