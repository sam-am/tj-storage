# 路由



## 1.路由的概念

​		路由的本质就是一种`对应关系` 根据不同的URL请求，返回对应不同的资源。那么url地址和真实的资源之间就有一种对应的关系，就是路由。

​		路由分为: `后端路由`和`前端路由` 



- **后端路由**:由服务器端进行实现并实现资源映射分发

  - 概念:根据不同的用户URL请求，返回不同的内容(**地址与资源**产生对应关系)
  - 本质:URL请求地址与服务器资源之间的对应关系

  ![image-20210729102555785](assets/image-20210729102555785.png)



- **前端路由**:根据不同的事件来显示不同的页面内容，是事件与事件处理函数之间的对应关系
  - 概念:根据不同的用户事件，显示不同的页面内容(**地址与事件**产生对应关系)
  - 本质:用户事件与事件处理函数之间的对应关系

![image-20210729102648397](assets/image-20210729102648397.png)





​		**SPA**(Single Page Application)单页面应用程序，基于前端路由而起:整个网站只有一个页面，通过 监听地址栏中的变化事件，来通过Ajax局部更新内容信息显示、同时支持浏览器地址栏的前进和后退操作。



## 2.前端路由实现

前端路由有2种模式:

- hash模式

  hash路由模式是这样的:http://xxx.abc.com/#/xx。 有带#号，后面就是hash值的变化。改 变后面的hash值，它不会向服务器发出请求，因此也就不会刷新页面。并且每次hash值发生改变 的时候，会触发hashchange事件。因此我们可以通过监听该事件，来知道hash值发生了哪些变化。

  ```javascript
  window.addEventListener('hashchange', ()=>{ 
    // 通过 location.hash 获取到最新的 hash 值 			
    console.log(location.hash);
  });
  ```

  

- history模式

  HTML5的History API为浏览器的全局history对象增加了该扩展方法。它是一个浏览器 (bom)的一个接口，在window对象中提供了onpopstate事件来监听历史栈的改变，只要历史栈有信息发生改变的话，就会触发该事件。

  ```javascript
  history.pushState({},title,url); // 向历史记录中追加一条记录 history.replaceState({},title,url); // 替换当前页在历史记录中的信息。 window.addEventListener('popstate', function(event) {
  	console.log(event) 
  })
  ```



**hash路由体验**

```
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title></title>
</head>
<body>
	<ul>
		<li><a href="#/a">去a页面</a></li>
		<li><a href="#/b">去b页面</a></li>
		<li><a href="#/c">去c页面</a></li>
		<li><a href="#/d">去d页面</a></li>
	</ul>

	<div id="route-view"></div>

<script type="text/javascript"> 

// 获取元内容素
var ctn = document.getElementById('route-view') 

// 默认渲染
render('/a')

// 监听hashchange事件 
window.addEventListener('hashchange',function(){
	render(location.hash.slice(1)) 
})

// 分支
function render(router){
    switch (router) {
        case '/a':
        ctn.innerHTML = '这是a页面'
		break;

		case '/b':
		ctn.innerHTML = '这是b页面'
		break;

		case '/c':
		ctn.innerHTML = '这是c页面'
		break;

		case '/d':
		ctn.innerHTML = '这是d页面'
		break;
		
		default:
		ctn.innerHTML = '404页面' 
		break;
	}
}
</script>
</body>
</html>
```



## 3.vue-router



### 1.介绍

官方文档：https://router.vuejs.org/zh/

Vue Router 是 Vue.js 官方的路由管理器。它和 Vue.js 的核心深度集成，让构建单页面应用变得易如反掌。包含的功能有：

- 嵌套的路由/视图表
- 模块化的、基于组件的路由配置
- 路由参数、查询、通配符
- 基于 Vue.js 过渡系统的视图过渡效果
- 细粒度的导航控制
- 带有自动激活的 CSS class 的链接
- HTML5 历史模式或 hash 模式，在 IE9 中自动降级
- 自定义的滚动条行为



### 2.安装

如果在vue-cli创建项目时没有勾选上 vue-router 选项，此时就需要手动的来安装它。（**切记，要进入项目中再去运行这个指令**）

```
npm i --save vue-router
```

![image-20210729105449210](assets/image-20210729105449210.png)



### 3.vue-router的基本使用

Vue Router的基本使用步骤:

- 引入相关库文件
-  VueRouter引入到Vue类中
-  **定义路由组件规则**
- 创建路由实例
-  把路由挂载到Vue根实例中
-  **添加路由组件渲染容器到对应组件中(占坑)**



**代码解读**

![image-20210729110329470](assets/image-20210729110329470.png)

![image-20210729110547659](assets/image-20210729110547659.png)

![image-20210729110638537](assets/image-20210729110638537.png)



> - 后期在项目中以 index 命名的文件在引入时，可以省去文件名不写。
> -  在 import 的时候如果涉及到了路径，建议写 @ 符号开头的路径( @表示src目录 )



![image-20210729111010521](assets/image-20210729111010521.png)





### 4.导航方式

在页面中，导航实现有2种方式:

- 声明式导航:通过点击链接实现的导航方式，例如HTML中的“a”标签，Vue中的“router-link”所实现的。

- 编程式导航:通过调用JavaScript形式API实现的导航方式，例如location.href实现的跳转效果

  

#### 1.声明式导航

它就是先在页面中定义好跳转的路由规则，vueRouter中通过 router-link组件来完成

```vue
<router-link to="/">Home</router-link> |
<!-- 
to 要跳转的路由规则 string | object
to="users"
:to="{path:'path'}"
-->
```



#### 2.编程式导航

简单来说，编程式导航就是通过 JavaScript 来实现路由跳转

```
this.$router.push("/login");
this.$router.push({ path:"/login" });
this.$router.push({ path:"/login",query:{username:"jack"} }); 
this.$router.push({ name:'user' , params: {id:123} }); 
this.$router.go( n );//n为数字 负数为回退
```



**注意点:**编程式导航在跳转到与当前地址一致的URL时会报错，但这个报错不影响功能:

![image-20210729113007115](assets/image-20210729113007115.png)



如果患有强迫症，可以在路由入口文件 index.js 中添加如下代码解决该问题:

```
const originalPush = VueRouter.prototype.push; 
VueRouter.prototype.push = function push(location) {
  return originalPush.call(this, location).catch((err) => err); 
};
```



### 5. 路由配置规则

路由规则是一个数组,里面的每个对象就是一个路由，一个对象至少包含path和component两个属性。

#### 1.基本配置 

**path 、component**

<img src="assets/image-20210729115253774.png" alt="image-20210729115253774" style="width:80%;" />

- path 表示当前路由规则匹配的hash地址
- component 表示当前路由规则对应要展示的组件



#### 2.重定向 

**redirect**

- 概念:用户在访问地址 A 的时候，强制用户跳转到地址 C ，从而展示特定的组件页面
- 实现: 通过路由规则的 redirect 属性，指定一个新的路由地址，可以很方便地设置路由的重定向

```
{
    path:'/c',
    redirect:'/about'
    // 访问/c 跳转到 /about
}
```



#### 3.**嵌套路由**

**children**

​		嵌套路由最关键在于理解子级路由的概念:

​		比如我们有一个 /users 的路由，那么 /users 下面还可以添加子级路由， 如: /users/index 、 /users/add 等等，这样的路由情形称之为嵌套路由。

> 核心思想:在**父路由组件**的模板内容中添加子路由链接和子路由**填充位(占坑)**，同时在路由规则处为父路由配置**children**属性指定子路由规则:

```vue
{
    path:'/parent',
    component:Parent,
    children:[
      {
        path:'child1',
        component:child1
      },
      {
        path:'child2',
        component:child2
      }
    ]
  }
```

**注意  ,  配置了子路由需要在 父组件中 定义一个router-view，用于嵌套路由的渲染显示 ** 

​		<img src="assets/image-20210729121330349.png" alt="image-20210729121330349" style="width:77%;" />



#### 4.动态路由

​		所谓动态路由就是路由规则中有部分规则是动态变化的，不是固定的值，需要去匹配取出数据(即

`路由参数` )。

```
{
    path: '/active/:id',
    component:Test
}
```

在组件中获取路由参数

```
{{$route.params.id}}
```

**注意：**路由规则中的“:”只是在声明的时候写，在使用的时候不需要写“:”



> **问题：** 访问active 是需要带参数的，如果不带参数会怎样？
>
> 答:如果声明需要传递参数，但是实际不传的话则会影响落地页的显示，显示成白板(但是不报错)。
>
> 

**注意:在实际开发的时候会有可能需要传参也可能不需要传参的情况，这个时候需要用到** `可选路由参数`**点**

定义可选路由参数的方式很简单，只需要在原有的路由参数声明位置后面加上个 ? 即可。例如:

```
{
    path: '/active/:id?',
    component:Test
}
```



#### 5.命名路由

​		命名路由:路由别名，顾名思义就是给路由起名字(外号)。

​		通过一个名称来标识一个路由显得更方便一些，特别是在链接一个路由，或者是执行一些跳转的时 候。

```
{
    path: '/',
    name: 'Home',
    component: Home
 }
```

```
<!-- 使用name进行跳转  -->
<router-link:to="{name:'Home',params:{id:123}}">Home</router-link>
```



#### 6.路由守卫

​		`导航守卫` 就是路由跳转过程中的一些 `钩子函数` ，这个过程中触发的这些函数能让你操作一些其它事情时，可以进行过滤操作，这就是导航守卫。



- 全局守卫

  ```
  
  // 全局前置守卫 路由规则文件中定义
  // 当一个导航触发时,触发前置守卫，
  // to: Route: 即将要进入的目标 路由对象
  // from: Route: 当前导航正要离开的路由
  // next: Function: 一定要调用该next方法，否则路由不向下执行。
  router.beforeEach((to,from,next)=>{
    //...
    next()
  })
  
  // 全局后置钩子
  // 此钩子不会接受 next 函数也不会改变导航本身
  router.afterEach((to,from)=>{
    //...
  })
  
  
  ```

  > 全局守卫定义在路由规则文件中
  >
  > 参数含义:
  >
  > - to: Route: 即将要进入的目标 路由对象
  > - from: Route: 当前导航正要离开的路由
  > - next: Function: 一定要调用该next方法，否则路由不向下执行(后置全局守卫 没有next函数)



- 组件内守卫

  ```javascript
  <script>
  export default {
    name:'Test',
    beforeRouteEnter (to, from, next) {
      // 在渲染该组件的对应路由被 confirm 前调用
      // 不!能!获取组件实例 `this`(这个时候还没有进入到to对应的组件中，所以拿不到this) 
      // 因为当守卫执行前，组件实例还没被创建
      console.log('beforeRouteEnter');
      next()
    },
    beforeRouteUpdate (to, from, next) {
      // 在当前路由改变，但是该组件被复用时调用
      // 举例来说，对于一个带有动态参数的路径 /foo/:id，在 /foo/1 和 /foo/2 之间跳转的 时候，
      // 由于会渲染同样的 Foo 组件，因此组件实例会被复用。而这个钩子就会在这个情况下被调 用。
      // 可以访问组件实例 `this` 
      console.log('beforeRouteUpdate');
      next()
    },
    beforeRouteLeave (to, from, next) { 
      // 导航离开该组件的对应路由时调用
      // 可以访问组件实例 `this`
      console.log('beforeRouteLeave')
      next()
    }
  }
  </script>
  
  ```

- 路由独享的守卫

  ```
  {
      path:'/user/:id',
      component:()=>import('../views/Son.vue'),
      beforeEnter:(to,from, next)=>{  // 路由独享的守卫
        next();
      }
    }
  ```

  



> $router 与 $route 的区别
>
> $router 是操作对象，  如：this.$router.push() 、this.$router.go()
>
> $route 是路由信息对象 ，如：获取参数：this.$route.params.id  、this.$route.query.name







