# vuex

## 1.Vuex 是什么？

Vuex 是一个专为 Vue.js 应用程序开发的**状态管理模式**。它采用集中式存储管理应用的所有组件的状态，并以相应的规则保证状态以一种可预测的方式发生变化。Vuex 也集成到 Vue 的官方调试工具 [devtools extension (opens new window)](https://github.com/vuejs/vue-devtools)，提供了诸如零配置的 time-travel 调试、状态快照导入导出等高级调试功能。

以上是一段来自官网的描述， --状态管理模式  ？  又是什么？ 首先我们先想象一种场景，vue里面都是由组件构成的，假设组件之间有一些公共的数据，以为我们目前学习的vue的知识点来看，可以通过传值的方式来实现它们之间的数据共享，但是组件一旦多了，或者层级嵌套在深几层，那么这种传值的方式就变得很繁琐，也不靠谱，那么vuex就可以给我们解决这一问题， vuex可以看做是是一个仓库，把需要共享的数据存在仓库中，然后组件中有需要共享数据的就统一去仓库中取。 那么我们就避免了传值的问题。接下来我们就看看vuex如何使用。

首先我们使用vue-cli脚手架工具创建工具的时候已经勾选了vuex, 如果没有就需要在项目中再重新安装一次。安装方法在官网有详细步骤介绍：

## 2.安装

```
//第一步
npm install vuex --save

//第二步  (main.js中挂载store)
import store from './store'
new Vue({
  store,
  render: h => h(App),
}).$mount('#app')
```



查看store/index.js文件

```
import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({   //创建了一个仓库，并导出，然后有如下几个属性
  state: { //仓库
  },
  mutations: {  //修改仓库的方法
  },
  actions: { //异步修改仓库的方法
  },
  modules: {  //分模块（分成几个小仓库）
  }
})
```

## 3.state

在这个属性中 ，存放需要共享的数据（ 可以理解为组件中的data）。

```
 state: {
    count:0
 }
```

如何取到count呢?   可以直接获取：$store.state.count

```
 // 在模板中获取
 <div>
      {{$store.state.count}}
 </div>
  
 
 //在js中获取

 this.$store.state.count 
```



> 官方建议: 取值时候用计算属性返回值
>
> ```
> computed:{
>     count(){
>       return this.$store.state.count
>     }
>  }
> ```



以上建议在调用时候会简单一点，但是如果数据多了，就需要在computed里面写很多的属性。然而就有了mapState辅助函数,  使用方式如下：

```
// 从vuex中解构出 mapState
import {mapState} from 'vuex'

// 使用扩展运算符扩展到computed
computed:{
  ...mapState(['count'])   //count会自动解构到computed属性中
}


// 给计算属性设置别名
computed:{
    ...mapState({
      count:state=>state.count,
      countAlias:'count' //key为别名，value为需要映射的字段
    })
}
```

以上就是vuex的初始使用和取值了

## 4.getters

Gettters是我们专门给我们做的取值的方法。 可以对取值进行一些操作处理处理后，在返回。如：返回age的时候前面加上  “年龄：18”； 返回某个数组时候对数组进行过滤后在返回(官网例子) ，这些操作可以在computed里面完成（自定义的，没有使用mapState辅助函数的情况下）。 但是如果多个组件都需要有这样的操作呢，那就需要把这些操作的代码都复制到需要用的组件上。这样就不方便的维护了。getters就可以解决这种问题。

### 1.定义getters

```
getters:{
    getcount(state){  //第一个参数必须为state
      return '数量为:'+state.count    //这里的操作是做了字符串的拼接
    }
},
```

### 2.调用getters

```
import {mapGetters} from 'vuex'

computed:{
   ...mapGetters(['getcount'])
}
```



## 5.mutations

前面已经讲了 state 、gettes，分别的存和取的操作。 那么如何做修改呢?   在vuex中要做修改，需要在mutations里面做。

#### 错误示范 ❌

```
this.$store.state.count = 10;
```

虽然不报错，但是不规范

### 1.定义mutations

```
mutations: {
		setcount(state){  //state为默认值
			state.count++
		}
}
```

### 2.触发mutations

触发mutations需要用到commit()方法

```
this.$store.commit('setcount')
```



### 3.修改mutations传参数

```
setcountAny(state,num){ //第二个参数为调用时传递的参数
	state.count = num
},

//调用
this.$store.commit('setcountAny',8)
```

### 4.mapMutations

可以把mutations解构到methods中

```
 ...mapMutations(['setcountAny','setcount']),
```

> **以上为mutations的常规使用操作。注意点：mutiations里面的操作只能同步的，** 关于异步的操作我们接下来讲。



## 6.Actions

- Action 提交的是 mutation，而不是直接变更状态。
- Action 可以包含任意异步操作。

### 1.定义actions

```
setNum(content){ //第一参数为content,其实就是$store
	setTimeout(()=>{  //异步操作
		content.commit('setcountAny','22')
	},3000)
}
```

### 2.通过dispatch调用

```
 this.$store.dispatch('setNum')
```

### 3.mapActions

```
import { mapActions } from 'vuex';

methods:{ //结构到methods中
    ...mapActions(['setNum']),
},

//调用
this.setNum()
```





以上是vuex 的用法，state放数据，getters修饰取 ，mutations修改数据 ，actions异步修改数据，还有它们对应的辅助函数

![vuex](assets/vuex.png)













