import request from '@/utils/request'

// 获取桌子列表的接口
export function getList() {
  return request({
    url: '/v2/table/list',
    method: 'post',
  })
}

// 增加桌子的接口
export function add(data){  
  return request({
    url: "/v2/table/add",
    method: 'post',
    data
  })
}

// 删除桌子的接口
export function del(data){  
  return request({
    url: "/v2/table/del",
    method: 'post',
    data
  })
}

// 修改桌子的接口
export function update(data){  
  return request({
    url: "/v2/table/update",
    method: 'post',
    data
  })
}