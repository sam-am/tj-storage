# 开始开发点餐后台管理系统

1. 克隆项目

```
 git clone https://gitee.com/panjiachen/vue-admin-template.git
```

2. 安装依赖

```
npm i 
```



# 开始开发

演示网站： http://www.young1024.com/dian/#/login?redirect=%2Fdashboard

交互稿： https://js.design/f/CxEGyX?p=WcdNNrW12k

接口文档： 链接: https://www.apifox.cn/apidoc/shared-41432d8e-f3ed-422f-ae12-bcbf6d083739  访问密码 : sz2208 





## 修改项目

1. 修改接口根地址

.env.development

```
# just a flag
ENV = 'development'

# base api
VUE_APP_BASE_API = 'https://www.young1024.com:177'
```

2. 修改axios的请求拦截，修改请求类型的数据

   utils/request.js

```
import qs from 'qs'

service.interceptors.request.use(
  config => {
   	// 数据类型的转换
    config.data = qs.stringify(config.data)

    if (store.getters.token) {
      config.headers['X-Token'] = getToken()
    }
    return config
  },
  error => {
    console.log(error) // for debug
    return Promise.reject(error)
  }
)
```

3. 修改登录的参数名

store/modules/user.js

actions中login方法

```
login({ commit }, userInfo) {
    const { username, password } = userInfo
    return new Promise((resolve, reject) => {
    	// 修改参数名称
      login({ account: username.trim(), pw: password }).then(response => {
        const { data } = response
        commit('SET_TOKEN', data.token)
        setToken(data.token)
        resolve()
      }).catch(error => {
        reject(error)
      })
    })
  },
```



4. 修改登录的接口地址

   api/user.js

   ```
   export function login(data) {
     return request({
       url: '/v2/user/login',
       method: 'post',
       data
     })
   }
   ```

5. 修改了登录页面的验证方法

   views/login/idnex.vue

```
const validateUsername = (rule, value, callback) => {
      if (!value) {  // 非空即可
        callback(new Error('Please enter the correct user name'))
      } else {
        callback()
      }
    }
    const validatePassword = (rule, value, callback) => {
      if (value.length < 1) { // 非空即可
        callback(new Error('The password can not be less than 6 digits'))
      } else {
        callback()
      }
    }
```



6. 修改请求工具的响应拦截

utils/request.js

```
service.interceptors.response.use(
  response => {
    const res = response.data
    if (res.status !== 'success') {
      Message({
        message: res.msg || 'Error',
        type: 'error',
        duration: 5 * 1000
      })
      return Promise.reject(new Error(res.msg || 'Error'))
    } else {
      return res
    }
  },
  error => {
    console.log('err' + error) // for debug
    Message({
      message: error,
      type: 'error',
      duration: 5 * 1000
    })
    return Promise.reject(error)
  }
)
```



7.修改获取用户信息的接口

api/user.js中的getInfo的方法

```
export function getInfo() {
  return request({
    url: '/v2/user/status',
    method: 'post'
  })
}
```



8. 修改请求工具的请求拦截

   utils/request.js

```
service.interceptors.request.use(
  config => {
    // do something before request is sent

    config.data = qs.stringify(config.data)

    if (store.getters.token) {
      // let each request carry token
      // ['X-Token'] is a custom headers key
      // please modify it according to the actual situation
      config.headers['token'] = getToken()
    }
    return config
  },
  error => {
    // do something with request error
    console.log(error) // for debug
    return Promise.reject(error)
  }
)
```



9. 修改仓库中名字

store/modules/user.js

```
  getInfo({ commit, state }) {
    return new Promise((resolve, reject) => {
      getInfo(state.token).then(response => {
        const { data } = response
        if (!data) {
          return reject('Verification failed, please Login again.')
        }
        const { shopname } = data
        commit('SET_NAME', shopname)
        commit('SET_AVATAR', 'http://www.young1024.com:3008/avatar/1630920351067.png')
        resolve(data)
      }).catch(error => {
        reject(error)
      })
    })
  },

```



# 桌子管理

1.增加路由

router/index.js

```
 {
    path: '/table',
    component: Layout,
    meta: { title: '台桌管理', icon: 'table' },
    children: [
      {
        path: 'list',
        name: 'tablelist',
        component: () => import('@/views/table/list'),
        meta: { title: '台桌列表', icon: 'tablelist' }
      },
      {
        path: 'add',
        name: 'tableadd',
        component: () => import('@/views/table/add'),
        meta: { title: '增加台桌', icon: 'tableadd' }
      }
    ]
  },
```



2.增加视图文件

views/table/list

```
<template>
  <div class="app-container">
    <h2>台桌列表</h2>
    <el-table :data="tableData" style="width: 100%">
      <el-table-column prop="index" label="序号" width="180">
      </el-table-column>
      <el-table-column prop="name" label="姓名" width="180">
      </el-table-column>
      <el-table-column label="操作">
        <template slot-scope="scope">
          <el-link type="primary" @click="showedit(scope)">编辑</el-link>
          <span class="m_lr10"></span>
          <el-link type="danger" @click="del(scope)">删除</el-link>
        </template>
      </el-table-column>
    </el-table>

    <el-dialog title="编辑" :visible.sync="dialogVisible" width="30%">

      <el-form ref="form" label-width="80px">
        <el-form-item label="序号">
          <el-input v-model="table.index"></el-input>
        </el-form-item>

        <el-form-item label="名称">
          <el-input v-model="table.name"></el-input>
        </el-form-item>
      </el-form>
      <span slot="footer" class="dialog-footer">
        <el-button @click="dialogVisible=false">取 消</el-button>
        <el-button type="primary" @click="sure">确 定</el-button>
      </span>
    </el-dialog>

  </div>
</template>

<script>
import { getList, del, update } from '@/api/table'
export default {
  data() {
    return {
      tableData: [],
      dialogVisible: false,
      table:{}
    }
  },
  mounted() {
    getList().then(res => {
      // console.log(res)
      this.tableData = res.data
    })
  },
  methods: {
    sure(){
      update({
        id: this.table._id,
        index: this.table.index,
        name: this.table.name
      }).then(res=>{
        this.$message({
            type: 'success',
            message: '修改成功!'
        });
        this.dialogVisible = false
      })
    },
    showedit(obj){
      this.table = obj.row;
      this.dialogVisible = true
    },
    del(obj) {

      this.$confirm('真的要删除这张桌子吗?', '提示', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        type: 'warning'
      }).then(() => {

        del({ id: obj.row._id }).then(res => {
          this.tableData.splice(obj.$index, 1);

          this.$message({
            type: 'success',
            message: '删除成功!'
          });
        })

      }).catch(() => {
        // this.$message({
        //   type: 'info',
        //   message: '已取消'
        // });          
      });
      // console.log(obj)

    }
  }
}
</script>
```

views/table/add

```
<template>
  <div class="app-container">
    <h2>增加台桌</h2>
    <div class="box">
      <el-form ref="form" label-width="80px">
        <el-form-item label="序号">
          <el-input v-model="index"></el-input>
        </el-form-item>

        <el-form-item label="名称">
          <el-input v-model="name"></el-input>
        </el-form-item>

        <el-form-item>
          <el-button type="primary" @click="add">增加</el-button>
        </el-form-item>
      </el-form>
    </div>
  </div>
</template>

<script>
import { add } from '@/api/table'
export default {
  data(){
    return {
      index: '',
      name: ''
    }
  },
  methods:{
    add(){
      if(this.index&&this.name){
        add({
          index: this.index,
          name: this.name
        }).then(res=>{
          // console.log(res)
          this.$message({
            message: '恭喜你，增加了一张桌子',
            type: 'success'
          });
          this.index = '';
          this.name = ''
        })
      }else{
        this.$message({
            message: '不能为空',
            type: 'warning'
        });
      }
      
    }
  }
}
</script>

```



3. 增加对应的接口文件

   api/table.js

```
import request from '@/utils/request'

// 获取桌子列表的接口
export function getList() {
  return request({
    url: '/v2/table/list',
    method: 'post',
  })
}

// 增加桌子的接口
export function add(data){  
  return request({
    url: "/v2/table/add",
    method: 'post',
    data
  })
}

// 删除桌子的接口
export function del(data){  
  return request({
    url: "/v2/table/del",
    method: 'post',
    data
  })
}

// 修改桌子的接口
export function update(data){  
  return request({
    url: "/v2/table/update",
    method: 'post',
    data
  })
}
```





# 答辩提交的文件

1. 源代码
2. 视频
3. ppt
4. 说明文档 （怎么把这个项目启动起来，需要哪些环境，以及其他的初始化操作...）
5. 接口文档（）
