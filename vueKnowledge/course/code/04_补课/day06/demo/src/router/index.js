import VueRouter from "vue-router";
import Vue from 'vue';
import HomeView from '../views/HomeView.vue'
Vue.use(VueRouter)


// path name component children redirect
// 动态路由
const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/about',
    name: 'about',
    component: ()=>import('../views/AboutView.vue')
  },
  {
    path: '/test',
    redirect: '/about'
  },
  {
    path: '/parent',
    name: 'parent',
    component: ()=>import('../views/ParentView.vue'),
    children: [
      {
        path: 'son1',
        name: 'son1',
        component: ()=>import('../views/Son1View.vue')
      },
      {
        path: 'son2',
        name: 'son2',
        component: ()=>import('../views/Son2View.vue')
      }
    ]
  },
  {
    path: '/active/:id?',
    name: 'active',
    component: ()=>import('../views/ActiveView.vue'),
    beforeEnter:(to,from,next)=>{
      console.log('beforeEnter')
      next()
    }
  }
]
const router = new VueRouter({
  mode: 'hash'  , // history
  routes
})


export default router