// 创建仓库对象
import Vue from 'vue';
import Vuex from 'vuex';

import user from './user'
Vue.use(Vuex);

// 创建一个仓库独享  vuex3的写法,     vuex4的写法改成了createStore()
export default new Vuex.Store({
  state: {  // 需要共享的数据就定义在state中
    count: 0,
    age: 18,
    name: 'young',
    list: [2,4,3,6,7,9,10]
  },
  getters:{ //提供统一去state的方法,  类似computed
    getOuList(state){
      return state.list.filter(item=>{
        return item % 2 == 0
      })
    }
  },
  mutations: {   
    // 定义修改的数据的方法,  this.$store.commit() 来触发里面的方法
    // 在mutaitions只能是同步代码,不能写异步
    changeage(state){   // 默认的第一个参数就是state
      state.age++
    },
    setage(state,payload){  // payload 就是第二个参数,commit()的第二个参数
      state.age = payload
    }
  },
  actions: {  // 可以写异步,但是修改也要通过mutation去修改  , $store.dispatch()来触发action
    asyncChangeAge(context){  // context 就是$store
      setTimeout(()=>{
        context.commit('changeage')
      },2000)
    }
  },
  modules: {
    user: user
  }
})

