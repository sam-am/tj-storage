import VueRouter from "vue-router";
import Vue from 'vue';
import HomeView from '../views/HomeView.vue'
Vue.use(VueRouter)


// path name component children redirect
// 动态路由
const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/about',
    name: 'about',
    component: ()=>import('../views/AboutView.vue')
  },
  {
    path: '/test',
    redirect: '/about'
  },
  {
    path: '/parent',
    name: 'parent',
    component: ()=>import('../views/ParentView.vue'),
    children: [
      {
        path: 'son1',
        name: 'son1',
        component: ()=>import('../views/Son1View.vue')
      },
      {
        path: 'son2',
        name: 'son2',
        component: ()=>import('../views/Son2View.vue')
      }
    ]
  },
  {
    path: '/active/:id?',
    name: 'active',
    component: ()=>import('../views/ActiveView.vue'),
    beforeEnter:(to,from,next)=>{
      console.log('beforeEnter')
      next()
    }
  }
]
const router = new VueRouter({
  mode: 'hash'  , // history
  routes
})

// 全局的前置守卫
// 路由跳转之前会执行
router.beforeEach((to,from,next)=>{
  console.log('跳转前执行')
  console.log(to);  // 目标路由信息  (去哪里)
  console.log(from) // 当前的路由信息 (从哪来)
  next()  // 如果写了这个钩子,就必须要执行执行,如果不执行就跳转不到目标路由
  // if(to.name=='son1'){
  //   next('/parent/son2')
  // }else{
  //   next();
  // }
})


// 全局的后置守卫
// 跳转后执行
router.afterEach((to,from)=>{
  console.log("跳转后再执行")
  console.log(to)
  console.log(from)
})


export default router