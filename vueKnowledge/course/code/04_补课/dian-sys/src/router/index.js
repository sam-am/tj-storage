import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'/'el-icon-x' the icon show in the sidebar
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },

  {
    path: '/register',
    component: () => import('@/views/register/index'),
    hidden: true
  },

  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },

  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [{
      path: 'dashboard',
      name: 'Dashboard',
      component: () => import('@/views/dashboard/index'),
      meta: { title: '楼面', icon: 'dashboard' }
    }]
  },
  {
    path: '/table',
    component: Layout,
    redirect: '/table/list',
    name: 'table',
    meta: { title: '台桌管理', icon: 'el-icon-s-help' },
    children: [
      {
        path: 'list',
        name: 'list',
        component: () => import('@/views/table/index'),
        meta: { title: '台桌列表', icon: 'table' }
      },
      {
        path: 'add',
        name: 'add',
        component: () => import('@/views/table/add'),
        meta: { title: '增加台桌', icon: 'tree' }
      }
    ]
  },
  {
    path: '/serie',
    component: Layout,
    redirect: '/serie/list',
    name: 'serie',
    meta: { title: '菜品分类管理', icon: 'el-icon-s-help' },
    children: [
      {
        path: 'list',
        name: 'list',
        component: () => import('@/views/serie/index'),
        meta: { title: '分类列表', icon: 'table' }
      },
      {
        path: 'add',
        name: 'add',
        component: () => import('@/views/serie/add'),
        meta: { title: '增加分类', icon: 'tree' }
      }
    ]
  },
  {
    path: '/dish',
    component: Layout,
    redirect: '/dish/list',
    name: 'dish',
    meta: { title: '菜品管理', icon: 'el-icon-s-help' },
    children: [
      {
        path: 'list',
        name: 'list',
        component: () => import('@/views/dish/index'),
        meta: { title: '菜品列表', icon: 'table' }
      },
      {
        path: 'add',
        name: 'add',
        component: () => import('@/views/dish/add'),
        meta: { title: '增加菜品', icon: 'tree' }
      }
    ]
  },

  {
    path: '/setting',
    component: Layout,
    redirect:'/setting/sys',
    meta: { title: '系统设置', icon: 'tree' },
    children: [
      {
        path: 'sys',
        name: 'Setting',
        component: () => import('@/views/setting/index'),
        meta: { title: '小程序设置', icon: 'dashboard' }
      },
      {
        path: 'account',
        name: 'account',
        component: () => import('@/views/setting/account'),
        meta: { title: '账户设置', icon: 'dashboard' }
      },
    ]
  },
 

  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
