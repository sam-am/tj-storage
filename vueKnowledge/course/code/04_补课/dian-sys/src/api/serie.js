import request from '@/utils/request'

export function list(data){
  return request({
    url:'serie/list',
    method: 'POST',
    data
  })
}

export function update(data){
  return request({
    url:'serie/update',
    method: 'POST',
    data
  })
}

export function del(data){
  return request({
    url:'serie/del',
    method: 'POST',
    data
  })
}

export function add(data){
  return request({
    url:'serie/add',
    method: 'POST',
    data
  })
}