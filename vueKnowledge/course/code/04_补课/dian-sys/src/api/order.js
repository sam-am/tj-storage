import request from '@/utils/request'

export function detail(data){
  return request({
    url:'order/detail',
    method: 'POST',
    data
  })
}

export function deldish(data){
  return request({
    url:'order/deldisk',
    method: 'POST',
    data
  })
}

export function buy(data){
  return request({
    url:'order/buy',
    method: 'POST',
    data
  })
}