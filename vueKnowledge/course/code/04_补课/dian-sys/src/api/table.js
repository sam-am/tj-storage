import request from '@/utils/request'

/*
  RESTful api 相同类型的操作的api名称一致;

  根据method的类型来区分
  get  // 通常是代表获取列表
  post // 增加
  put  // 更新
  DELETE // 删除
*/

export function getList() {
  return request({
    url: 'table/list',
    method: 'POST'
  })
}

export function del(data) {
  return request({
    url: 'table/del',
    method: 'POST',
    data
  })
}

export function updata(data) {
  return request({
    url: 'table/update',
    method: 'POST',
    data
  })
}

export function add(data) {
  return request({
    url: 'table/add',
    method: 'POST',
    data
  })
}


