import request from '@/utils/request'

export function login(data) {
  // console.log(data)
  return request({
    url: '/user/login',
    method: 'post',
    data
  })
}

export function getInfo(token) {
  return request({
    url: '/user/status',
    method: 'post'
    // params: { token }
  })
}

export function setting(data) {
  return request({
    url: '/user/setting',
    method: 'post',
    data
  })
}


export function getcode(data) {
  return request({
    url: '/code',
    method: 'post',
    data
  })
}

export function apply(data) {
  return request({
    url: '/user/apply',
    method: 'post',
    data
  })
}


export function update(data) {
  return request({
    url: '/user/update',
    method: 'post',
    data
  })
}



