import request from '@/utils/request'

export function list(data){
  return request({
    url:'dish/list',
    method: 'POST',
    data
  })
}

export function update(data){
  return request({
    url:'dish/update',
    method: 'POST',
    data
  })
}

export function del(data){
  return request({
    url:'dish/del',
    method: 'POST',
    data
  })
}

export function add(data){
  return request({
    url:'dish/add',
    method: 'POST',
    data
  })
}