
// 那个文件不需要用eslint来检查代码格式,就把上面这个注释写在这个文件的最开始  /* eslint-disable */
import { login, logout, getInfo } from '@/api/user'
import { getToken, setToken, removeToken } from '@/utils/auth'
import { resetRouter } from '@/router'

const getDefaultState = () => {
  return {
    token: getToken(),
    name: '',
    avatar: '',
    user: {}
  }
}

const state = getDefaultState()

const mutations = {
  RESET_STATE: (state) => {
    Object.assign(state, getDefaultState())
  },
  SET_TOKEN: (state, token) => {
    state.token = token
  },
  SET_NAME: (state, name) => {
    state.name = name
  },
  SET_AVATAR: (state, avatar) => {
    state.avatar = avatar
  },
  SET_USER: (state, user) => {
    state.user = user
  }
}

const actions = {
  // user login

  // context  == $store.commit
  login({ commit }, userInfo) {
    const { username, password } = userInfo
    // const username = userInfo.username

    return new Promise((resolve, reject) => {  // 调用了这个action返回的是一个promise

      // 这login是从@/api/user导进来的一个方法
      login({ account: username.trim(), pw: password }).then(response => {
        console.log(response)
        const { data } = response // 把登录成功后的对象做了一个解构, 因为data里面有token

        // console.log(response)
        commit('SET_TOKEN', data.token)  // 把返回返回的token,放在state的token属性
        setToken(data.token)  //  把返回返回的token, 放在cookie里面
        resolve()  // 到这里的就说明promise执行结束
      }).catch(error => {
        console.log(error)
        reject(error)
      })
    })


  },

  // get user info
  getInfo({ commit, state }) {
    return new Promise((resolve, reject) => {
      getInfo(state.token).then(response => {
        const { data } = response

        if (!data) {
          return reject('Verification failed, please Login again.')
        }

        const { account } = data
        commit('SET_NAME', account)
        commit('SET_AVATAR', 'http://www.young1024.com:3008/avatar/1630920351067.png'),
        commit('SET_USER',data)
        resolve(data)
      }).catch(error => {
        reject(error)
      })
    })
  },

  // user logout
  logout({ commit, state }) {
    return new Promise((resolve, reject) => {
      // logout(state.token).then(() => {
      //   removeToken() // must remove  token  first
      //   resetRouter()
      //   commit('RESET_STATE')
      //   resolve()
      // }).catch(error => {
      //   reject(error)
      // })

      removeToken() // must remove  token  first
      resetRouter()
      commit('RESET_STATE')
      resolve()
    })
  },

  // remove token
  resetToken({ commit }) {
    return new Promise(resolve => {
      removeToken() // must remove  token  first
      commit('RESET_STATE')
      resolve()
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}

