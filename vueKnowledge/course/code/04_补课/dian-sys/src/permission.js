import router from './router'
import store from './store'
import { Message } from 'element-ui'
import NProgress from 'nprogress' // progress bar
import 'nprogress/nprogress.css' // progress bar style
import { getToken } from '@/utils/auth' // get token from cookie
import getPageTitle from '@/utils/get-page-title'



NProgress.configure({ showSpinner: false }) // 进度条插件的配置

const whiteList = ['/login','/register'] // 导航的白名单, 哪些路径的不需要登录状态的


// 路由的全局前置守卫, 所有的路由跳转跳转都会经过这个钩子,并且在这里面可以做路由拦截
router.beforeEach(async(to, from, next) => {

  NProgress.start() // 打开进度条

  document.title = getPageTitle(to.meta.title)  // 设置页面的title

  const hasToken = getToken() // 从cookie中取token

  if (hasToken) {  // 如果有token,就说明是有登录状态的
    if (to.path === '/login') { // 如果有登录的情况下还访问login,
      
      next({ path: '/' }) //就拦截,让它跳转到 /
      NProgress.done()  //关闭进度条
    } else {  // 不是login页的情况

      const hasGetUserInfo = store.getters.name  // 从仓库中取名字
      // const hasGetUserInfo = 'young' // 从仓库中取名字

      if (hasGetUserInfo) { // 判断是否有名字
        next() // 有名字改去哪里就去哪里
      } else {
        try {
          // 再调用仓库中的action中的方法:在这个中去获取用户信息
          await store.dispatch('user/getInfo')

          next()
        } catch (error) {
          // 能到这个catch方法里面来,就说明try里面的代码出错了,就是获取用户信息出错了
          
          await store.dispatch('user/resetToken') // 调用action中重置token

          Message.error(error || 'Has Error')  // 弹出一个报错的弹框
          next(`/login?redirect=${to.path}`)   // 跳转到登录,并且带上to的路径作为参数
          NProgress.done()
        }
      }
    }
  } else { // 没有 token的情况
  
    if (whiteList.indexOf(to.path) !== -1) {// 在白名单内,就直接跳转
     
      next()
    } else {
      // 如果不在就跳转登录页,并且带上to的参数
      next(`/login?redirect=${to.path}`)
      NProgress.done() // 关闭进度条
    }
  }
})

router.afterEach(() => {  // 路由跳转完成后, 都关闭进度条
  // finish progress bar
  NProgress.done()
})
