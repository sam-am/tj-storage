# 侦听器

```
<template>
  <div>
    <h2>侦听器</h2>
    <!-- 
      监听data的变化,如果有变化后会执行的一个函数
    -->
    <div>
      {{count}}
      <button @click="count++">count++</button>
    </div>

    <div>
      <h3>拼接全名</h3>
      
      <input type="text" v-model="firstname">+
      <input type="text" v-model="lastname">=
      <input type="text" v-model="fullname">
    </div>

    <br>
    <hr>
    <h3>监听对象</h3>
    <input type="text" v-model="user.fname">+
    <input type="text" v-model="user.lname">=
    <input type="text" v-model="user.fullname">
    <br>
    {{user}}
    <br>
    <button @click="changeuser">change-user</button>

  </div>
</template>

<script>
  export default{
    data(){
      return {
        count: 0,
        firstname: '',
        lastname: '',
        fullname: '',
        user: {
          fname: 'young',
          lname: 'zlin',
          fullname: '',
          hobby:['code','run']
        }
      }
    },
    methods: {
      changeuser(){
        this.user = {
          fname: 'young',
          lname: 'zzzzzzz',
          fullname: 'youngzlin'
        }

        // this.user.hobby[0] = 'html5'
        // this.user.hobby.push('sing')
      }
    },
    watch: {  // 侦听器, 他的key肯定是来自于data中的一个变量, value是一个函数
      count: function(newval, oldval){  // 函数有两个参数: 1.修改后,  2.修改前 
        console.log('修改后:'+newval)
        console.log('修改前:'+oldval)
        console.log('count发生改变了')
      },
      firstname(newval){
        this.fullname = newval+' '+this.lastname
      },
      lastname(newval){
        this.fullname = this.firstname+' '+ this.lastname
      },
      // user(){  
      //   // user是一个对象,这样监听的话,里面的属性值发生变化是不会触发这个监听函数的
      //   // 要整个user重新赋值的时候才会被触发
      //   console.log('user变化了')
      // },
      'user.fname':function(newval,oldval){  // 监听user.fname变化后就会执行
        console.log(newval)
        console.log(oldval)
      },
      // 上面的写法,没个属性都要写一遍
      // 如果监听user的全部属性,就可以使用深度监听
      // value就不是一个函数了,而是一个对象
        // handler  user变化的时候要出发的函数
        // deep 是否开启深度监听的开关
      user: { 
        handler(newval,oldval){
          // console.log(newval,oldval)
          // 深度监听的两个参数的值是一样的
          this.user.fullname = newval.fname+' ' + newval.lname
          console.log(newval)
        },
        deep: true,
        immediate: true  // 主动触发一次handle
      }
    }
  }
</script>
```





# 组件基础

组件

​        在开发者的角度来看就是自定义标签;

​        在这个组件里面有他自己的页面样式,结构,逻辑

​        ul li  默认就有列表样式

​        a 可以有跳转功能的标签

​        img 可以显示图片的标签

​        ...

​        比如我们现在写项目的时候,有一大串功能是相同的, 可能很多页面中都需要被调用

​        那么这个功能模块就可以封装一个组件中, 可以更好方便调用

​        怎么定义组件: 

​        在我们的开发环境中,一个.vue就是一个组件

​        在组件内再调用别的组件,就要先注册再使用



## 定义组件

src/components/List.vue

```
<!-- 定义一个列表组件 -->
<template>
  <div>
    <ul>
      <li v-for="item in list" :key="item.name">{{item.name}}--{{item.age}}</li>
    </ul>
  </div>
</template>

<script>
  export default {
    data(){
      return {
        list: [
          {name: 'yy1',age:18},
          {name: 'yy2',age:17},
          {name: 'yy3',age:18},
          {name: 'yy4',age:30},
          {name: 'yy5',age:88},
        ]
      }
    }
  }
</script>

<style scoped>
  /* 
    scoped是当前的样式只会在当前的组件中生效 (局部样式)  
    否则就是全局样式
  */
  ul li:nth-child(even){
    background-color: gray
  }
</style>
```

src/components/btn.vue

```
<template>
  <a class="mybtn">测试的按钮</a>
</template>

<style scoped>
  .mybtn{
    display: inline-block;
    width: 150px;
    height: 50px;
    background-color: blueviolet;
    border-radius: 10px;
    text-align: center;
    line-height: 50px;
    color: #fff;
    cursor: pointer;
    margin-right: 10px;
  }
</style>
```



## 调用组件

App.vue

调用之前要先注册，然后再使用

要注意命名方式。 大驼峰的名称方式改成-的形式在调用

```
<template>
  <div>
    <list></list>
    <list></list>
    <list></list>
    <br>
    <hr>

    <MyBtn></MyBtn>

    <my-btn></my-btn>
  </div>
</template>

<script>
  // 使用组件: 
  // 1. 导入组件
  // 2. 注册组件  componets  key组件名称(要使用这个组件的名称) value就是组件对象
  // 3. 调用组件  注册的名称是什么,就当做标签名称使用就可以了
        // 如果是大驼峰的命名规则, 可以改成 -的形式来调用, 把大写字母都改成小写,在加上-
  import List from './components/List.vue'

  // @代表src的路径
  import mybtn from '@/components/btn.vue'
  export default{
    data(){
      return {
        
      }
    },
    components: {
      list: List,
      MyBtn: mybtn
    }
  }
</script>
```





# 思考与练习

- methods  computed  watch 他们之间的区别
- 拼接全名的案例： 是否用compute再实现一遍。
- 对组件的理解，你能想到页面中哪些功能可以封装成组件（为什么），试着封装一下





# props

用来向下传值，两种定义props的方式，对象，数组。

```
  <my-list :list="mylist" :flag="false"></my-list>
```

定义： 

```
<!-- 
  也是自定义的组件
  也是一个列表组件
  接收一个列表,然后把这个列表渲染出来
  这个列表不是自己的,而是从外面传进来的
-->

<template>
  <div>
    <h3>需要传值的列表组件</h3>
    <ul :class="{'stripe':flag}">
      <li v-for="(item,index) in list" :key="index">{{item.name}}:{{item.age}}</li>
    </ul>
  </div>
</template>

<script>
  // props的定义方式有两种
  // 1. 数组:  定义很简洁,但是没有太多的功能
  // 2. 对象:  定义的时候稍微麻烦一点,但是它的功能,比如: 传值的数据类型,默认值
  //    key: 属性名称
  export default {
    // props: ['list', 'flag'], 数组的形式
    // props选项就是定义当前组件,可以接受哪些属性
    // 比如上面定义的list, 就可以像data的使用方式来去使用list
    // 但是在组件内部去修改list

    // list 是接受一个数组还然后渲染它
    // flag 是否需要斑马线的效果

    props: {  // 对象的形式
      // list: Array,  //规定list为数组类型
      list: {
        type: Array,  // 规定数据类型
        required: true  //规定必传
      },
      flag: {
        type: Boolean, 
        default: true   // 默认值 , 没有传flag的时候,那么flag的值就为true
      }
    },
    mounted(){
      // console.log(this.flag)
      // console.log(this.list)
    }
  }
</script>

<style scoped>
  .stripe li:nth-child(even){
    background-color: cornflowerblue;
  }
</style>
```



# $emit

暴露自定义事件

主要可以用来向上传值  （子向父）

mybox.vue

```
<template>
  <button @click="func">子组件中定义的按钮</button>
</template>

<script>
  export default {
    methods: {
      func(){
        // console.log('AAAAAAA')

        // 子向父传值 
        // 定义了一个变量想要转给APP父组件 
        // 不能直接转,要通过事件,

        // 在组件里面可以自定义事件, this.$emit()
        // $emit()  
        // 参数1: 就是事件的名称
        // 参数2... : 需要上传的值 : 对应函数的参数

        const num = 100;  
        const age = 18;
        this.$emit('myevent',num, age)  // 当前组件就有自定义的事件 myevent
      }
    }
  }
</script>
```

App.vue

```
 <mybox @myevent="appfunc"></mybox>
 
 ...
 
 methods: {
      appfunc(n,a){
        console.log("appfunc:"+n+', age='+a)
      }
    },
```



# slot

在子组件中要显示出来组件标签之间的内容

app.vue

```
<myslot>
      <h2>这是myslot里面的h2</h2>
      <p>这是div里面的p标签的内容</p>
    </myslot>
```

MySlot.vue

```
<template>
  <div>
    <!-- 组件标签中间的内容 ,都会显示在slot的位置  -->
    <!-- 
      当组件里面的内容是需要在调用的时候的传进来的时候,就需要用到slot
      定义了一些ui组件的时候  (后面用到ui框架的时候,有些自定义的内容就是用slot做的)
    -->
    <slot></slot>
  </div>
</template>
```



# 动态组件

可以根据组件名称去渲染组件

app.vue

```
<!-- 动态组件 -->
<!-- 
  模仿一个app底部tab
  首页  分类   购物车  我的
  对应点击的时候,就切换到对应的组件
-->
<template>
  <div>
    <!-- 条件渲染 -->
    <index v-if="active=='index'"></index>
    <type v-else-if="active=='type'"></type>
    <cart v-else-if="active=='cart'" ></cart>
    <my v-else-if="active=='my'"></my>

    <hr>
    <!--  动态组件 -->
    <!--  
      is 是指定组件名称 
      is的值是有这个组件的,就渲染这个组件
      没有这个组件不渲染
    -->
    <component :is="active"></component>

    <div class="tab">
      <ul>
        <li @click="change('index')">首页</li>
        <li @click="change('type')">分类</li>
        <li @click="change('cart')">购物车</li>
        <li @click="change('my')">我的</li>
      </ul>
    </div>
  </div>
</template>

<script>
  import index from './views/pages/Index.vue'
  import type from './views/pages/Type.vue'
  import cart from './views/pages/Cart.vue'
  import my from './views/pages/My.vue'

  export default{
    data(){
      return {
        active: 'my'
      }
    },
    methods: {
     change(act){
      this.active = act
     }
    },
    components: {
      index,
      type,
      cart,
      my
    }
  }
</script>

<style>
  *{
    margin: 0;
    padding: 0;
    box-sizing: border-box;
  }
  .tab ul{
    display: flex;
    align-items: center;
    height: 50px;
  }
  .tab ul li{
    list-style: none;
    flex: 1;
    display: inline-flex;
    align-items: center;
    justify-content: center;
    cursor: pointer;
  }

  .tab {
    position: fixed;
    bottom: 0;
    left: 0;
    width: 100%;
    height: 50px;
    border: 1px solid red;
  }
</style>
```



# 思考与练习

- vue的基础部分已经学的差不多，可以怎么运用到项目中（用vue重构一下二阶段的项目，那怕是一个页面也好）

- 回顾今天的内容  

  - 组件的传值方式，向下传，向上转
  - slot的作用
  - 动态组件

- 找下有没有自己喜欢的项目。自己想做的项目。 找出来记录好。

  可以**以组**单位： 

  渠道： 

  日常使用的app,或者网站

  各大设计网站中找

  https://www.axureshop.com/

  https://www.pmdaniu.com/explore

  https://js.design/square?page=2

  
