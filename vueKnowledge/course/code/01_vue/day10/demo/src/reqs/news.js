
// 1
import myaxios from "@/utils/axios";


// 2
export function getlist(data){
  return myaxios.post('/news',data)
}

// xxxxxxxxxxxxxxx
// const getlist2 = myaxios.post('/news',{data})
// export {getlist2}



export function addnews(data){
  return myaxios({
    url: '/news',
    method:'post',
    params: data
  })
}

export function delnews(){
  return myaxios.delete('/news',{
    data
  })
}

// RESTful 的代码接口风格
// 新闻的操作为例
// 增删改查  对应的4接口
//           /news/add  /news/del/:id  /news/update  /news/list
//           /table/add  /table/del/:id  /table/update  /table/list
//           /user/add  /user/del/:id  /user/update  /user/list

// 虽然说接口的定义都是后端给的 , 后端也可能为了少一个news ,

// RESTful 就换成不同提交方法来对不同的操作
// get: list
// put : add
// delete: del
// post: updata

// 和news相关的接口 都叫news
// 根据不同提交方法来区分不同的操作
// axios.post('/news',{id})   //修改
// axios.get('/news')         //获取列表
// axios.put('/news')         // 添加
// axios.delete('/news',{id}) //删除
