import myaxios from "@/utils/axios";

// 获取新闻列表
export function getlist(data){
  return myaxios.post('/news',data)
}

// 搜索新闻
export function search(data){
  return myaxios.post('/search',data)
}