import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

// 把请求工具挂载到vue实例(app)中
// 就可以全局使用 请求工具了
// 注意点  vue2是允许这样写的,  但是在vue3中就不建议

import myaxios from './utils/axios'

const app = createApp(App)

app.config.globalProperties.$axios = myaxios

app.use(store)
app.use(router)
app.mount('#app')
