import axios from 'axios';
import qs from 'qs'

// 1.创建实例
const myaxios = axios.create({
  baseURL: 'https://www.young1024.com:3002'
})

// 请求拦截
myaxios.interceptors.request.use(function(config){
  // 使用qs 来处理post的参数
  config.data = qs.stringify(config.data)
  return config
})

// 响应拦截
myaxios.interceptors.response.use(function(res){
  return res.data
})

export default myaxios

