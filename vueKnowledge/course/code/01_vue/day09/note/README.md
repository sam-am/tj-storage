# 在vue项目中使用axios

## 正常使用

1. 要先安装axios

```
npm i axios
```

2.封装axios

utils/axios.js

```
import axios from 'axios';
import qs from 'qs'

// 1.创建实例
const myaxios = axios.create({
  baseURL: 'https://www.young1024.com:3002'
})

// 请求拦截
myaxios.interceptors.request.use(function(config){
  // 使用qs 来处理post的参数
  config.data = qs.stringify(config.data)
  return config
})

// 响应拦截
myaxios.interceptors.response.use(function(res){
  return res.data
})

export default myaxios
```



3.在组件中使用

先导入自定义好的axios,然后再使用

```
<script>
  import myaxios from '@/utils/axios';
  export default {
    data(){
      return {
        info: {}
      }
    },
    mounted(){
      myaxios.post('/newsinfo',{newsid:'5f9938dd232a2c205391b111'}).then(res=>{
        // console.log(res)
        this.info = res.data
      })
    }
  }
</script>
```



## 另外一种使用方式

// 把请求工具挂载到vue实例(app)中

// 就可以全局使用 请求工具了

// 注意点  vue2是允许这样写的,  但是在vue3中就不建议

main.js

```
import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

// 把请求工具挂载到vue实例(app)中
// 就可以全局使用 请求工具了
// 注意点  vue2是允许这样写的,  但是在vue3中就不建议

import myaxios from './utils/axios'

const app = createApp(App)

app.config.globalProperties.$axios = myaxios

app.use(store)
app.use(router)
app.mount('#app')
```

在组件中使用

```
<script>
  export default {
    data(){
      return {
        info: {}
      }
    },
    mounted(){
      this.$axios.post('/newsinfo',{newsid:'5f9938dd232a2c205391b111'}).then(res=>{
        // console.log(res)
        this.info = res.data
      })
    }
  }
</script>
```





## 还有一种使用方式

把某个请求封装到一个方法里面，要调用这个请求就调用这个方法就可以了

http/news.js

```
import myaxios from "@/utils/axios";

// 获取新闻列表
export function getlist(data){
  return myaxios.post('/news',data)
}

// 搜索新闻
export function search(data){
  return myaxios.post('/search',data)
}
```



在组件中调用方法

```
<script>
  import { getlist, search } from '@/http/news.js'
  export default {
    mounted(){
      getlist({type:'文化'}).then(res=>{
        console.log(res)
      })
      
      getlist({type:'NBA'}).then(res=>{
        console.log(res)
      })

      search({keyword:'球'}).then(res=>{
        console.log(res)
      })

    }
  }
</script>
```









# vue中使用rem

```
<!DOCTYPE html>
<html lang="">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <link rel="icon" href="<%= BASE_URL %>favicon.ico">
    <title><%= htmlWebpackPlugin.options.title %></title>
    <style>
      html{
        font-size: 0.5px
      }
    </style>
  </head>
  <body>
    <noscript>
      <strong>We're sorry but <%= htmlWebpackPlugin.options.title %> doesn't work properly without JavaScript enabled. Please enable it to continue.</strong>
    </noscript>
    <div id="app"></div>
    <!-- built files will be auto injected -->
  </body>

  <script>
    function change(){
      var uiw = 750;      // 设计稿的宽度
      var dw = window.innerWidth;  // 当前浏览的宽度
      document.documentElement.style.fontSize = dw/uiw + 'px';  // 动态的设置html的font-size
      // 设计稿的宽度是多少,然后我们写代码的是就写多少的rem
    }

    // 绑定加载事件
    window.onload = change

    // 绑定 窗口变化大小的事件
    window.onresize = change
  </script>
</html>

```

