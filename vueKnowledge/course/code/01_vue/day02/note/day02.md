# 常用指令

指令，在vue里面就是v- 开头的属性的，可以对当前标签有一些操作，不同的指令就有不同的做用



如果要把一个变量的内容显示在页面中 可以使用 {{ }}  或者 v-html 、v-text  来做。

如果我的变量是一个连接地址，要绑定在a标签的href属性上



## v-bind

总结: 只要是需要传变量的,都需要用到v-bind,  在引号里面也是可以写表达式

```
<template>
  <div>
    <!--  {{}}  只能在内容中用个,不能用在属性中   -->
    <!-- <a href="{{link}}">跳转到博客</a> -->
    <!-- 如果要绑定属性,就需要用到 v-bind  指令
      语法: 
      v-bind:属性名称="变量名"
      简写:  直接写: 省略v-bind
        真实的开发中是简写的方式用的比较多多的
    -->
    <a v-bind:href="link" v-bind:target="tar">跳转到博客</a>
    <div>
      <a :href="link" :target="tar">跳转到博客-简写的方式</a>
    </div>
    <div>
      <!-- 
        v-bind 绑定布尔值 
        如果没有v-bind 其实就是绑定的字符串
        使用了v-bind 就是一个变量
        值为 true , 就是这个属性生效
        为false: 这个属性不生效(就可以说不要这个属性),

        总结: 只要是需要传变量的,都需要用到v-bind,  在引号里面也是可以写表达式
      -->
      <button :disabled="true">测试的按钮</button>
      <a :title="1+1">测试的链接 </a>
      <a :title="msg.split('').reverse().join('')">测试的链接 </a>
    </div>
  </div>
</template>

<script>
  export default {
    data(){  
      return {
        msg: 'hello 2208',
        link: 'http://www.young1024.com:3008/',
        tar: '_blank'
      }
    }
  }
</script>
```



## 计算属性

```
<template>
  <div>
    <h2>{{ msg.split('').reverse().join('-') }}</h2>
    <h2>{{ msg.split('').reverse().join('') }}</h2>
    <h2>{{ msg.split('').reverse().join('-') }}</h2>

    <hr/>
    <div>方法的形式:</div>
    <h2> {{ reverseMsg() }} </h2>
    <h2> {{ reverseMsg() }} </h2>
    <h2> {{ reverseMsg() }} </h2>

    <hr/>
    <div>计算属性的形式:</div>
    <h3>{{ cacheMsgChange }}</h3>
    <h3>{{ cacheMsgChange }}</h3>
    <h3>{{ cacheMsgChange }}</h3>
    <hr/>
    <input type="text" v-model="msg" />
  </div>
</template>
<script>
  //迭代1 需求: 需要反转显示字符串msg, 3次
  //迭代2 需求: 需要反转显示字符串msg,要用-隔开, 3次 ,
    // 为了减少操作,可以用方法实现, 只要修改一次,调用了这个方法的都修改了(调用多少次,方法就执行多少次)
  
    // 计算属性的方式 computed: 定义的时候和方法一样,使用的时候和data一样
    // 当依赖发生变化的时候才会执行,否则就是调用的缓存
  export default {
    data(){  
      return {
        msg: '测试的文字'
      }
    },
    methods:{
      reverseMsg(){
        console.log('a')
        return this.msg.split('').reverse().join('---')
      }
    },
    computed:{
      cacheMsgChange(){
        // 只有依赖发生改变时才会执行这个方法,否则就是调用的缓存
        console.log('AAAAAAAAAAAAAAAAAAAAAAAAAA');
        return this.msg.split('').reverse().join('+')
      }
    }
  }
</script>

```



## 绑定样式

```
<template>
  <div>
    <!-- box样式不是写死的,而是要通过isBox这个变量来决定是否要显示 -->

    <!--  
      v-bind:class 绑定样式:  有两种写法: 
      1. 对象的形式:
        key-是否要显示的样式名
        value-决定是否要显示的样式名的布尔值

      2. 数组的形式
        可以绑定多个样式
        每一个下标都是变量,每个变量都会解析成样式名称
        在[]可以写三木
    -->
    <div :class="{'box': isBox}">box</div>
    <div :class="[big,bor,bg]">测试文字</div>
    <br />
    <div :class="[big, bor, flag?bg:'']"></div>
    <button v-on:click="flag = !flag">change-bg</button>

    <hr />
    <!-- 
      
      行内样式 : v-bind:style  也有两种形式
      1.对象的形式
        key-行内样式的属性名
        value-属性值

      2.数组的形式
        每个元素都一个样式对象
    -->
    <!-- <div style="width:50px; height:50px;border: 1px solid red"></div> -->
    
    <div :style="{
      width: '50px',
      height: '50px',
      border: '1px solid red',
      'background-color': 'blue'  // 要改成小驼峰: backgroudColor ,要么就加 'background-color'
    }"></div>
    <div :style="[obj,obj2]">测试行内样式的文字</div>
  </div>
</template>

<script>

  export default {
    data(){  
      return {
        isBox: false,
        big: 'rect',
        bor: 'bor',
        bg: 'bg',
        flag: false,
        obj: {
          color: 'yellow',
          fontSize: '30px'
        },
        obj2: {
          backgroundColor: 'red'
        }
      }
    }
  }
</script>

<style>
  .box{
    width: 100px;
    height: 100px;
    border: 1px solid red;
  }

  .bor{
    border: 3px solid blue;
  }
  .bg{
    background-color: brown;
  }
  .rect{
    width: 100px;
    height: 100px;
  }
</style>
```



## 思考与练习

- v-bind的左右，还有哪些常用属性需要绑定？ 举例说明
- 计算属性的作用，与方法有什么区别，哪些场景用计算属性比较合适
- 绑定样式的方式有哪些？ 如何设计一个vip不同等级的标识样式 ，vip1-3  ,svip
- 做一个页面效果，点击的时候每次让字体大小+1





