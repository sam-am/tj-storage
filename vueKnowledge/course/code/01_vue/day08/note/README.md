# 网络请求

ajax, 就是异步请求，可以不刷新页面的情况下，去服务器中拿到数据

就是向服务拿数据的工具，通过接口去拿。

## XMLHttpRequest

​	浏览器对XMLHttpRequest对象的支持度不足, 创建 XMLHttpRequest 对象时需要对IE浏览器做的兼容解决。 -  ActiveXObject

​	回顾：XHR

* readyState

* 0-4，0表示未初始化，4表示请求已完成

* status（HTTP响应状态码）

  * 200：**OK**，成功

  * 3XX【重定向系列的状态码】
    * 301：永久重定向

    * 302：临时重定向

    * 307：内部浏览器（缓存）重定向

  * 4XX【错误系列】
    * 400：bad request，错误请求

    * 401：鉴权失败
    * 403：禁止访问 
    * 404：找不到对应的资源 
    * 405：方法不被允许

  * 5XX【服务器错误，环境问题】
    * 500：服务器内部错误（代码、环境问题）

    * 502：bad Getway，错误网关

  **使用XHR请求全国高校数据接口**

  接口地址


​				在note文件夹中有两个接口文档

​					都是是使用的（前提服务器正常的情况下）

​				卖座网： http://www.young1024.com:1234/			

​				点餐系统的接口文档： 

​				链接: https://www.apifox.cn/apidoc/shared-41432d8e-f3ed-422f-ae12-bcbf6d083739  访问密码 : sz2208 



在请求接口之前，先测试一把接口是否是好的

```
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title></title>
</head>
<body>

<script type="text/javascript">
	// 1. 创建xhr对象 （这个对象就有发起异步请求的能力）
	var xhr = new XMLHttpRequest();

	// 2.绑定函数 状态发生改变时要执行的函数
	// 这些api最好就看文档， w3school 菜鸟  MDN
	xhr.onreadystatechange = function(){
		// 3. 监听状态的变化，然后再做处理
		// xhr.readyState  对象的状态
		// xhr.status  //请求的状态
		if(xhr.readyState ===4 && xhr.status===200){
			// 请求成功
			// 打印返回值
			// xhr.responseText  
			console.log(xhr.responseText  )
			var obj = JSON.parse(xhr.responseText)
			console.log(obj)
		}	
	}

	// 4. 打开请求 open()
	// 参数1： 请求类型    参数2： 接口地址
	// 此时还不会发送请求，就好像是在浏览器中输入了地址，但是还没有敲回车
	xhr.open('get','http://www.young1024.com:1234/city')

	// 5. 发送请求 send()  // 就好比是巧了回车
	xhr.send()
</script>
</body>
</html>
```



## jq.ajax

是jq给我们封装好的一方法，可以直接使用

就可以不需要像上面那样写这么多复杂的代码了

```
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title></title>
</head>
<body>
<script src="https://cdn.bootcdn.net/ajax/libs/jquery/3.6.1/jquery.js"></script>

<script type="text/javascript">
	$.ajax({
		url:'http://www.young1024.com:1234/city',
		type: 'GET',
		success:function(data){
			// console.log(data)
		}
	})

	//直接调用 $.ajax() 就可以发起请求了
	// url  接口的地址
	// type 请求类型
	// data 接口的参数
	// success  接口请求成功后的回调
	// ...
	$.ajax({
		url:'http://www.young1024.com:1234/hot',
		type: 'GET',
		data:{
			pageSize:3
		},
		success:function(data){
			console.log(data)
		}
	})
</script>
</body>
</html>
```



## fetch

html5新加的api用于网络请求，基于promise封装的

```
// fetch()
// 参数1: 就是接口地址
// 参数2: 就是配置和其他参数

fetch('http://www.young1024.com:1234/city').then(res=>{
  // console.log(res)  
  // 此时的res还是一个promise对象，不是接口返回回来的值
  // 此时的res是当前请求信息

  // 如果要拿到接口返回的值，就要调用json(), 然后还要返回
  // console.log(res.json())
  // 还是返回的一个promise，就可以在第二个then里面返回的对象
	return res.json()  
}).then(data=>{
	console.log(data)
})
```



## axios

也是封装好的请求工具，也是基于promise

特点： 

- 可以运行在浏览器，也可以运行服务端（服务器node.js）

- 能够拦截 请求和响应
- 自动转换json数据



```
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>axios</title>
</head>
<body>
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
<script type="text/javascript">
	// console.log(axios)

	// 发起一个get请求
	// 参数1： 接口地址
	// 参数2： 是相关的配置对象
	axios.get('http://www.young1024.com:1234/city').then(res=>{
		console.log(res)  // res是当前请求的信息对象
		console.log(res.data)  // res.data才是返回的值
	})

	axios.get('http://www.young1024.com:1234/hot',{
		params: {  // get请求的参数
			pageSize: 3
		}
	}).then(res=>{
		console.log(res.data)
	})

	// 发起一个post请求
	// {type: '文化'}
	// 默认是以对象形式传参数的
	// 而我们news接口，他的参数不是要的对象，
	// 而是x-www-form-urlencoded(表单)类型的参数
	axios.post('https://www.young1024.com:3002/news','type=文化&size=3').then(res=>{
		console.log('----------------------')
		console.log(res)
	})

</script>
</body>
</html>
```



## 思考与练习

- 了解几种请求工具，分别说出他们的优势，劣势

- 熟悉axios的api ,练习get请求和post请求， 接口就去文档中找

  

- 浏览器axios的文档，还有其他的api的用法，能够从文档中学习到全局配置以及相应拦截和请求拦截













































## 接口测试工具

**postman**              // 以前用的比较多

poatwoman

//下面这两种是现在比较流行的，（因为功能比较多，自动测试，压力测试，也可以生成测试报告， 还可以生成**接口文档**）

apifox             

**Apipost**     下载下来体验一把

![image-20220907144155389](assets/image-20220907144155389.png)



![image-20220907145117488](assets/image-20220907145117488.png)







![image-20220907150545251](assets/image-20220907150545251.png)







![image-20220907162536969](assets/image-20220907162536969.png)







## 跨域

![image-20220907161931263](assets/image-20220907161931263.png)



只要访问的页面地址和接口地址不一样都会造成跨域： 同源策略

端口，域名、ip，协议

还有一种可能，参数类型不对也会造成跨域

解决跨域的方法：

- jsonp   只能发get请求 （基本没有在用的了）
- 代理    （通常只在开发环境中使用）
- cors  从服务器端就允许这个接口跨域（目前都是用这种方式解决的）

