import { createRouter, createWebHashHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/about',
    name: 'about',
    component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue')
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('../views/Login.vue')
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

router.beforeEach((to,from,next)=>{
  // 登录拦截
  // 除了登录页,其他页面都需要登录后(登录状态)才可以访问
  // cookie localStorage session sessionStorage

  /*
    因为这里是全局守卫,所有路由变化都会经过这里
    一定要注意逻辑: 跳转login的时候也会经过这个钩子 (很容易造成死循环)
  */

 
   // const isLogin = true // 当做是登录状态, true:已登录, false:没有登录
  /*
    if(isLogin){
      next();
    }else{
      next('/login')
    }
    // 错误的示范,这样会一直的跳转到login (死循环)
  */
  
  // 1. 先判断是否login页, 就让它直接过去
  // 2. 不是login页面, 就判断登录状态,
      // 如果有登录就让它走
      // 如果没有登录,让它跳转到登录页
    
  let isLogin = false // 当做是登录状态, true:已登录, false:没有登录
  
  // 动态: 就是isLogin不是写死的,没每次都要去 本地储存中拿到登录状态, 然后再赋值
  // 登录页: 把登录状态保存到本地存储中
  
  // 从本地存储中拿到登录状态然后再给isLogin赋值

  let user = localStorage.getItem('user')
  // console.log(user)
  if(user){
    isLogin = true
  }else{
    isLogin = false
  }

  if(to.name=='login'){  // 判断是否是login页
    next()  // 是login页,就直接过去
  }else{
    // console.log('不是登录页')
    // 不是 的情况就要判断是否有登录状态
    if(isLogin){  // 判断登录状态
      next()  // 有登录就直接走
    }else{
      // 没有登录的情况  跳转到登录页
      next('/login')
    }
  }
  // console.log(to)
  // next()
})


export default router
