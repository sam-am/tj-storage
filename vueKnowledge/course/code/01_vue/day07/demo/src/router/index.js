
import { createRouter, createWebHashHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/about',
    name: 'about',
    component: () => import('../views/AboutView.vue')
  },
  {
    path: '/test/:id?',
    name: 'test',
    component : ()=>import('../views/Test.vue'),
    beforeEnter: (to,from)=>{  // 路由的独享的守卫 只有访问到这个路由的时候才会执行
      // 如果访问的是test/aaaaaaa
      // 因为在全局的前置守卫中会跳转到 /, 所以就不会经过这个钩子了
      // 先执行全局前置,然后再执行路由独享
      console.log('访问了test')
    }
  },
  {
    path: '/parent',
    component:()=>import('../views/Parent.vue'),
    children: [
      {
        path: '/son1',   //在子路由的配置中通常是不加/ , /代表根路径
        name: 'son1',
        component: ()=>import('../views/Son1.vue')
      },
      {
        path: 'son2',
        name: 'son2',
        component: ()=>import('../views/Son2.vue')
      }
    ]
  } 
]

// 导航守卫
// 监听路由的变化
// 路由 从哪里来 要到哪里去  
// 某个守卫的钩子中还可以做路由的拦截

const router = createRouter({
  history: createWebHashHistory(), 
  routes
})


/*
全局的前置守卫 !
路由跳转前会执行

接受一个函数作为传参数
这个函数就是跳转前要执行的函数

  这个函数也有3个参数 (to, from, next)
  to : 目标路由信息(要去哪里)
  from : 当前的路由信息(来自哪里)
  next :  是一个方法, 执行了才会跳转到目标路由 (也可以用来跳转到指定路由)
*/
router.beforeEach((to,from,next)=>{
  console.log(to) 
  console.log(from)
  // 可以做登录拦截:  如果有登录才给访问后面的页面,如果没有就不给访问
  // 做个案例, 拦截test页面的params就跳转到首页, 否则就是调到目标路由
  if(to.params.id == 'aaaaaaaaa'){
    next('/')
  }else{
    next()
  }
})


// 全局后置守卫
// 跳转完了之后在执行 (不能再做拦截了)
router.afterEach((to,from)=>{
  console.log('------afterEach----------------------------')
  console.log(to)
  console.log(from)
})





export default router
