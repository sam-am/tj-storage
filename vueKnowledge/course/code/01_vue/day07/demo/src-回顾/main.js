import { createApp } from 'vue'
import App from './App.vue'
import router from './router'  // 导入配置好的路由对象
import store from './store'

// use() 就是用来加载插件的
const app = createApp(App)
app.use(store)
app.use(router)  // vue实例使用route插件 
app.mount('#app')
