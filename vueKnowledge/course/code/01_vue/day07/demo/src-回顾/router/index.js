// 导入创建路由, 创建hash模式  的两个方法
import { createRouter, createWebHashHistory } from 'vue-router'
// 导入一个视图组件
import HomeView from '../views/HomeView.vue'

// 配置路由的一个数组
// path : 路径
// component : 路由的视图
// name : 路由的名称
// redirect : 路由重定向
// children : 是配置子路由
const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/about',
    name: 'about',
    // 路由的懒加载 : vue开发中一种优化方式
    // /* webpackChunkName: "about" */  这个最好不删除  ; 给打包的时候加一个标识
    // 每个视图组件的chunName都要不一样的名字
    component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue')
  },
  {
    path: '/test/:id?', // 路由的参数  , 如果这个参数要可选就要用?去修饰
    name: 'test',
    component : ()=>import('../views/Test.vue')
  },
  {
    path: '/test1',
    redirect: '/about'  // 路由重定向
  },
  {
    // 配置子路由需要2步
    // 1. 通过children配置
    // 2. 要在父路由的视图中使用<router-view/> 来显示子路由的视图
    path: '/parent',
    component:()=>import('../views/Parent.vue'),
    children: [
      {
        path: '/son1',   //在子路由的配置中通常是不加/ , /代表根路径
        name: 'son1',
        component: ()=>import('../views/Son1.vue')
      },
      {
        path: 'son2',
        name: 'son2',
        component: ()=>import('../views/Son2.vue')
      }
    ]
  } 
]

// 状态路由对象
const router = createRouter({
  history: createWebHashHistory(),  // 制定路由模式
  routes // 路由路由配置
})

export default router // 导入路由对象
