# 表单绑定

v-model  也只是一个语法糖

也可以通过是 事件和属性的形式 来做到

​      v-model: 

​        1.会根据不同的表单绑定不同的类型  text  select  textarea ...  radio checkbox

​        2.会忽略表单属性的默认值, 那么默认值就需要定义在变量中

​        3.需要注意绑定的事件



## 语法糖

```
<input :value="msg" @input="change" />
```

```
methods: {
    change(e){
      // console.log(e.target.value)
      this.msg = e.target.value
    }
  }
```



## 文本

```
<div>
      <h2>绑定的文本</h2>
      <input v-model="msg" />
      {{msg}}
    </div>

```



## 多行文本

```
<div>
      <!--  多行文本也要通过v-model去绑定  -->
      <h2>多行文本</h2>
      <textarea v-model="context"></textarea>
      <div>{{context}}</div>
    </div>
```



## 复选框

复选框： 绑定的是数组

单个复选框： 一个布尔值

```
<div>
      <!--   
        复选框的值,是保存在一个数组中  
        会忽略checked属性
        如果需要默认选中,就要在数组中定义value值
        选中的值也会把value值添加到数组中
      -->
      <h2>复选框</h2>
      爱好: 
      <input v-model="hs" type="checkbox" name="hobby" value="html5" /> html5
      <input v-model="hs" type="checkbox" name="hobby" value="js" /> js
      <input v-model="hs" type="checkbox" name="hobby" value="css3" /> css3

      <div>{{ hs }}</div>

      <h2>单个复选框</h2>
      <!-- 单个复选框是一个布尔值 -->

      <input type="checkbox" v-model="flag" />
      同意xxxxx 协议

      {{flag}}

    </div>
```



## 单选框

```
<div>
      <h2>单选框</h2>
      <div>
        <input v-model="sex" type="radio" name="sex" value="0" /> 男
        <input v-model="sex" type="radio" name="sex" value="1" /> 女
        {{sex}}
      </div>
    </div>
```



## 事件的绑定

```
<h3>事件绑定</h3>
    <!-- 
      在点击事件中 要拿到单个checkbox的状态 
      点击的函数式先执行的, v-model 还没有
      所以点击的函数中拿到的值,可能还是之前的,
      
      如果要拿到修改后的值, 就要考虑换一个事件 
      change事件: 是input修改后再执行
      所以就可以拿到修改后的值
    -->
    <div>
      <input type="checkbox" v-model="test" @click="gettest" />
      <input type="checkbox" v-model="test" @change="gettest" />
      {{test}}
    </div>
```



# 思考与练习

- 继续完成购车的全选和全不选， 然后计算出选中商品的总价
- 自己写一个注册页面，尽可能的写多种表单标签。





------------------------------



# 下午： 

## 选择器

select-option

```
 <!-- 
      默认的selected属性就失效了
      如果制定默认选中
      给变量加默认值
    -->
    <select v-model="flag">
      <option value="one">一阶段</option>
      <option value="two">二阶段</option>
      <option selected value="three">三阶段</option>
    </select>
    <h3> {{ flag==='one'?'一阶段': flag==='two'?'二阶段': '三阶段' }} </h3>
    <h3>{{ show }}</h3>
```

```
computed: {
     show(){
      if(this.flag==='one'){
        return '一阶段'
      }else if(this.flag==='two'){
        return '二阶段'
      }else{
        return '三阶段'
      }
     }
  }
```



## v-model的修饰符

```
<template>
  <div>
    <hr>
    <h2>修饰符</h2>
    <div>
      <!-- lazy 不是实时变, 而是失去焦点再更新数据 -->
      <input type="text" v-model.lazy="msg">{{msg}}
    </div>

    <div>
      <!-- number 在输入的值可以转成数字的情况下,就转成数字类型, 否则就不转 -->
      <input type="text" v-model.number="num">{{num}}
    </div>

    <div>
      <!-- trim 去掉空格后,再赋值 -->
      <input type="text" v-model.trim="msg">{{msg}}
    </div>
  </div>
</template>

<script>

export default {
  name: 'App',
  data(){
    return {
      flag: 'three',
      msg: '',
      num: 0
    }
  },
}
</script>
```



## 生命周期

https://cn.vuejs.org/guide/essentials/lifecycle.html

```
<!--
  怀胎-出生-成长-入土
  在不同的阶段可以做不同事情 (有些事件只能在某个阶段做才合理(合法))
  其实就是描述的是vue组件的生命周期
  
  描述的组件实例、虚拟dom、真实dom的关系

  分4个阶段:  (8个钩子,也是组件实例的选项)
  创建前/后 - 挂载前后   -  更新前后  - 销毁前/后 
  
  beforeCreate
  created
  
  beforeMount
  mounted
  -- 以上4钩子,在组件创建的时候会依次执行
    场景: 
      某个页面一打开的时候,就要去请求数据然后显示出来
      在vue开发中,只要把请求到的数据,赋值给data, 那么页面显示出来了
        这时候就要选择一个生命周期的钩子去做请求操作
        created  beforeMount  mounted 这三个都可以
        但是建议在mounted上做:  此时整个实例,虚拟dom都是完整的了

        所以mounted这个生命周期钩子,使用频率肯能会比较频繁

  当组件有更新的时候,就会触发更新阶段的钩子
  beforeUpdate
  updated


  ------其他的生命周期钩子,就要到组件之后再讲

  vue的其中一个特点就是虚拟dom, 他的作用就是减少真实dom的操作(比较耗性能)

-->

<template>
  <div>
    <h2>生命周期 钩子(函数) </h2>
    <div>{{count}}</div>
    <button @click="count++">count++</button>
    <button @click="getdom">getdom</button>
  </div>
</template>

<script>

export default {
  name: 'App',
  data(){
    return {
      count: 99
    }
  },
  beforeCreate(){
    console.log(this.count)  // undefined  // 组件实例还没有状态好,还不能使用data
    console.log(this.$el)    // null  // 此时虚拟dom也没有创建好
    console.log(document.getElementById('app').innerHTML) // 真实dom // 也还没有渲染
  },
  created(){
    console.log('----created----------------')
    console.log(this.count)  // 组件实例已经装好了,可以使用data
    console.log(this.$el) // null  此时虚拟dom也没有创建好
    console.log(document.getElementById('app').innerHTML) // 也还没有渲染
  },
  beforeMount(){
    console.log('----beforeMount----------------')
    console.log(this.count) // 组件实例已经装好了,可以使用data
    console.log(this.$el) // null  此时虚拟dom也没有创建好
    console.log(document.getElementById('app').innerHTML) //// 也还没有渲染
  },
  mounted(){
    console.log('----mounted----------------')
    console.log(this.count)  // 组件实例已经装好了,可以使用data
    console.log(this.$el)  // 可以拿到生成的虚拟dom
    console.log(document.getElementById('app').innerHTML) // 也已经挂载真实的dom中
  },
  beforeUpdate(){
    console.log('----beforeUpdate----------------')
    console.log(this.count)  // 拿到是更新后的数据
    console.log(this.$el)  // 更新后的虚拟dom
    console.log(document.getElementById('app').innerHTML) // 但是还没有更新好真实dom
  },
  updated(){
    console.log('----updated----------------')
    console.log(this.count)  // 拿到是更新后的数据
    console.log(this.$el)  // 更新后的虚拟dom
    console.log(document.getElementById('app').innerHTML) // 虚拟dom也更新到真实dom了
  },
 
  methods: {
    getdom(){
      var html =  document.getElementById('app').innerHTML
      console.log(html)
    }
  }
}
</script>
```





## 思考与练习

- 理解v-model与各种表单的绑定
- 需要理解不同事件和v-model的关系（先后顺序）

- 需要理解生命周期 （着重的理解案例中的场景）
