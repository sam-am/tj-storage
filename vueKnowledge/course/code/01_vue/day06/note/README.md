# 思考与练习

- keep-alive的作用
- 路由的概念- 前端路由后端路由
- 理解前端路由的概念
- 两种前端路由模式的区别
- 跑两个路由原理的项目
- 试着描述说两种路由模式的原理



下午开始在vue项目中怎么配置使用路由





# main.js

整个项目的入口文件

```

import { createApp } from 'vue'   // 导入创建vue实例的方法
import App from './App.vue'       // 导入根视图
import router from './router'     // 导入路由的配置文件 
//(导入的是router文件夹,默认会找index.js文件) 
import store from './store'       // 导入仓库的配置文件

// createApp(App).use(store).use(router).mount('#app')   

// vue3: 
// 创建vue实例
// 使用store插件
// 使用router插件
// 挂载实例 到 #app

const app = createApp(App);
app.use(store);
app.use(router);
app.mount('#app')
```





# 路由

上午已经讲过了路由的概念， vue-router的库已经给我们实现了前端路由，我们只要使用就可以了，

我们在创建时候已经选择vue-router这个库，所以我们直接使用就可以了

## 第一步

**router/index.js  文件解析**

```
// 导入创建路由的方法, 路由模式的方法
import { createRouter, createWebHashHistory , create } from 'vue-router'
// 导入了一个组件
import HomeView from '../views/HomeView.vue'

// 路由的配置文件
// 所有需要显示的页面,都需要在这个数组中配置
// 每一个元素都是一个对象
// 选项: 
    // path:  匹配的路径    /代表根目录
    // component:  当前匹配到的路径要显示的视图
      // path component 是一个路由配置必须要有的两个属性
    // name是路由的别名(给这个路由取一个名字: 这名字不要和其他的路由名字重名了)
      // 建议大家写上,通过name做路由跳转会比较方便
const routes = [
  {
    path: '/',
    name: 'home',  
    component: HomeView
  },
  {
    path: '/about', 
    name: 'about',
    // 这个路由视图是使用了路由懒加载的方式加载的视图组件
    // 这种加载方式是vue开发的优化项目的一种方式
    // /* webpackChunkName: "about" */   
      // 这个注释最好不要删除,并不是一个简单的注释,最后打包项目
      // 最后打包项目的时候,会给这个文件加这个chunkname, 为了方便的区分文件.
    component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue')
  }
]
const router = createRouter({  // 创建路由实例
  history: createWebHashHistory(),  // 是使用哪种模式:  hash  history
  routes  // 路由的配置
})
export default router

```



## 第二步

有了路由的文件配置后，还需要有**第二步**： 就是要把视图加载出来

<router-view>     // 这个组件就是用来显示子路由的。 所有的路由视图都是通过这个组件显示出来的。



## 第三步

路由的跳转

###  声明式的导航

本质的就是a连接， 但是我们不要直接使用a连接，要使用它提供组件来去跳转<router-link>

```
<!-- 
        声明式的跳转 
        1.字符串, (需要跳转的路径)
        2.对象  (里面是一个对象,可以通过path或者name去跳转)
            // 对象的形式功能比较多,比如携带参数可以属性的方式
            // 如果是字符串形式的话,就只能通过拼串的方式了
    -->
    <router-link to="/about">跳转转到about</router-link>
    <br>
    <router-link :to="{path:'/about'}">通过对象的形式跳转到about1</router-link>
    <br>
    <router-link :to="{name:'about'}">通过对象的形式跳转到about2</router-link>
    <br>
    <router-link :to="{name:'about',query: {name:'young',age:18}}">通过对象的形式跳转到about3</router-link>
```

### 编程式的导航

场景： 登录成功后要自动跳转到首页,  声明式的导航是否可以做到？ 

就需要用到编程式导航，或者其他的场景也是会用的到的

编程式导航是用的比较多的。一般项目中都是使用编程式导航来去做跳转的



因为 在mian.js中已经使用router插件，那么就会全局的对象中加一对象 ，$router

通过this.$router就可以完成编程式导航

```
<button @click="jump1">跳转到about-push</button>
<button @click="jump2">跳转到about-replace</button>
```

对应的操作的方式

对配置路由的跳转

```
methods: {
    jump1(){
      // console.log(this.$router)
      /*
        push()  // 有记录的跳转
        参数也有两种形式
        1. 字符串 路由的path
        2. 对象   可以通过name|path去跳转, 也可以属性来去携带参数
      */
      // this.$router.push('/about')
      
      // this.$router.push({name:'about'})
      // this.$router.push({path:'about'})

      // this.$router.push({
      //   name:'about',
      //   query:{name:'young',age:18}
      // })

      this.$router.push({
        path: '/about',
        query: {name:'young',age:18}
      })
      // 此时通过path和name没有什么区别, 都可以携带query参数
      // 区别在于params的参数
    },
    jump2(){
      // replace()  目标路由替换当前路径(没有记录的跳转)
      // 参数的形式也和push一样
      this.$router.replace('/about')
    }
  }
```

对浏览历史的的跳转

```
<button @click="jump3">返回</button>
<button @click="jump4">go方法</button>
<button @click="jump5">前进</button>
```

```
methods:{
	jump3(){
		this.$router.back();  // 返回
	},
  jump4(){
    // this.$router.go(-2)  // 负数表示返回多少页
    this.$router.go(1)    // 正数表示前进多少页
  },
  jump5(){
    this.$router.forward()  //前进
  }
}
```



# 路由的配置

必要的属性就是两个path ,component

## 路由的参数

在path的值使用： 来配置参数

？代表这个参数可选

```
{
    // 这个:id就要路由的参数  /info/后面的内容都会当做id参数
    // 如果这个参数是可选的,就要用?来修饰一下
    path: '/info/:id?',  
    name: 'info',
    component: () => import('../views/Info.vue') 
  },
  {
    path: '/demo/:uid',
    name: 'demo',
    component: ()=>import('../views/Demo.vue')
  },
```

获取参数： 要通过this.$route

```
 mounted(){
      // 接受当前路由的参数
      // 需要通过this.$route(不带r)来获取
      // $router 路由对象-可以操作路由
      // $route 路由信息对象(只是获取信息)

      console.log(this.$route)
      /*
        params :  路由参数
        query  :  问号?后面的参数
        path   :  路径
        href   :  解析后的完整路径
        name   :  当前路由的name属性
      */ 
    },
```

**注意**： parmas不能通过path来传

```
 methods: {
      jump(){
        // 通过path方式跳转就不能带params
        this.$router.push({
          path: '/demo',
          query : {name:'young', age: 18},
          params: {uid: 'aaa123'} //路由参数
        })
        // 通过name跳 ,两种参数都可以带
        // this.$router.push({
        //   name: 'demo',
        //   query : {name:'young', age: 18},
        //   params: {uid: 'aaa123'} //路由参数
        // })
      }
    }
```



## 子路由

配置子路由需要2步

1. 通过children子路由
2. 需要在父路由视同中使用<router-view/> 来显示子路由的视图

```
{
    path: '/parent',
    name: 'parent',
    component: ()=>import('../views/Parent.vue'),
    // 子路由要通过children属性去配置
    // 是一个数组, 里面的每一项也都是路由对象
    children: [
      {
        path: '/son1',  // 子路由的路径通常是不带 / 的, 因为/代表根路径
        name: 'son1',
        component: ()=>import('../views/Son1.vue')  
        // 子路由的视图要显示出来, 要通过父路由的视图的<router-view>
      },
      {
        path: 'son2',
        name: 'son2',
        component: ()=>import('../views/Son2.vue')  
      }
    ]
  },
```

Pararen.vue

```
<template>
  <div>
    <h2>这是parent视图</h2>
    <router-view />
  </div>
</template>
```







# 思考与练习

- 熟悉路由的配置
- 熟悉vue-router提供的api
- 熟悉两种导航方式的区别
- $router和$route的区别
- 怎么配置子路由



- 把之前写过的页面转到vue中 （最好是统一个项目的页面）

（组长现在要安装准备项目的事了，1，先选择好项目 ）

可以优先准备准备toC的项目  ， 

to client  给客户直接使用的

toB  to business 给商家使用的（通常就是后台管理系统）

```
https://www.axureshop.com/
https://www.pmdaniu.com/explore
https://js.design/square?page=2
https://mastergo.com/community/
https://www.mockplus.cn/example/rp/
https://pixso.cn/community/home?classify=file&tag=8

https://www.zcool.com.cn/
```

