// 导入创建路由的方法, 路由模式的方法
import { createRouter, createWebHashHistory , create } from 'vue-router'
// 导入了一个组件
import HomeView from '../views/HomeView.vue'

// 路由的配置文件
// 所有需要显示的页面,都需要在这个数组中配置
// 每一个元素都是一个对象
// 选项: 
    // path:  匹配的路径    /代表根目录
    // component:  当前匹配到的路径要显示的视图
      // path component 是一个路由配置必须要有的两个属性
    // name是路由的别名(给这个路由取一个名字: 这名字不要和其他的路由名字重名了)
      // 建议大家写上,通过name做路由跳转会比较方便
const routes = [
  {
    path: '/',
    name: 'home',  
    component: HomeView
  },
  {
    path: '/about', 
    name: 'about',
    // 这个路由视图是使用了路由懒加载的方式加载的视图组件
    // 这种加载方式是vue开发的优化项目的一种方式
    // /* webpackChunkName: "about" */   
      // 这个注释最好不要删除,并不是一个简单的注释,最后打包项目
      // 最后打包项目的时候,会给这个文件加这个chunkname, 为了方便的区分文件.
    component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue')
  },
  {
    // 这个:id就要路由的参数  /info/后面的内容都会当做id参数
    // 如果这个参数是可选的,就要用?来修饰一下
    path: '/info/:id?',  
    name: 'info',
    component: () => import('../views/Info.vue') 
  },
  {
    path: '/demo/:uid',
    name: 'demo',
    component: ()=>import('../views/Demo.vue')
  },
  {
    path: '/parent',
    name: 'parent',
    component: ()=>import('../views/Parent.vue'),
    // 子路由要通过children属性去配置
    // 是一个数组, 里面的每一项也都是路由对象
    children: [
      {
        path: '/son1',  // 子路由的路径通常是不带 / 的, 因为/代表根路径
        name: 'son1',
        component: ()=>import('../views/Son1.vue')  
        // 子路由的视图要显示出来, 要通过父路由的视图的<router-view>
      },
      {
        path: 'son2',
        name: 'son2',
        component: ()=>import('../views/Son2.vue')  
      }
    ]
  },
  {
    // 路由重定向
    // 访问test的时候跳转到about
    path: '/test',
    redirect: '/about'
  }
]
const router = createRouter({  // 创建路由实例
  history: createWebHashHistory(),  // 是使用哪种模式:  hash  history
  routes  // 路由的配置
})
export default router
