
import { createApp } from 'vue'   // 导入创建vue实例的方法
import App from './App.vue'       // 导入根视图
import router from './router'     // 导入路由的配置文件  (导入的是router文件夹,默认会找index.js文件)
import store from './store'       // 导入仓库的配置文件

// createApp(App).use(store).use(router).mount('#app')   

// vue3: 
// 创建vue实例
// 使用store插件
// 使用router插件
// 挂载实例 到 #app

const app = createApp(App);
app.use(store);
app.use(router);
app.mount('#app')
