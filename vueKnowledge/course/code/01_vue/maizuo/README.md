# maizuo

maizuo项目

因为时间关系，没有太多的去写样式了，所以就是用的ui来构建页面。

常用的ui库

​	elementUI    后台

​	antd   ： 比较多，用react的比较多，

​	iview   ： 也是后台，它有收费版本（有技术支持）

​	vant： 移动端，也有其他平台对应的ui库，小程序...



ui库的选择： 通常不同ui库的使用方式不同，面对的使用也不同，可能面对的方向也不同

我们的卖座网是属于移动的项目，所以选择vantui做ui库



但是： 我们现在有一点要特别的注意，我们的项目是vue3的，但是我们此时此刻还没有学vue3的新特性，还是vue2的写法。 就因为这点,  安装vant的时候还是要安装支持vue3的版本，但是写法还是要写vue2的写法（也就是要参考vant2的文档）

其他项目也有一点的要注意： **如果是vue3的项目就一定安装支持vue3的ui库版本。**



https://vant-contrib.gitee.io/vant/#/zh-CN



## 使用

1.安装

```
npm i vant
```

2.引入组件（配置）

不要忘记引入样式

在这里我们使用按需引入的方式，比较全面（后续开发的时候可以方便一点）

因为我们使用脚手架创建的项目，要使用对应的配置的配置方式

- 安装插件

```
npm i unplugin-vue-components
```

- 配置插件

  vue.config.js

  ```
  const { VantResolver } = require('unplugin-vue-components/resolvers');
  const ComponentsPlugin = require('unplugin-vue-components/webpack');
  
  const { defineConfig } = require('@vue/cli-service')
  module.exports = defineConfig({
    transpileDependencies: true,
    configureWebpack: {
      plugins: [
        ComponentsPlugin({
          resolvers: [VantResolver()],
        }),
      ],
    },
  })
  
  ```



3. 引入样式文件

为了方便，就引入全局样式

main.js

```
import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

// 引入vantui的组件样式
import 'vant/lib/index.css';

createApp(App).use(store).use(router).mount('#app')

```



4.使用组件

homoView.vue

```
<template>
  <div class="home">
    <img alt="Vue logo" src="../assets/logo.png">
    <van-button type="primary">主要按钮</van-button>
    <van-button type="success">成功按钮</van-button>
    <van-button type="default">默认按钮</van-button>
    <van-button type="warning">警告按钮</van-button>
    <van-button type="danger">危险按钮</van-button>
  </div>
</template>

<script>

export default {
  name: 'HomeView'
}
</script>

```





## 开始项目

- 路由搭建 （也就底部导航配置的开始）







## swiper

1.安装swiper

安装要注意版本，不同版本的版本，使用的方式不一样

我们现在使用的是 4.3的版本，所以安装4.3的版本

```
npm i swiper@4.3
```



2.导入样式和对象

注意： 样式可以全局导入： （main.js中导入一次就可以）

但是 对象，在那个组件中用，就在哪再导一次

```
  // 2.1 导入swiper的样式
  import 'swiper/dist/css/swiper.css'

  // 2.2 导入swiper对象
  import Swiper from 'swiper'
```



3.修改样式结构

```
 <div class="imgbox">
      <div class="tbox">演职人员</div>
      <div class="actors swiper-container">
        <ul class="swiper-wrapper">
          <li class="swiper-slide" v-for="item in info.actors">
            <div class="avatar">
              <img :src="item.avatarAddress" />
            </div>
            <div>{{item.name}}</div>
            <div>{{item.role}}</div>
          </li>
        </ul>
      </div>
    </div>
```



4. 创建实例配置

```

        // 4. 创建swiper实例
        // 此时创建实例,页面未必就渲染好了,
        // 如果页面没有渲染好,swiper实例的计算就会有误差.
        // 所以,我们创建实例要等页面渲染好了在创建
       
        //  vue实例带的方法   ($开头的基本都是vue自带的)
        // this.$nextTick()  // 这个函数就是页面渲染完再执行
        this.$nextTick(()=>{
          var swiper1 = new Swiper('.actors', {
            slidesPerView: 3,
            spaceBetween: 30,
            freeMode: true
          });

          var swiper2 = new Swiper('.imgs', {
            slidesPerView: 2,
            spaceBetween: 1,
            freeMode: true
          });
        })
```

