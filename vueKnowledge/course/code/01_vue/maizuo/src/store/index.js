import { createStore } from 'vuex'

import VuexPersistence from 'vuex-persist'

const vuexLocal = new VuexPersistence({
  key: 'maizuo',
  storage: window.localStorage
})

export default createStore({
  state: {
    city: {
      cityId: 110100,
      isHot : 1, 
      name : "北京", 
      pinyin :  "beijing"
    }
  },
  getters: {
  },
  mutations: {
    changeCity(state,info){
      state.city = info
    }
  },
  actions: {
  },
  modules: {
  },
  plugins:[vuexLocal.plugin]
})
