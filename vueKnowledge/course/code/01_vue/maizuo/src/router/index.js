
import { createRouter, createWebHashHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView,
    redirect: '/movie',
    children: [
      {
        path: 'movie',
        name: 'movie',
        component: ()=> import('../views/home/movie.vue'),
        children: [
          {
            path: 'hot',
            name: 'hot',
            component: ()=> import('../views/home/hot.vue')
          },
          {
            path: 'soon',
            name: 'soon',
            component: ()=> import('../views/home/soon.vue')
          }
        ]
      },
      {
        path: 'cinema',
        name: 'cinema',
        component: ()=> import('../views/home/cinema.vue')
      },
      {
        path: 'news',
        name: 'news',
        component: ()=> import('../views/home/news.vue')
      },
      {
        path: 'my',
        name: 'my',
        component: ()=> import('../views/home/my.vue')
      }
    ]
  },
  {
    path: '/info/:mid', //电影id movieID,
    name: 'info',
    component: ()=>import('../views/info.vue')
  },
  {
    path: '/cinemainfo/:cid/:fid?',  //影院ID  cinemaID  ;  fid是电影ID: filmId
    name: 'cinfo',
    component: ()=>import('../views/cinfo.vue'),
    children: [
      {
        path: ':time',  // 需要有一个time的参数才能正常显示    //   /cinemainfo/:cid/:fid/:time
        name: 'schedule',
        component: ()=>import('../views/schedule.vue') 
      },
      {
        path: 'test',   //   /cinemainfo/:cid/:fid?/test'
        name: 'test',
        component: ()=>import('../views/schedule.vue') 
      }
    ]
  },
  {
    path: '/city',
    name: 'city',
    component: ()=>import('../views/city.vue')
  },
  {
    path: '/about',
    name: 'about',
    component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue')
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
