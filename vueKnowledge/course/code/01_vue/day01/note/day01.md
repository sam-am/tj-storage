# 创建项目-方式1

```
npm init vue@latest
```

是使用npm创建项目指定模板的方式。

```
Project name:   // 项目的名称
Add TypeScript?  // 是否TS
Add JSX Support?  // 是否要支持jsx
Add Vue Router for Single Page Application development?   // 是否要添加路由
Add Pinia for state management?  // 是否要添加Pinia状态管理工具
Add Vitest for Unit Testing?  // 是否要使用Vitest测试工具
Add Cypress for both Unit and End-to-End testing?   // 是否使用E2E测试工具
Add ESLint for code quality?   // 是否启动代码格式检查工具
```

这种创建方式是使用vite工具给我们创建的项目

## 目录结构

![image-20220829141652085](assets/image-20220829141652085.png)





node_modules  项目的依赖包

public   项目的静态资源文件  //这里面的内容通常不会被打包

.gitignore  git的忽略配置文件

index.html   项目的页面模板

package.json  项目描述文件。基本信息， 命令相关，依赖相关（生产，开发） ... 可能还有配置相关的

vite.config.js   vite的配置文件

src   项目的源代码， 我们开发的东西都在这个文件里面完成

​	   assets  项目资源  //是被打包

​		components   组件

​		router   路由配置的文件夹

​		stores   状态管理的文件夹

​		**views**   视图的文件夹

​		**App.vue**   整个项目的视图入口文件

​		main.js  项目的入口文件（项目已启动先执行的main.js）



## 方式二

通过脚手架的方式创建项目

主要版本的问题， 

@vue/cli     vue2后的版本

vue-cli   vue2前的版本



- 安装脚手架

  ```
  npm i -g @vue/cli
  ```

  安装完成后就可以使用vue这个命令

- 使用脚手架工具来创建项目

```
vue create 项目名称
vue create demo3
```

```
 Please pick a preset:    选择配置
 	Manually select features   // 后面再选择需要的项目配置
 Check the features needed for your project:   // 选择项目所需要的配置
 	 Babel
 	 Router
 	 Vuex
 	 
 Choose a version of Vue.js that you want to start the project with // 选择vue的版本
 Use history mode for router?    // 路由模式是否使用history，  还有一种叫hash
 Where do you prefer placing config for Babel, ESLint, etc.?   // 配置文件是单独的文件，还是package.json中
 
 Save this as a preset for future projects?   // 当前的项目配置是否可以给后面创建项目的时候使用
 
 Pick the package manager to use when installing dependencies:    // 选择那种包管理工具
```

这种创建的项目是 使用webpack创建的

```
babel.config.js   // babel.config.js
vue.config.js   // webpack的配置文件
```



建议刚开始学习的时候，使用方式二去创建项目





# 单文件组件

![image-20220829152944986](assets/image-20220829152944986.png)

一个.vue就是一个单文件组件

template   ，是用来写页面结构的，以前怎么写html,在这里面就怎么写 ，如果是vue2项目，只能有一个根节点

script  ，  js逻辑代码， 要默认导入这个对象， 这个对象就是组件的选项

style ， 写样式文件   如果有scope属性，只会在当前组件中生效，如果没有样式就是全局的







## 作业

vue devtools   vue的开发调试工具



- 常见的几种开发模式

- 创建项目
- 什么是单文件组件
- 模板语法是怎样的。