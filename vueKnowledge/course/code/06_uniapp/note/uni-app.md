# uni-app

是一个混合开发的框架，主要用来开发 各种小程序 以及  app(android,ios)  还有web.

hbuilder这个公司出的， 所以要开发uni-app 就要HBuilderX。



里面的写法 是和 vue+mini 的写法是一样的。（是他们两个的结合）

要生成对应平台的代码，就取决于他的编译器。

我们已经有vue和小程序的开发经验，所以上手uni-app是容易得事情。



https://uniapp.dcloud.net.cn/

https://www.dcloud.io/hbuilderx.html





# 开始

## 创建项目

直接使用hbx 来创建项目就可以了

- 普通项目

- uni-app

- Wep2app  可以把网页打包成app
- 5+ app , 也是网页打包成app,  区别在js的功能多了些



在我们选择uni-app,  右边会有很多模板



开发环境中预览

点击： 运行-选择要预览的平台就可以了

如果运行到微信开发者工具 ，要把微信开发者工具的 设置->安全设置->服务端口打开，才可以正常预览



发行：

可以使用dccloud提供的证书，然后选择云打包，  （要等，但是简单）



## 项目目录

pages    页面文件夹

static     静态资源文件

App.vue  项目的根视图

index.html  首页的模板文件

main.js 项目的入口文件

**manifest.json**   对应各种平台的对应的配置文件

		微信小程序的AppID
	
		app: 图标、名称
	
		....

pages.json    当前项目的全局配置   ：  窗口，背景色.... 还有路由...  

uni.scss   全局的样式文件

**unpackage**     打包后的资源文件，要发布的每一个平台都有对应的代码

uni_modules    存放插件的包，在这个文件里面的插件，就可以在组件中直接使用，不需要导入和注册。

				可以直接去插件市场中导入



## 页面

通常页面的目录都是放在pages，而且用开发工具去创建页面就可以，加的页面路径也会放在pages.json中字段



页面就是一个.vue文件 ；里面的结构也是和单文件组件（SFC）一样的 ,  template  script  style

正常的页面结构写法就是用uniapp里面组件来去写结构。

js部分也是vue组件一样的，但是加了一些钩子， onShow onLoad  ....

	也包括的vue组件的生命周期钩子



页面 声明式跳转 用navigator

```
<navigator url="/pages/index/index">index</navigator>
```



**注意** ,   uni-app开发的应用，其实就是app里面嵌套了webview，所以开发的页面就不是原生，就是还是一个普通html5页面。    .vue文件编译后就是一个html页面。

如果使用是 .nvue  文件开发的页面，那么就会编译成原生(android,ios)页面   （android下用java开的页面,ios： **Swift语言**，Objective-C）



.nvue 的常见坑

1.文本必须在text组件内，才可以设置样式

2.有些css是不能写的，最好的方式就是直接使用flex来做布局

3.只能用class选择器



## css

单位： 推荐使用rpx (是响应式的单位，前提设计稿的宽度是750px)

		以前的单位upx，也是响应式的，  现在都是推荐使用rpx



样式导入   @import



选择器（干脆就直接只用class, 其他的不用，只是不用，不是不知道）

| 选择器           | 样例           | 样例描述                                       |
| :--------------- | :------------- | :--------------------------------------------- |
| .class           | .intro         | 选择所有拥有 class="intro" 的组件              |
| #id              | #firstname     | 选择拥有 id="firstname" 的组件                 |
| element          | view           | 选择所有 view 组件                             |
| element, element | view, checkbox | 选择所有文档的 view 组件和所有的 checkbox 组件 |
| ::after          | view::after    | 在 view 组件后边插入内容，**仅 vue 页面生效**  |
| ::before         | view::before   | 在 view 组件前边插入内容，**仅 vue 页面生效**  |



## 组件

需要用第三方组件，就要去插件市场中导入

uni_modules 中的组件就可以直接使用，不需要导入和注册



使用第三方的ui组件：  uview

- 先去插件市场导入插件

- 要初始化

  要导入组件

  要导入样式



## api

之前小程序api基本一样，把wx改成uni调用即可

```
uni.showLoading({
	title:'拼命加载中...'
})

setTimeout(()=>{
	uni.hideLoading()
},3000)
```