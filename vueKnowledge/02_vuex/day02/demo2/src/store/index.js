import { createStore } from 'vuex'

const myPlugin = (store) => {

  const local = window.localStorage.getItem('age')
  console.log(local);
  if(local) {
    const data = JSON.parse(local)
    store.commit('setCount',data.count)
  } else {
    window.localStorage.setItem('age',JSON.stringify(store.state))
  }
  // 数据修改了触发subscribe函数
  store.subscribe((mutation, state) => {
    window.localStorage.setItem('age',JSON.stringify(state))
  })
}

export default createStore({
  state: {
    count: 0,
    list: [1,2,3,4,5,6,7,8]
  },
  getters: {  // 类似于一个全局的计算属性
    getList(state) {
      return state.list.filter(item=>{
        return item%2 == 1
      })
    }
  },
  mutations: {
    changeCount(state) {
      state.count++
    },
    setCount(state,info) {
      state.count = info
    }
  },
  actions: {
    asyncChangeCount(context) {
      setTimeout(() => {
        console.log('异步函数触发了');
        console.log(context);
        console.log(context.state);
        console.log(context.state.count);
        console.log(context.state.list);
        context.commit('changeCount')
      }, 1000);
    }
  },
  modules: {
  },
  plugins: [myPlugin]
})

/*
  笔记：
    1. Action 提交的是 mutation，而不是直接变更状态
    2. Action 可以包含任意异步操作
    3. Action 函数接受一个与 store 实例具有相同方法和属性的 context 对象

  需求：
    1. 在plugins中给state中的count做状态缓存在localstorage中，刷新页面count显示的数据不重置
    思路：
      1. 判断localstorage中没有存的值
*/
