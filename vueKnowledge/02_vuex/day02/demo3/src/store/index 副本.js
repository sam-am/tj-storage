import { createStore } from 'vuex'

// vuex持久化插件: vuex-persist
import VuexPersistence from 'vuex-persist'

const vuexLocal = new VuexPersistence({
  storage: window.localStorage
})


export default createStore({
  state: {
    count: 0
  },
  getters: {
  },
  mutations: {
    changeCount(state) {
      state.count++
    }
  },
  actions: {
  },
  modules: {
  },
  plugins: [vuexLocal.plugin]
})

/*
  1. vuex-persist 插件下载命令：npm i vuex-persist
*/
