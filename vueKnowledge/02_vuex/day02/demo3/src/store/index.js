import { createStore } from 'vuex'

const myplugin = (store)=> {
  const local = localStorage.getItem('vuex')
  console.log(local);
  if(local) {
    const data = JSON.parse(local)
    // 转成对象的数据类型，将value值赋值给state
    // store.state = data
    store.state.count = data.count  // store.state不能直接对它赋值
  } else {
    const jsonstring = JSON.stringify(store.state)
    localStorage.setItem('vuex',jsonstring)
  }


  // 每次state被mutation之后，就会调用下面这行代码
  // 每次初始化的时候会调用下面这行代码
  store.subscribe((mutation,state)=>{
    const jsonstring = JSON.stringify(state)
    localStorage.setItem('vuex',jsonstring)
  })
}

export default createStore({
  state: {
    count: 0
  },
  getters: {
  },
  mutations: {
    changeCount(state) {
      state.count++
    }
  },
  actions: {
  },
  modules: {
  },
  plugins: [myplugin]
})

/*
  思路：
    1. 验证localStorage里面有没key值
    2. 如果有，那么把里面的值拿出来赋值给state；如果没有，那么把state里面的值转成JSON字符串缓存进去
    3. 在state修改时，动态的存取state数据在localStorage中

*/
