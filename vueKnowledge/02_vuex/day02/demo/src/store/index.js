import { createStore } from 'vuex'

import user from './user'

const store = createStore({
  state: {  // 存放数据
    count: 0,
    list: [2,4,6,8,9,14,13]
  },
  getters: {  // 统一获取数据的方法，相当于一个全局的计算属性
    oulist(state) {
      return state.list.filter((item)=>{
        return item%2 == 0
      })
    }
  },
  mutations: {  // 修改数据的方法，必须是纯函数
    ChangeCount(state) {
      state.count++
    },
    setCount(state,info) {
      state.count = info
    },
  },
  actions: {  // 异步修改数据，只是可以写异步，修改数据还是在mutations
    asyncChangeCount(context) {  // 这里的context参数只是一个行参，== $store
      setTimeout(()=>{
        context.commit('ChangeCount')
      },1000)
    }
  },
  modules: {  // 仓库模块化
    // 在这个modules中加载一个小仓库
    user: user
  }
})

export default store
