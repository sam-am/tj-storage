const user = {
  // 添加命名空间
  namespaced: true,
  state() {
    return {
      name: 'young',
      age: '18'
    }
  },
  mutations: {
    changeName(state) {
      state.name = 'zhangsan'
    },
    changeAge(state) {
      state.age++
    }
  }
}

export default user