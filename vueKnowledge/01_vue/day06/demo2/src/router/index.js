import {createRouter, createWebHashHistory} from 'vue-router'
import HomeView from '../views/HomeView'

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/about',
    name: 'about',
    component: () => import(/* webpackChunkName: "about" */ '../views/AboutView')
  },
  {
    // 加上问号表示这个路径参数是可选的，就算不要这个参数当前路由也能匹配到视图组件
    path: '/info/:id?',
    name: 'info',
    component: ()=> import('../views/info')
  },
  {
    path: '/demo/:userId?',
    name: 'demo',
    component: ()=> import('../views/Demo')
  },
  {
    path: '/parent',
    name: 'parent',
    component: ()=> import('../views/Parent'),
    children: [
      {
        path: 'son1',
        name: 'son1',
        component: ()=> import('../views/Son1')
      },
      {
        path: 'son2',
        name: 'son2',
        component: ()=> import('../views/Son2')
      }
    ]
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router

/*
  1. 导入创建路由和创建路由模式的两个方法
  2. 导入根目录路由渲染的视图组件，之后的视图组件渲染都使用路由懒加载的方式导入
  3. 创建路由信息
  4. 创建路由实例
  5. 抛出路由实例
*/
