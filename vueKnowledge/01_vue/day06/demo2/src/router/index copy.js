// 导入创建路由的方法，路由模式的方法
import { createRouter, createWebHashHistory } from 'vue-router'
// 导入了一个组件
import HomeView from '../views/HomeView.vue'

/* 
  routes 是路由的配置文件
  所有需要显示的页面，都需要在这个数组中配置，routes数组中每一个元素都是一个对象
  选项：
    path：匹配的路径，是一个路由配置的必须属性
    component：当前匹配到的路径要显示的视图组件，通常放在views文件夹下，是一个路由配置的必须属性
    name: 是路由的别名（唯一性，不要重名，建议写上，通过name做路由跳转更方便） 
*/

const routes = [
  {
    // 当前路径为根目录
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/about',
    name: 'about',
    component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue')
    /*
      这个路由视图是使用了路由懒加载的方式加载的视图组件，这种加载方式是vue开发的优化项目的一种方式
      区别：当代码读到这里的时候才会导入，而不是一次性全部导入进来，提升性能
      注释不要删除，并不是一个简单的注释
      作用：最后打包项目的时候，会给这个文件加上chunkname，为了方便区分文件 
    
    */
  }
]

// 创建路由实例
const router = createRouter({
  // 使用哪种路由模式
  history: createWebHashHistory(),
  // 路由的配置
  routes
})

export default router
