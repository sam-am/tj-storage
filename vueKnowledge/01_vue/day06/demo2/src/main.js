// 导入创建vue实例的方法
import { createApp } from 'vue'
// 导入根节点视图
import App from './App.vue'
// 导入路由配置文件，（导入的是router文件夹，默认会找里面的index.js文件）
import router from './router/index'
// 导入仓库的配置文件
import store from './store'

// 书写方式一：创建vue实例，使用store插件，使用router插件，挂载实例到#app节点
// createApp(App).use(store).use(router).mount('#app')

/*
  书写方式二：

  1. 创建Vue实例
  2. 使用store插件
  3. 使用router插件
  4. 挂载到#app
*/

const app = createApp(App)
app.use(store)
app.use(router)
app.mount('#app')
