import { createRouter, createWebHashHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/about',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue'),
    // 路由独享的守卫
    beforeEnter: (to,from,next) => {
      console.log(to);
      console.log(from);
      next()
    }
  },
  {
    path: '/test/:id',
    name: 'test',
    component: () => import('../views/TestView.vue'),
    beforeEnter: (to,from,next) => {
      console.log('路由独享的守卫');
      next()
    }
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

router.beforeEach((to,form,next)=>{
  console.log('全局前置守卫触发了');
  next()
})

router.afterEach(()=>{
  console.log('全局后置守卫触发了');
})

export default router
