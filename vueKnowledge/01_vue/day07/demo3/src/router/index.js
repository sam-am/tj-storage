import { createRouter, createWebHashHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/about',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue')
  },
  {
    path: '/test/:id',
    name: 'test',
    component: () => import('../views/TestView.vue')
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

// 拦截路由params.id为12345的跳转到首页，否则跳转到目标路由
router.beforeEach((to,from,next)=>{
  // console.log(to.params.id);
  if(to.params.id == '12345') {
    next('/')
  } else {
    next()
  }
})

export default router
