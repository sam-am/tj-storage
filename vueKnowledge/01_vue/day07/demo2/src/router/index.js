import { createRouter, createWebHashHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/about',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue')
  },
  {
    path: '/parent/:id',
    name: 'parent',
    component: ()=> import('../views/ParentView.vue'),
    // children: [
    //   {
    //     path: 'children1/:id',
    //     name: 'children1',
    //     component: ()=> import('../views/ChildrenView1.vue')
    //   },
    //   {
    //     path: 'children2/:id',
    //     name: 'children2',
    //     component: ()=> import('../views/ChildrenView2.vue')
    //   }
    // ]
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router

// 带有动态参数params的组件对应路由，修改它的动态参数会渲染同样的组件，组件实例会被复用，这个beforeRouteUpdate钩子就会在这个时候调用
// 可以访问this实例
