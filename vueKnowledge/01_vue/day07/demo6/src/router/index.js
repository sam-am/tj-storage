import store from '@/store'
import { createRouter, createWebHashHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/about',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue')
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('../views/LoginView.vue')
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

// 全局导航前置守卫只要路由发生变化就会触发这个钩子，注意不要造成死循环
router.beforeEach((to,from,next)=>{
  // 设置登录状态初始值
  let isLogin = false

  const user = localStorage.getItem('username')
  if(user) {
    isLogin = true
  } else {
    isLogin = false
  }

  // 验证是否登录页
  if(to.name == 'login') {
    next()
  } else {
    if(isLogin) {
      next()
    } else {
      next('/login')
    }
  }
})

export default router
