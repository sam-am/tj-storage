import { createRouter, createWebHashHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/about',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue')
  },
  {
    path: '/myview/:id',
    name: 'myview',
    component: () => import('../views/MyView.vue'),
    // beforeEnter: (to, from) => {
    //   return true
    // }
  },
  {
    path: '/parent',
    name: 'parent',
    component: () => import('../views/ParentView.vue'),
    children: [
      {
        path: '/A/:id',
        name: 'A',
        component: () => import('../views/AView.vue')
      },
      {
        path: '/B',
        name: 'B',
        component: () => import('../views/BView.vue')
      }
    ]
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

// 全局守卫
// 前置导航守卫
router.beforeEach((to,from,next)=>{
  console.log('这是全局前置守卫');

  if(to.params.id == 12345) {
    next('/')
  } else {
    next()
  }
})

// 全局后置钩子，不会接受next函数，也不会改变导航本身
router.afterEach((to,from)=>{
  console.log('这是全局后置钩子');
})

export default router
