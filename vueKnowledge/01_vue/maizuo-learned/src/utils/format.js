export default {
  toWMD(time){
    const day = new Date(time*1000);
    const w = day.getDay();

    let week = ''
    switch(w){
      case 0: 
        week = '周日';
        break;
      case 1: 
        week = '周一';
        break;
      case 2: 
        week = '周二';
        break;
      case 3: 
        week = '周三';
        break;
      case 4: 
        week = '周四';
        break;
      case 5: 
        week = '周五';
        break;
      case 6: 
        week = '周六';
        break;
    }
    return `${week} ${day.getMonth()+1}月${day.getDate()}日`
  },
  toYMD(time){
    const day = new Date(time*1000);
    return `${day.getFullYear()}年${day.getMonth()+1}月${day.getDate()}日`
  },
  toMD(time){
    const day = new Date(time*1000);
    return `${day.getMonth()+1}月${day.getDate()}日`
  },
  toHM(time){
    const day = new Date(time*1000);
    return `${day.getHours()}:${day.getMinutes()}`
  }
}
// import obj from './formatß'

// export default function(){ } 
// import func from './format'


// export const a ;
// export const b ;
// import { a, b } from './format'



