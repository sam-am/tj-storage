import { createApp } from 'vue'
// import { createPinia } from 'pinia'

import App from './App.vue'
import router from './router'
// 1. 导入仓库
import store from './vuex'
import './assets/main.css'

const app = createApp(App)

// app.use(createPinia())
app.use(router)

// 2. 安装仓库插件
app.use(store)

app.mount('#app')
