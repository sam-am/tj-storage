import { createStore } from "vuex";

const store = createStore({
  state:{
    count: 99,
    age: 100
  }
})

export default store