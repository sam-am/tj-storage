import { createStore } from 'vuex'

const store = createStore({
  state: {
    name: 'young',
    age: 99
  }
})

export default store