let baseURL = 'https://www.fastmock.site/mock/c58a70bfffefdbbbb56dffcfe2b4440d/jd'  //配置服务器根地址，方便切换服务器
function wxrequest(url,methods="GET",data={}) {
  return new Promise((resolve,reject)=>{
    wx.request({
      url: baseURL + url,
      methods,
      data,
      header: {
        'content-type': 'application/json'
      },
      success(res) {
        // console.log(res.data);
        resolve(res.data)
      }
    })
  }) 
}

export default wxrequest
