Component({
  data: {
    navList: [
      {
        name: '首页',
        path: '',
        icon: '../../common/navBar/home.png',
        isIcon: '../../common/navBar/homeActive.png'
      },
      {
        name: '超级返',
        path: '',
        icon: '../../common/navBar/action.png',
        isIcon: '../../common/navBar/actionActive.png'
      },
      {
        name: '附近生活圈',
        path: '',
        icon: '../../common/navBar/nearby.png',
        isIcon: '../../common/navBar/nearbyActive.png'
      },
      {
        name: '购物车',
        path: '',
        icon: '../../common/navBar/shop.png',
        isIcon: '../../common/navBar/shopActive.png'
      },
      {
        name: '我的',
        path: '',
        icon: '../../common/navBar/my.png',
        isIcon: '../../common/navBar/myActive.png'
      }
    ],
    activeStyle: 0
  },
  methods: {
    change(e) {
      // 小程序的更新机制 => 重新渲染组件，不是响应式的 => 通过this.setData({})赋值实现更新
      this.setData({
        activeStyle: e.currentTarget.dataset.index
      })
    }
  }
})