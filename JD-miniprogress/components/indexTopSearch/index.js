Page({

  // 页面的初始数据
  data: {
    placeholder: '../../common/placeHolder/img.png',
    shake: '../../common/topSeach/shake.png',
    fangdajing: '../../common/topSeach/fangdajing.png',
    camera: '../../common/topSeach/camera.png',
    vertical: true,
    autoplay: true,
    interval: 3000,
    duration: 1000,
    circular: true,
    "easing-function": "easeInOutCubic",
    swiperList: ['电脑支架','洗衣凝珠','苹果笔记本充电线','运动羽绒服','玩具']
  },

  // 头部搜索框绑定的事件函数
  gopath(e) {
    // console.log(e);
    // 不是跳转到tabbar，可以返回
  },

})