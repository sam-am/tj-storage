// components/indexTopNav/index.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {
		navlist: [
			{name:'首页',id:0},
			{name:'补贴爆品',id:1},
			{name:'运动',id:2},
			{name:'大家电',id:3},
			{name:'电脑办公',id:4},
			{name:'家居厨具',id:5},
			{name:'小家电',id:6},
			{name:'女鞋',id:7},
			{name:'宠物',id:8},
			{name:'爱车',id:9},
			{name:'箱包皮具',id:10},
			{name:'男鞋',id:11},
			{name:'钟表',id:12},
			{name:'女装',id:13},
			{name:'房产',id:14},
			{name:'拍拍二手',id:15},
			{name:'奢侈品',id:16},
			{name:'图书',id:17},
			{name:'珠宝首饰',id:18},
			{name:'装修定制',id:19},
			{name:'工业品',id:20},
			{name:'手机',id:21}
		],
		activeStyle: 0,
		iconShow: true,
  },

  /**
   * 组件的方法列表
   */
  methods: {
		changeActive(e) {
			this.setData({
				activeStyle: e.currentTarget.dataset.index,
				iconShow: true
			})
		},
		changeIconShow() {
			this.setData({
				iconShow: !this.data.iconShow
			})
		}
  }
})
