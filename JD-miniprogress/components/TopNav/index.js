const app = getApp()

Page({
	data: {
		isRefresh: false,
		currentTab: 0,
		taskList: [{
			name: '有趣好玩'
		}, {
			name: '有趣好玩'
		}, {
			name: '有趣好玩'
		}, {
			name: '有趣好玩'
		}, {
			name: '有趣好玩'
		}, {
			name: '有趣好玩'
		}, {
			name: '有趣好玩'
		}, {
			name: '有趣好玩'
		}, {
			name: '有趣好玩'
		}, {
			name: '有趣好玩'
		}, {
			name: '有趣好玩'
		}, {
			name: '有趣好玩'
		}, {
			name: '有趣好玩'
		}, {
			name: '有趣好玩'
		}, {
			name: '有趣好玩'
		}, {
			name: '有趣好玩'
		}]
	},
	onLoad() {

	},
	handleClick(e) {
		let currentTab = e.currentTarget.dataset.index
		this.setData({
			currentTab
		})
	},
	handleSwiper(e) {
		let {
			current,
			source
		} = e.detail
		if (source === 'autoplay' || source === 'touch') {
			const currentTab = current
			this.setData({
				currentTab
			})
		}
	},
	handleTolower(e) {
		wx.showToast({
			title: '到底啦'
		})
	},
	refresherpulling() {
		wx.showLoading({
			title: '刷新中'
		})
		setTimeout(() => {
			this.setData({
				isRefresh: false
			})
			wx.showToast({
				title: '加载完成'
			})
		}, 1500)
	}
})