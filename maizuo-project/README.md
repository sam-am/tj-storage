# maizuo

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

# 项目环境说明
- 安装的是 vue3 + vant3
- 使用的语法是vue2的，应该参照vant2文档
