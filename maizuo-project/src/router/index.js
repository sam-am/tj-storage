import { createRouter, createWebHashHistory } from 'vue-router'

const routes = [
  {
    path: '/',
    name: 'home',
    component: () => import('../views/HomeView.vue'),
    redirect: '/movie',
    children: [
      {
        path: 'movie',
        name: 'movie',
        component: () => import('../views/home/movie.vue'),
        children: [
          {
            path: 'hot',
            name: 'hot',
            component: () => import('../views/home/MovieChildren/HotMovie.vue')
          },
          {
            path: 'soon',
            name: 'soon',
            component: () => import('../views/home/MovieChildren/SoonMovie.vue')
          }
        ]
      },
      {
        path: 'cinema',
        name: 'cinema',
        component: () => import('../views/home/cinema.vue')
      },
      {
        path: 'news',
        name: 'news',
        component: () => import('../views/home/news.vue')
      },
      {
        path: 'my',
        name: 'my',
        component: () => import('../views/home/my.vue')
      }
    ]
  },
  {
    path: '/info/:mid',
    name: 'info',
    component: () => import('../views/info.vue')
  },
  {
    path: '/cinemainfo/:cid/:fid?',  // 影院id是cid，fid是filmId电影id
    name: 'cinfo',
    component: () => import('../views/cinfo.vue'),
    children: [
      {
        path: ':time',
        name: 'schedule',
        component: () => import('../views/schedule.vue')
      }
    ]
  },
  {
    path: '/city',
    name: 'city',
    component: () => import('../views/city.vue')
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
