export default {
  toWMD(time) {
    const day = new Date(time*1000)
    return `${day.getFullYear()}年${day.getMonth() + 1}月${day.getDate()}日`
  }
}