import axios from "axios";
import qs from 'qs'

const myaxios = axios.create({
  baseURL: 'http://www.young1024.com:1234/',
  timeout: 6000
})

// 请求拦截
myaxios.interceptors.request.use((config)=>{
  config.data = qs.stringify(config.data)
  return config
})

// 响应拦截
myaxios.interceptors.response.use((res)=>{
  return res.data
})

export default myaxios