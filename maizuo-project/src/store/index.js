import { createStore } from 'vuex'

export default createStore({
  state: {
    city: {
      cityId: 440300,
      name: '深圳',
      pinyin: 'shenzhen',
      isHot: 1
    }
  },
  getters: {
  },
  mutations: {
    changeCity(state,info) {
      state.city = info
    }
  },
  actions: {
  },
  modules: {
  },
  plugins: [(store)=>{
    /*
      1. 先判断storage中年有没有存的值
        1）有 - 赋值给state.city
        2) 没有 - 把默认值存进storage中年

      2. 修改的时候同步更新到storage
    */
    // 注意：仓库初始化好了之后会调用一次，修改的时候也会调用subscribe()
    const local = localStorage.getItem('city')
    if(local) {
      store.commit('changeCity',JSON.parse(local))
    } else {
      localStorage.setItem('city', JSON.stringify(store.state.city))
    }

    // 2. 只要mutation了就会触发subscribe()
    store.subscribe((mutation, state)=>{
      // console.log(state);
      localStorage.setItem('city', JSON.stringify(store.state.city))
    })
  }]
})
