import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

// 引入vant-ui组件样式
import 'vant/lib/index.css';

createApp(App).use(store).use(router).mount('#app')
