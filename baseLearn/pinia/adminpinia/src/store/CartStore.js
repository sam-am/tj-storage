// defineStore => 创建全局数据 => 语法：defineStore('id',配置项) => 返回值就是仓库函数
import { defineStore } from 'pinia'

let CartStore = defineStore('CartStore',{
  state() {
    return {
      age: 18,
      name: '向阳'
    }
  },
  getters: {
    getBro() {
      return this.age + 10  //参数一：当前仓库实例 => this当前仓库存储实例
    }, 
    gets() {
      return this.getBro + 10
    }
  },
  actions:{
    ayGetData(val){
      setTimeout(()=>{
        let datas = val  //获取异步数据

        this.age = datas
      }, 1000)
    }
  }
})

export default CartStore