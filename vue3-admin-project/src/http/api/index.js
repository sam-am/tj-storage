import service from '../index'

export function getIndex(url,params={}) {
  return new Promise((resolve,reject)=>{
    service.get(url,{
      params
    }).then(res=>{
      resolve(res)
    }).catch(err=>{
      reject(err)
    })
  })
}

export function getPie(url,params={}) {
  return new Promise((resolve,reject)=>{
    service.get(url,{
      params
    }).then(res=>{
      resolve(res)
    }).catch(err=>{
      reject(err)
    })
  })
}