// 导入请求公共配置
import service from '../index'

// 封装登录请求
export function loginReq(url,data={}) {
  return new Promise((resolve,reject)=>{
    service.post(url,data).then(res=>{
      resolve(res)
    }).catch(err=>{
      reject(err)
    })
  })
}

// 获取权限数据的请求函数
export function getLimits(url,data={}) {
  return new Promise((reslove,reject)=>{  //执行器
    service.post(url,data).then(res=>{
      reslove(res)
    }).catch(err=>{
      reject(err)
    })
  })
}