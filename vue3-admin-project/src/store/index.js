import { createStore } from 'vuex'

// 引入请求的接口
import { getLimits } from '@/http/api/login'

export default createStore({
  state: {  //仓库存放数据的，=> 数据是响应式的 => 本质是 vue中的响应api实现的
    navListOpen: false,  //默认展开的
    limitList: JSON.parse(sessionStorage.getItem('limitlist'))
  },
  getters: {  
  },
  mutations: { 
    changenavListOpen(state,data) {
      state.navListOpen = data
    },
    changeLimitList(state,data) {
      state.limitList = data
    }
  },
  actions: {  
    asyncChangeLimitList({commit}, obj) {
      // 发送请求获取权限
      getLimits('/getLimit', obj).then(res=>{
        commit('changeLimitList',res.data.limitList)
        sessionStorage.setItem('limitlist',JSON.stringify(res.data.limitList))
        
        let webpackObj = require.context('@/router/modules', true, /\.js/)
        console.log(webpackObj);
        // let allRoutes = []
        // webpackObj.keys().forEach(item=>{
        //   allRoutes.push(webpackObj(item).default)
        // })
      })
    }
  },
  modules: {
  }
})
