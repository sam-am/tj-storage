let documentRoute = {
  path: 'document',
  name: 'document',
  meta: {
    name: [{name:'文档'}],
    token: true
  },
  component: () => import('@/views/Document/index.vue')
}

export default documentRoute