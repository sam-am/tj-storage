let indexRoute = {
  path: 'index',
  name: 'index',
  meta: {
    name: [{name:'首页'}],
    token: true
  },
  component: () => import('@/views/Index/index.vue')
}

export default indexRoute