// createRouter => 创建路由实例对象，vue2: new VueRouter({mode:})

import { createRouter, createWebHistory } from 'vue-router'
import Login from '../views/Login/configAsyncIndex.vue'

// 将抽离出去的路由对象引入
import indexRoute from './modules/index'
import documentRoute from './modules/document'
import leadRoute from './modules/lead'
import children1Route from './modules/children1'
import children2Route from './modules/children2'

const routes = [
  {
    path: '/login',
    name: 'login',
    meta: {
      token: false
    },
    component: Login
  },
  {
    path: '/',
    name: 'Layout',
    redirect: '/index',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../Layout/index.vue'),
    children: [indexRoute,documentRoute,leadRoute,children1Route,children2Route]
  }
]

const router = createRouter({  // 创建路由实例  process.env.BASE_URL配置环境变量的可要可不要
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
