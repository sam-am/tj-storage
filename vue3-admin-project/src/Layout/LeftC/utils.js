// 方法：根据 name => 匹配出这一项的icon
// name 后端定义 => icon 前端定义

import { isTSMethodSignature } from "@babel/types"

/*
  步骤：
    1. 保存所有侧边栏的icon图标
*/

// js存放数据的本质 => 数组 对象
let iconList = [  //根据ui图自己添加icon图标
  { name: '首页', icon: 'icon-shouye1' },
  { name: '文档', icon: 'icon-shiyongwendang' },
  { name: '引导页', icon: 'icon-zhifeiji' }
]

export function getIcon(val) { //这个参数就是你传过来的参数，每一项的名称  //比如 首页
  let items = iconList.find((item) => {
    return item.name == val  //满足条件，返回这一项
  })
  if (items) {
    return items.icon
  } else {
    // 这一项不存在，就添加一个默认图标
    return "icon-morentu"
  }
}